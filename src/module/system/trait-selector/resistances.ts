import { ActorSF2e, NPCSF2e, HazardSF2e } from "@actor/index";
import { TagSelectorBase } from "./base";
import { SelectableTagField } from "./index";

export class ResistanceSelector extends TagSelectorBase<ActorSF2e> {
    override objectProperty = "data.traits.dr";

    static override get defaultOptions(): FormApplicationOptions {
        return {
            ...super.defaultOptions,
            id: "trait-selector",
            classes: ["sf2e"],
            template: "systems/sf2e/templates/system/trait-selector/resistances.html",
            title: "SF2E.ResistancesLabel",
            width: "auto",
            height: 700,
        };
    }

    protected get configTypes(): readonly SelectableTagField[] {
        return ["resistanceTypes"] as const;
    }

    override getData() {
        const data: any = super.getData();

        if (this.object instanceof NPCSF2e || this.object instanceof HazardSF2e) {
            data.hasExceptions = true;
        }

        const choices: any = {};
        const resistances = this.object.data._source.data.traits.dr;
        Object.entries(this.choices).forEach(([type, label]) => {
            const resistance = resistances.find((resistance) => resistance.type === type);
            choices[type] = {
                label,
                selected: !!resistance,
                value: resistance?.value ?? "",
                exceptions: resistance?.exceptions ?? "",
            };
        });
        data.choices = choices;

        return data;
    }

    override activateListeners($html: JQuery) {
        super.activateListeners($html);

        $html
            .find<HTMLInputElement>("input[id^=input_value]")
            .on("focusin", (event) => {
                const input = $(event.currentTarget);
                input.prev().prev().prop("checked", true);
            })
            .on("focusout", (event) => {
                const input = $(event.currentTarget);
                if (!input.val()) {
                    input.prev().prev().prop("checked", false);
                }
            });
    }

    protected override async _updateObject(_event: Event, formData: Record<string, unknown>) {
        const update = this.getUpdateData(formData);
        if (update) {
            this.object.update({ [this.objectProperty]: update });
        }
    }

    protected getUpdateData(formData: Record<string, unknown>) {
        const choices: Record<string, unknown>[] = [];
        for (const [k, v] of Object.entries(formData as Record<string, any>)) {
            if (v.length > 1 && v[0]) {
                if (Number.isInteger(Number(v[1])) && v[1] !== "") {
                    const exceptions = v[2] ?? "";
                    choices.push({ type: k, value: Number(v[1]), exceptions });
                }
            }
        }
        return choices;
    }
}
