import { ActionsSF2e, SkillActionOptions } from "../actions";

export function access(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "computers");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.Access.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:access"],
        ["action:access"],
        ["manipulate"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Access", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Access", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Access", "criticalFailure"),
        ]
    );
}
