import { ActionsSF2e, SkillActionOptions } from "../actions";

export function manipulateModule(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "computers");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "D",
        "SF2E.Actions.ManipulateAModule.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:manipulateModule"],
        ["action:manipulateModule"],
        ["manipulate"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.ManipulateAModule", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.ManipulateAModule", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.ManipulateAModule", "criticalFailure"),
        ]
    );
}
