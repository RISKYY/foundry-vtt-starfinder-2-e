import { ActionsSF2e, SkillActionOptions } from "../actions";

export function feint(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "deception");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.Feint.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:feint"],
        ["action:feint"],
        ["mental"],
        checkType,
        options.event,
        (target) => target.perception,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Feint", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Feint", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Feint", "criticalFailure"),
        ]
    );
}
