import { ActionsSF2e, SkillActionOptions } from "../actions";

export function lie(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "deception");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph,
        "SF2E.Actions.Lie.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:lie"],
        ["action:lie"],
        ["auditory", "concentrate", "linguistic", "mental", "secret"],
        checkType,
        options.event,
        (target) => target.perception,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Lie", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Lie", "failure"),
        ]
    );
}
