import { ActionsSF2e, SkillActionOptions } from "../actions";

export function impersonate(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "deception");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph,
        "SF2E.Actions.Impersonate.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:impersonate"],
        ["action:impersonate"],
        ["concentrate", "exploration", "manipulate", "secret"],
        checkType,
        options.event,
        (target) => target.perception,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Impersonate", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Impersonate", "failure"),
            ActionsSF2e.note(selector, "SF2E.Actions.Impersonate", "criticalFailure"),
        ]
    );
}
