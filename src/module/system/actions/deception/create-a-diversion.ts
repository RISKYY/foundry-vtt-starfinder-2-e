import { ActionsSF2e, SkillActionOptions } from "../actions";

type CreateADiversionVariant = "distracting-words" | "gesture" | "trick";

export function createADiversion(options: { variant: CreateADiversionVariant } & SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "deception");
    let title = "SF2E.Actions.CreateADiversion.";
    const traits = ["mental"];
    switch (options.variant) {
        case "distracting-words":
            title += "DistractingWords";
            traits.push("auditory", "linguistic");
            break;
        case "gesture":
            title += "Gesture";
            traits.push("manipulate");
            break;
        case "trick":
            title += "Trick";
            traits.push("manipulate");
            break;
        default: {
            const msg = game.i18n.format("SF2E.ActionsWarning.DeceptionUnknownVariant", { variant: options.variant });
            ui.notifications.error(msg);
            return;
        }
    }
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        title,
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:create-a-diversion"],
        ["action:create-a-diversion", options.variant],
        traits.sort(),
        checkType,
        options.event,
        (target) => target.perception,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.CreateADiversion", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.CreateADiversion", "failure"),
        ]
    );
}
