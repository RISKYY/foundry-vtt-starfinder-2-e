import { ActionsSF2e, SkillActionOptions } from "../actions";

export function sneak(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "stealth");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.Sneak.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:sneak"],
        ["action:sneak"],
        ["move", "secret"],
        checkType,
        options.event,
        (target) => target.perception,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Sneak", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Sneak", "failure"),
            ActionsSF2e.note(selector, "SF2E.Actions.Sneak", "criticalFailure"),
        ]
    );
}
