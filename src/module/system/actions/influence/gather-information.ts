import { ActionsSF2e, SkillActionOptions } from "../actions";

export function gatherInformation(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "influence");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph,
        "SF2E.Actions.GatherInformation.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:gather-information"],
        ["action:gather-information"],
        ["exploration", "secret"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.GatherInformation", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.GatherInformation", "criticalFailure"),
        ]
    );
}
