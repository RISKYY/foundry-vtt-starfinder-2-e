import { ActionsSF2e, SkillActionOptions } from "../actions";

export function makeAnImpression(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "influence");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph,
        "SF2E.Actions.MakeAnImpression.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:make-an-impression"],
        ["action:make-an-impression"],
        ["auditory", "concentrate", "exploration", "linguistic", "mental"],
        checkType,
        options.event,
        (target) => target.will,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.MakeAnImpression", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.MakeAnImpression", "failure"),
            ActionsSF2e.note(selector, "SF2E.Actions.MakeAnImpression", "criticalFailure"),
        ]
    );
}
