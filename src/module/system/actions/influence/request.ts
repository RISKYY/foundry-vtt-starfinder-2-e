import { ActionsSF2e, SkillActionOptions } from "../actions";

export function request(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "influence");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.Request.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:request"],
        ["action:request"],
        ["auditory", "concentrate", "linguistic", "mental"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Request", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Request", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Request", "failure"),
            ActionsSF2e.note(selector, "SF2E.Actions.Request", "criticalFailure"),
        ]
    );
}
