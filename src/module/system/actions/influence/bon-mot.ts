import { ActionsSF2e, SkillActionOptions } from "../actions";

export function bonMot(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "influence");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.BonMot.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:bon-mot"],
        ["action:bon-mot"],
        ["auditory", "concentrate", "emotion", "linguistic", "mental"],
        checkType,
        options.event,
        (target) => target.will,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.BonMot", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.BonMot", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.BonMot", "criticalFailure"),
        ]
    );
}
