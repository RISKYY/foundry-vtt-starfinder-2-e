import { ActionsSF2e, SkillActionOptions } from "../actions";

export function coerce(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "influence");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph,
        "SF2E.Actions.Coerce.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:coerce"],
        ["action:coerce"],
        ["auditory", "concentrate", "emotion", "exploration", "linguistic", "mental"],
        checkType,
        options.event,
        (target) => target.will,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Coerce", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Coerce", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Coerce", "failure"),
            ActionsSF2e.note(selector, "SF2E.Actions.Coerce", "criticalFailure"),
        ]
    );
}
