import { ActionsSF2e, SkillActionOptions } from "../actions";

export function demoralize(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "influence");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.Demoralize.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:demoralize"],
        ["action:demoralize"],
        ["auditory", "concentrate", "emotion", "fear", "mental"],
        checkType,
        options.event,
        (target) => target.will,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Demoralize", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Demoralize", "success"),
        ]
    );
}
