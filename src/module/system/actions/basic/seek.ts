import { ActionsSF2e, SkillActionOptions } from "../actions";

export function seek(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "perception");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.Seek.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:seek"],
        ["action:seek"],
        ["concentrate", "secret"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Seek", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Seek", "success"),
        ]
    );
}
