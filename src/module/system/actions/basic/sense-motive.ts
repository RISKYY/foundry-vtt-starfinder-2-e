import { ActionsSF2e, SkillActionOptions } from "../actions";

export function senseMotive(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "perception");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.SenseMotive.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:sense-motive"],
        ["action:sense-motive"],
        ["concentrate", "secret"],
        checkType,
        options.event,
        (target) => target.deception,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.SenseMotive", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.SenseMotive", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.SenseMotive", "failure"),
            ActionsSF2e.note(selector, "SF2E.Actions.SenseMotive", "criticalFailure"),
        ]
    );
}
