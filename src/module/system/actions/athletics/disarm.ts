import { ActionsSF2e, SkillActionOptions } from "../actions";

export function disarm(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "athletics");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.Disarm.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:disarm"],
        ["action:disarm"],
        ["attack"],
        checkType,
        options.event,
        (target) => target.reflex,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Disarm", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Disarm", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Disarm", "criticalFailure"),
        ]
    );
}
