import { ActionsSF2e, SkillActionOptions } from "../actions";

export function trip(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "athletics");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.Trip.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:trip"],
        ["action:trip"],
        ["attack"],
        checkType,
        options.event,
        (target) => target.reflex,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Trip", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Trip", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Trip", "criticalFailure"),
        ]
    );
}
