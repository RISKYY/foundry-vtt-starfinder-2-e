import { ActionsSF2e, SkillActionOptions } from "../actions";

export function highJump(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "athletics");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "D",
        "SF2E.Actions.HighJump.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:stride", "action:leap", "action:high-jump"],
        ["action:stride", "action:leap", "action:high-jump"],
        ["move"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.HighJump", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.HighJump", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.HighJump", "failure"),
            ActionsSF2e.note(selector, "SF2E.Actions.HighJump", "criticalFailure"),
        ]
    );
}
