import { ActionsSF2e, SkillActionOptions } from "../actions";

export function grapple(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "athletics");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.Grapple.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:grapple"],
        ["action:grapple"],
        ["attack"],
        checkType,
        options.event,
        (target) => target.fortitude,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Grapple", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Grapple", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Grapple", "failure"),
            ActionsSF2e.note(selector, "SF2E.Actions.Grapple", "criticalFailure"),
        ]
    );
}
