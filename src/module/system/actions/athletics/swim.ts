import { ActionsSF2e, SkillActionOptions } from "../actions";

export function swim(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "athletics");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.Swim.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:swim"],
        ["action:swim"],
        ["move"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Swim", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Swim", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Swim", "criticalFailure"),
        ]
    );
}
