import { ActionsSF2e, SkillActionOptions } from "../actions";

export function forceOpen(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "athletics");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.ForceOpen.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:force-open"],
        ["action:force-open"],
        ["attack"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.ForceOpen", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.ForceOpen", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.ForceOpen", "criticalFailure"),
        ]
    );
}
