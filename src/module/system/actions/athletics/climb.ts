import { ActionsSF2e, SkillActionOptions } from "../actions";

export function climb(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "athletics");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.Climb.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:climb"],
        ["action:climb"],
        ["move"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Climb", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Climb", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Climb", "criticalFailure"),
        ]
    );
}
