import { ActionsSF2e, SkillActionOptions } from "../actions";

export function longJump(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "athletics");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "D",
        "SF2E.Actions.LongJump.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:stride", "action:leap", "action:long-jump"],
        ["action:stride", "action:leap", "action:long-jump"],
        ["move"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.LongJump", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.LongJump", "failure"),
            ActionsSF2e.note(selector, "SF2E.Actions.LongJump", "criticalFailure"),
        ]
    );
}
