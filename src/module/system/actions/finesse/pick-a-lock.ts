import { ActionsSF2e, SkillActionOptions } from "../actions";

export function pickALock(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "finesse");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "D",
        "SF2E.Actions.PickALock.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:pick-a-lock"],
        ["action:pick-a-lock"],
        ["manipulate"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.PickALock", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.PickALock", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.PickALock", "criticalFailure"),
        ]
    );
}
