import { ActionsSF2e, SkillActionOptions } from "../actions";

export function squeeze(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "acrobatics");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph,
        "SF2E.Actions.Squeeze.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:squeeze"],
        ["action:squeeze"],
        ["exploration", "move"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Squeeze", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Squeeze", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Squeeze", "criticalFailure"),
        ]
    );
}
