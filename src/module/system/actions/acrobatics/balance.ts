import { ActionsSF2e, SkillActionOptions } from "../actions";

export function balance(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "acrobatics");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.Balance.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:balance"],
        ["action:balance"],
        ["move"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.Balance", "criticalSuccess"),
            ActionsSF2e.note(selector, "SF2E.Actions.Balance", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.Balance", "failure"),
            ActionsSF2e.note(selector, "SF2E.Actions.Balance", "criticalFailure"),
        ]
    );
}
