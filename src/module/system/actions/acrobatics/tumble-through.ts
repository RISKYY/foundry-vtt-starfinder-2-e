import { ActionsSF2e, SkillActionOptions } from "../actions";

export function tumbleThrough(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "acrobatics");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.TumbleThrough.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:tumble-through"],
        ["action:tumble-through"],
        ["move"],
        checkType,
        options.event,
        (target) => target.reflex,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.TumbleThrough", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.TumbleThrough", "failure"),
        ]
    );
}
