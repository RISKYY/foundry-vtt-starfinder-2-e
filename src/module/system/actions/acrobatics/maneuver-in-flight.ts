import { ActionsSF2e, SkillActionOptions } from "../actions";

export function maneuverInFlight(options: SkillActionOptions) {
    const { checkType, property, stat, subtitle } = ActionsSF2e.resolveStat(options?.skill ?? "acrobatics");
    ActionsSF2e.simpleRollActionCheck(
        options.actors,
        property,
        options.glyph ?? "A",
        "SF2E.Actions.ManeuverInFlight.Title",
        subtitle,
        options.modifiers,
        ["all", checkType, stat, "action:maneuver-in-flight"],
        ["action:maneuver-in-flight"],
        ["move"],
        checkType,
        options.event,
        undefined,
        (selector: string) => [
            ActionsSF2e.note(selector, "SF2E.Actions.ManeuverInFlight", "success"),
            ActionsSF2e.note(selector, "SF2E.Actions.ManeuverInFlight", "failure"),
            ActionsSF2e.note(selector, "SF2E.Actions.ManeuverInFlight", "criticalFailure"),
        ]
    );
}
