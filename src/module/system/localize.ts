import { ErrorSF2e } from "@module/utils";
import * as translationsSF2e from "static/lang/en.json";

type TranslationsSF2e = Record<string, TranslationDictionaryValue> & typeof translationsSF2e;

export class LocalizeSF2e {
    static ready = false;

    private static _translations: TranslationsSF2e;

    static get translations(): TranslationsSF2e {
        if (!this.ready) {
            throw ErrorSF2e("LocalizeSF2e instantiated too early");
        }
        if (this._translations === undefined) {
            this._translations = mergeObject(game.i18n._fallback, game.i18n.translations, {
                enforceTypes: true,
            }) as TranslationsSF2e;
        }
        return this._translations;
    }
}
