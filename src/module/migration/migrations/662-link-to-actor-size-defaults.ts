import { ActorSF2e } from "@actor";
import { ActorSourceSF2e } from "@actor/data";
import { MigrationBase } from "../base";

/** Set default linkToActorSize flag */
export class Migration662LinkToActorSizeDefaults extends MigrationBase {
    static override version = 0.662;

    override async updateActor(actorSource: ActorSourceSF2e): Promise<void> {
        const linkToActorSize = !["hazard", "loot"].includes(actorSource.type);
        actorSource.token.flags ??= { sf2e: { linkToActorSize } };
        actorSource.token.flags.sf2e ??= { linkToActorSize };
        actorSource.token.flags.sf2e.linkToActorSize ??= linkToActorSize;
    }

    override async updateToken(tokenSource: foundry.data.TokenSource, actor: ActorSF2e): Promise<void> {
        const linkToActorSize = !["hazard", "loot"].includes(actor.type);
        tokenSource.flags.sf2e ??= { linkToActorSize };
        tokenSource.flags.sf2e.linkToActorSize ??= linkToActorSize;
    }
}
