import { ItemSourceSF2e } from "@item/data";
import { MigrationBase } from "../base";

export class Migration618MigrateItemImagePaths extends MigrationBase {
    static override version = 0.618;

    readonly IMAGE_PATHS: Record<string, ImagePath> = {
        "systems/sf2e/icons/equipment/weapons/blowgun.png": "systems/sf2e/icons/equipment/weapons/blowgun.jpg",
        "systems/sf2e/icons/equipment/weapons/trident.png": "systems/sf2e/icons/equipment/weapons/trident.jpg",
        "systems/sf2e/icons/equipment/weapons/longsword.png": "systems/sf2e/icons/equipment/weapons/longsword.jpg",
        "systems/sf2e/icons/equipment/weapons/composite-longbow.png":
            "systems/sf2e/icons/equipment/weapons/composite-longbow.jpg",
        "systems/sf2e/icons/equipment/weapons/composite-shortbow.png":
            "systems/sf2e/icons/equipment/weapons/composite-shortbow.jpg",
        "systems/sf2e/icons/equipment/weapons/dagger.png": "systems/sf2e/icons/equipment/weapons/dagger.jpg",
        "systems/sf2e/icons/equipment/weapons/katar.png": "systems/sf2e/icons/equipment/weapons/katar.jpg",
        "systems/sf2e/icons/equipment/weapons/kukri.png": "systems/sf2e/icons/equipment/weapons/kukri.jpg",
        "systems/sf2e/icons/equipment/weapons/shortbow.png": "systems/sf2e/icons/equipment/weapons/shortbow.jpg",
        "systems/sf2e/icons/equipment/weapons/scimitar.png": "systems/sf2e/icons/equipment/weapons/scimitar.jpg",
        "systems/sf2e/icons/equipment/weapons/hatchet.png": "systems/sf2e/icons/equipment/weapons/hatchet.jpg",
        "systems/sf2e/icons/equipment/weapons/halfling-sling-staff.png":
            "systems/sf2e/icons/equipment/weapons/halfling-sling-staff.jpg",
        "systems/sf2e/icons/equipment/weapons/halberd.png": "systems/sf2e/icons/equipment/weapons/halberd.jpg",
        "systems/sf2e/icons/equipment/weapons/shield-spikes.png":
            "systems/sf2e/icons/equipment/weapons/shield-spikes.jpg",
        "systems/sf2e/icons/equipment/weapons/light-mace.jpg": "systems/sf2e/icons/equipment/weapons/light-mace.jpg",
        "systems/sf2e/icons/equipment/weapons/morningstar.png": "systems/sf2e/icons/equipment/weapons/morningstar.jpg",
        "systems/sf2e/icons/equipment/weapons/sling.png": "systems/sf2e/icons/equipment/weapons/sling.jpg",
        "systems/sf2e/icons/equipment/weapons/main-gauche.png": "systems/sf2e/icons/equipment/weapons/main-gauche.jpg",
        "systems/sf2e/icons/equipment/weapons/bastard-sword.png":
            "systems/sf2e/icons/equipment/weapons/bastard-sword.jpg",
        "systems/sf2e/icons/equipment/weapons/spear.png": "systems/sf2e/icons/equipment/weapons/spear.jpg",
        "systems/sf2e/icons/equipment/weapons/staff.png": "systems/sf2e/icons/equipment/weapons/staff.jpg",
        "systems/sf2e/icons/equipment/weapons/katana.png": "systems/sf2e/icons/equipment/weapons/katana.jpg",
        "systems/sf2e/icons/equipment/weapons/elven-curve-blade.png":
            "systems/sf2e/icons/equipment/weapons/elven-curve-blade.jpg",
        "systems/sf2e/icons/equipment/weapons/bo-staff.png": "systems/sf2e/icons/equipment/weapons/bo-staff.jpg",
        "systems/sf2e/icons/equipment/weapons/clan-dagger.png": "systems/sf2e/icons/equipment/weapons/clan-dagger.jpg",
        "systems/sf2e/icons/equipment/weapons/dogslicer.png": "systems/sf2e/icons/equipment/weapons/dogslicer.jpg",
        "systems/sf2e/icons/equipment/weapons/falchion.png": "systems/sf2e/icons/equipment/weapons/falchion.jpg",
        "systems/sf2e/icons/equipment/weapons/fist.png": "systems/sf2e/icons/equipment/weapons/fist.jpg",
        "systems/sf2e/icons/equipment/weapons/gauntlet.png": "systems/sf2e/icons/equipment/weapons/gauntlet.jpg",
        "systems/sf2e/icons/equipment/weapons/gnome-hooked-hammer.png":
            "systems/sf2e/icons/equipment/weapons/gnome-hooked-hammer.jpg",
        "systems/sf2e/icons/equipment/weapons/greatpick.png": "systems/sf2e/icons/equipment/weapons/greatpick.jpg",
        "systems/sf2e/icons/equipment/weapons/guisarme.png": "systems/sf2e/icons/equipment/weapons/guisarme.jpg",
        "systems/sf2e/icons/equipment/weapons/horsechopper.png":
            "systems/sf2e/icons/equipment/weapons/horsechopper.jpg",
        "systems/sf2e/icons/equipment/weapons/lance.png": "systems/sf2e/icons/equipment/weapons/lance.jpg",
        "systems/sf2e/icons/equipment/weapons/maul.png": "systems/sf2e/icons/equipment/weapons/maul.jpg",
        "systems/sf2e/icons/equipment/weapons/pick.png": "systems/sf2e/icons/equipment/weapons/pick.jpg",
        "systems/sf2e/icons/equipment/weapons/ranseur.png": "systems/sf2e/icons/equipment/weapons/ranseur.jpg",
        "systems/sf2e/icons/equipment/weapons/sai.png": "systems/sf2e/icons/equipment/weapons/sai.jpg",
        "systems/sf2e/icons/equipment/weapons/sawtooth-saber.png":
            "systems/sf2e/icons/equipment/weapons/sawtooth-saber.jpg",
        "systems/sf2e/icons/equipment/weapons/shield-bash.png": "systems/sf2e/icons/equipment/weapons/shield-bash.jpg",
        "systems/sf2e/icons/equipment/weapons/shield-boss.png": "systems/sf2e/icons/equipment/weapons/shield-boss.jpg",
        "systems/sf2e/icons/equipment/weapons/shuriken.png": "systems/sf2e/icons/equipment/weapons/shuriken.jpg",
        "systems/sf2e/icons/equipment/weapons/spiked-gauntlet.png":
            "systems/sf2e/icons/equipment/weapons/spiked-gauntlet.jpg",
        "systems/sf2e/icons/equipment/weapons/broom.png": "systems/sf2e/icons/equipment/held-items/broom-of-flying.jpg",
        "systems/sf2e/icons/equipment/weapons/cutlass.png": "systems/sf2e/icons/equipment/weapons/scimitar.jpg",
        "systems/sf2e/icons/equipment/weapons/scalpel.png": "systems/sf2e/icons/equipment/weapons/war-razor.jpg",
        "systems/sf2e/icons/equipment/weapons/cane.png": "systems/sf2e/icons/equipment/weapons/cane.jpg",
    };

    override async updateItem(itemData: ItemSourceSF2e) {
        itemData.img = this.IMAGE_PATHS[itemData.img] ?? itemData.img;
    }
}
