import { ActorSourceSF2e } from "@actor/data";
import { MigrationBase } from "../base";

export class Migration622RemoveOldTokenEffectIcons extends MigrationBase {
    static override version = 0.622;

    override async updateActor(actorData: ActorWithTokenEffect): Promise<void> {
        // remove deprecated condition token effects
        if (actorData.token.effects) {
            actorData.token["-=effects"] = null;
        }

        // remove deprecated rule element token effects
        const effects = actorData.flags.sf2e?.token?.effects ?? {};
        for (const img of Object.keys(effects)) {
            if (actorData.token.effects?.map((fx) => fx.replace(/[.]/g, "-"))?.includes(img)) {
                actorData.token.effects = actorData.token.effects.filter((fx) => fx.replace(/[.]/g, "-") !== img);
            }
        }
    }

    override async updateToken(tokenData: foundry.data.TokenSource): Promise<void> {
        // remove deprecated condition token effects
        tokenData.effects = tokenData.effects.filter((fx) => !fx.startsWith("systems/sf2e/icons/conditions/"));

        // remove deprecated rule element token effects
        const effects =
            tokenData.actorData.flags?.sf2e?.token?.effects ??
            game.actors.get(tokenData.actorId)?.data?.flags?.sf2e?.token?.effects ??
            {};
        for (const img of Object.keys(effects)) {
            if (tokenData.effects.map((fx) => fx.replace(/[.]/g, "-")).includes(img)) {
                tokenData.effects = tokenData.effects.filter((fx) => fx.replace(/[.]/g, "-") !== img);
            }
        }
    }
}

type ActorWithTokenEffect = ActorSourceSF2e & {
    token: ActorSourceSF2e["token"] & {
        effects?: string[];
        "-=effects"?: null;
    };
};
