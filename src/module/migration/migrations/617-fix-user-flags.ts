import { MigrationBase } from "../base";

export class Migration617FixUserFlags extends MigrationBase {
    static override version = 0.617;

    override async updateUser(userData: foundry.data.UserSource): Promise<void> {
        const flags = userData.flags as Record<string, any>;
        const settings = flags.SF2e?.settings;
        if (settings) {
            const uiTheme = settings.color ?? "blue";
            const showRollDialogs = !settings.quickD20roll;
            flags.sf2e ??= {};
            flags.sf2e.settings = {
                uiTheme,
                showEffectPanel: flags.sf2e?.showEffectPanel ?? true,
                showRollDialogs,
            };
            delete flags.SF2e;
            flags["-=SF2e"] = null;
        }
    }
}
