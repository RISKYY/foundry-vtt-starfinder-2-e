import { CreatureSF2e } from "@actor";
import { ItemSF2e } from "@item";
import { RuleElementSF2e } from "../rule-element";
import { RuleElementSource } from "../rules-data-definitions";

/** Reduce current hit points without applying damage */
export class LoseHitPointsRuleElement extends RuleElementSF2e {
    constructor(data: RuleElementSource, item: Embedded<ItemSF2e>) {
        super(data, item);
        const actorIsCreature = this.actor instanceof CreatureSF2e;
        const valueIsValid = typeof data.value === "number" || typeof data.value === "string";
        if (!(actorIsCreature && valueIsValid)) this.ignored = true;
    }

    override onCreate(actorUpdates: Record<string, unknown>): void {
        if (this.ignored) return;
        const value = Math.abs(this.resolveValue());
        if (typeof value === "number") {
            const currentHP = this.actor.data._source.data.attributes.hp.value;
            actorUpdates["data.attributes.hp.value"] = Math.max(currentHP - value, 0);
        }
    }
}

export interface LoseHitPointsRuleElement extends RuleElementSF2e {
    get actor(): CreatureSF2e;
}
