import type { CharacterSF2e, NPCSF2e } from "@actor";
import { ActorType } from "@actor/data";
import { RuleElementSF2e } from "../rule-element";

/**
 * @category RuleElement
 */
class DexterityModifierCapRuleElement extends RuleElementSF2e {
    protected static override validActorTypes: ActorType[] = ["character", "npc"];

    override onBeforePrepareData() {
        if (this.ignored) return;

        const value = this.resolveValue(this.data.value);
        if (typeof value === "number") {
            this.actor.data.data.attributes.dexCap.push({
                value,
                source: this.label,
            });
        } else {
            console.warn("SF2E | Dexterity modifier cap requires at least a label field or item name, and a value");
        }
    }
}

interface DexterityModifierCapRuleElement extends RuleElementSF2e {
    get actor(): CharacterSF2e | NPCSF2e;
}

export { DexterityModifierCapRuleElement };
