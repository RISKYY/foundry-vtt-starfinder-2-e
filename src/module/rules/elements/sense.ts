import { RuleElementSF2e } from "../rule-element";
import { SenseAcuity, SenseData } from "@actor/creature/data";
import { RuleElementData, RuleElementSource } from "../rules-data-definitions";
import { CharacterSF2e, FamiliarSF2e } from "@actor";
import { ActorType } from "@actor/data";
import { ItemSF2e } from "@item";

/**
 * @category RuleElement
 */
export class SenseRuleElement extends RuleElementSF2e {
    protected static override validActorTypes: ActorType[] = ["character", "familiar"];

    private static isMoreAcute(replacement?: SenseAcuity, existing?: SenseAcuity): boolean {
        if (!replacement && existing) return false;
        return (
            (replacement && !existing) ||
            (replacement === "precise" && ["imprecise", "vague"].includes(existing!)) ||
            (replacement === "imprecise" && existing === "vague")
        );
    }

    constructor(data: RuleElementSource, item: Embedded<ItemSF2e>) {
        const defaultLabels: Record<string, string | undefined> = CONFIG.SF2E.senses;
        data.label ??= defaultLabels[data.selector ?? ""];
        super(data, item);
    }

    override onBeforePrepareData(): void {
        if (this.ignored) return;

        const range = this.resolveValue(this.data.range);
        const senses = this.actor.data.data.traits.senses;

        if (this.data.selector) {
            const existing = senses.find((sense) => sense.type === this.data.selector);
            const source = `${this.item.id}-${this.item.name}-${this.data.key}`;
            if (existing) {
                // upgrade existing sense, if it has longer range or is more acute
                if (range && existing.value < range) {
                    existing.source = source;
                    existing.value = range;
                }
                if (this.data.acuity && SenseRuleElement.isMoreAcute(this.data.acuity, existing.acuity)) {
                    existing.source = source;
                    existing.acuity = this.data.acuity;
                }
            } else {
                const sense: SenseData & { source: string } = {
                    label: this.label,
                    source: source,
                    type: this.data.selector,
                    value: "",
                };
                if (range) {
                    sense.value = range;
                }
                if (this.data.acuity) {
                    sense.acuity = this.data.acuity;
                }
                senses.push(sense);
            }
        } else {
            console.warn("SF2E | Sense requires at least a selector field and a label field or item name");
        }
    }
}

export interface SenseRuleElement {
    get actor(): CharacterSF2e | FamiliarSF2e;

    data: RuleElementData & {
        acuity?: SenseAcuity;
        range?: string;
    };
}
