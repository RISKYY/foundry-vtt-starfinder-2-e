import { RuleElementSF2e } from "../rule-element";
import { RuleElementSynthetics, WeaponPotencySF2e } from "../rules-data-definitions";
import { WeaponSF2e } from "@item";
import { ActorType } from "@actor/data";

/**
 * @category RuleElement
 */
export class WeaponPotencyRuleElement extends RuleElementSF2e {
    protected static override validActorTypes: ActorType[] = ["character", "npc"];

    override onBeforePrepareData(_actorData: unknown, { weaponPotency }: RuleElementSynthetics) {
        if (this.ignored) return;

        const selector = this.resolveInjectedProperties(this.data.selector);
        const { item } = this;
        const potencyValue = this.data.value ?? (item instanceof WeaponSF2e ? item.data.data.potencyRune.value : 0);
        const value = this.resolveValue(potencyValue);
        if (selector && typeof value === "number") {
            const potency: WeaponPotencySF2e = { label: this.label, bonus: value };
            if (this.data.predicate) {
                potency.predicate = this.data.predicate;
            }
            weaponPotency[selector] = (weaponPotency[selector] || []).concat(potency);
        } else {
            console.warn("SF2E | Weapon potency requires at least a selector field and a non-empty value field");
        }
    }
}
