import { RuleElementSF2e } from "../rule-element";

/**
 * @category RuleElement
 */
export class PF2TokenImageRuleElement extends RuleElementSF2e {
    override onAfterPrepareData() {
        const value = this.data.value;

        if (!value) {
            console.warn("SF2E System | Token Image requires a non-empty value field");
            return;
        }

        mergeObject(this.actor.overrides, { token: { img: value } });
    }
}
