import { RuleElementSF2e } from "../rule-element";
import { RuleElementSynthetics, StrikingSF2e } from "../rules-data-definitions";
import { CharacterData, NPCData } from "@actor/data";
import { getStrikingDice } from "@item/runes";
import { WeaponSF2e } from "@item";

/**
 * @category RuleElement
 */
export class PF2StrikingRuleElement extends RuleElementSF2e {
    override onBeforePrepareData(_actorData: CharacterData | NPCData, { striking }: RuleElementSynthetics) {
        const selector = this.resolveInjectedProperties(this.data.selector);
        const strikingValue =
            "value" in this.data
                ? this.data.value
                : this.item instanceof WeaponSF2e
                ? getStrikingDice(this.item.data.data)
                : 0;
        const value = this.resolveValue(strikingValue);
        if (selector && typeof value === "number") {
            const additionalData: StrikingSF2e = { label: this.label, bonus: value };
            if (this.data.predicate) {
                additionalData.predicate = this.data.predicate;
            }
            striking[selector] = (striking[selector] || []).concat(additionalData);
        } else {
            console.warn("SF2E | Striking requires at least a selector field and a non-empty value field");
        }
    }
}
