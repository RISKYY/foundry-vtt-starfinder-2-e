import { DamageDiceSF2e, ModifierSF2e } from "../modifiers";
import { RollNoteSF2e } from "../notes";
import { WeaponSF2e } from "@item";
import { PredicateSF2e, RawPredicate } from "@system/predication";

export type RuleElementSource = {
    key: string;
    data?: unknown;
    selector?: string;
    value?: RuleValue | BracketedValue;
    scope?: string;
    label?: string;
    slug?: string;
    predicate?: RawPredicate;
    /** The place in order of application (ascending), among an actor's list of rule elements */
    priority?: number;
    ignored?: boolean;
    requiresInvestment?: boolean;
};

export interface RuleElementData extends RuleElementSource {
    key: string;
    data?: any;
    selector?: string;
    value?: RuleValue | BracketedValue;
    scope?: string;
    label: string;
    slug?: string;
    predicate?: PredicateSF2e;
    priority: number;
    ignored: boolean;
}

export type RuleValue = string | number | boolean | object | null;

export interface Bracket<T extends object | number | string> {
    start?: number;
    end?: number;
    value: T;
}

export interface BracketedValue<T extends object | number | string = object | number | string> {
    field?: string;
    brackets: Bracket<T>[];
}

export interface WeaponPotencySF2e {
    label: string;
    bonus: number;
    predicate?: PredicateSF2e;
}

export interface StrikingSF2e {
    label: string;
    bonus: number;
    predicate?: PredicateSF2e;
}

export interface MultipleAttackPenaltySF2e {
    label: string;
    penalty: number;
    predicate?: PredicateSF2e;
}

export interface RuleElementSynthetics {
    damageDice: Record<string, DamageDiceSF2e[]>;
    statisticsModifiers: Record<string, ModifierSF2e[]>;
    strikes: Embedded<WeaponSF2e>[];
    rollNotes: Record<string, RollNoteSF2e[]>;
    weaponPotency: Record<string, WeaponPotencySF2e[]>;
    striking: Record<string, StrikingSF2e[]>;
    multipleAttackPenalties: Record<string, MultipleAttackPenaltySF2e[]>;
}
