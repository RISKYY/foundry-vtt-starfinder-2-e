import { FeatSF2e } from "@item/feat";
import { FeatSheetData, ItemSheetDataSF2e } from "../sheet/data-types";
import { ItemSheetSF2e } from "../sheet/base";

export class FeatSheetSF2e extends ItemSheetSF2e<FeatSF2e> {
    override getData(): FeatSheetData {
        const data: ItemSheetDataSF2e<FeatSF2e> = super.getData();
        return {
            ...data,
            featTypes: CONFIG.SF2E.featTypes,
            actionTypes: CONFIG.SF2E.actionTypes,
            actionsNumber: CONFIG.SF2E.actionsNumber,
            categories: CONFIG.SF2E.actionCategories,
            damageTypes: { ...CONFIG.SF2E.damageTypes, ...CONFIG.SF2E.healingTypes },
            prerequisites: JSON.stringify(this.item.data.data.prerequisites?.value ?? []),
            rarities: this.prepareOptions(CONFIG.SF2E.rarityTraits, { value: [data.data.traits.rarity.value] }),
            traits: this.prepareOptions(CONFIG.SF2E.featTraits, data.data.traits),
        };
    }
}
