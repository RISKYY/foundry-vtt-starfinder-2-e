import { ItemSystemData } from "@item/data/base";
import { BaseNonPhysicalItemData, BaseNonPhysicalItemSource } from "@item/data/non-physical";
import type { KitSF2e } from ".";

export type KitSource = BaseNonPhysicalItemSource<"kit", KitSystemData>;

export class KitData extends BaseNonPhysicalItemData<KitSF2e> {
    static override DEFAULT_ICON: ImagePath = "systems/sf2e/icons/default-icons/feat.svg";
}

export interface KitData extends Omit<KitSource, "effects" | "flags"> {
    type: KitSource["type"];
    data: KitSource["data"];
    readonly _source: KitSource;
}

export interface KitEntryData {
    pack?: string;
    id: string;
    img: ImagePath;
    quantity: number;
    name: string;
    isContainer: boolean;
    items?: { [key: number]: KitEntryData };
}

export interface KitSystemData extends ItemSystemData {
    items: Record<string, KitEntryData>;
}
