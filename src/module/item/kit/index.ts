import { ActorSF2e } from "@actor/index";
import { ContainerSF2e, ItemSF2e, PhysicalItemSF2e } from "@item/index";
import { ErrorSF2e } from "@module/utils";
import { KitData, KitEntryData } from "./data";

const SYSTEM_EQUIPMENT_PACK_ID = "sf2e.equipment-srd";

export class KitSF2e extends ItemSF2e {
    static override get schema(): typeof KitData {
        return KitData;
    }

    get entries() {
        return Object.values(this.data.data.items);
    }

    /** Inflate this kit and add its items to the provided actor */
    async dumpContents(actor: ActorSF2e, kitEntries?: KitEntryData[], containerId = ""): Promise<void> {
        kitEntries ??= this.entries;
        const equipmentPack = await game.packs.get(SYSTEM_EQUIPMENT_PACK_ID)?.getDocuments();
        if (!equipmentPack) {
            throw ErrorSF2e("Failed to acquire system equipment compendium");
        }

        const promises = kitEntries.map(async (kitEntry): Promise<PhysicalItemSF2e | null> => {
            const inflatedItem = await (async () => {
                if (kitEntry.pack === SYSTEM_EQUIPMENT_PACK_ID) {
                    return equipmentPack.find((item) => item.id === kitEntry.id);
                } else if (kitEntry.pack) {
                    return game.packs.get(kitEntry.pack)?.getDocument(kitEntry.id);
                } else {
                    return game.items.get(kitEntry.id);
                }
            })();

            if (inflatedItem instanceof KitSF2e) {
                await inflatedItem.dumpContents(actor);
                // Filtered out just before item creation
                return null;
            }

            if (!(inflatedItem instanceof PhysicalItemSF2e)) {
                throw ErrorSF2e(`${kitEntry.pack ?? "World item"} ${kitEntry.name}} (${kitEntry.id}) not found`);
            }

            inflatedItem.data.update({
                "data.quantity.value": kitEntry.quantity,
                "data.containerId.value": containerId,
            });

            // Get items in this container and inflate any items that might be contained inside
            if (inflatedItem instanceof ContainerSF2e && kitEntry.items) {
                const container = await ContainerSF2e.create(inflatedItem.toObject(), { parent: actor });
                if (container) {
                    await this.dumpContents(actor, Object.values(kitEntry.items), container.id);
                }
                return null;
            }

            return inflatedItem;
        });

        const createData = (await Promise.all(promises))
            .flat()
            .filter((item): item is PhysicalItemSF2e => item instanceof PhysicalItemSF2e)
            .map((item) => item.toObject());
        if (createData.length > 0) {
            await actor.createEmbeddedDocuments("Item", createData);
        }
    }
}

export interface KitSF2e {
    readonly data: KitData;
}
