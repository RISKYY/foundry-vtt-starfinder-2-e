import { ItemSF2e } from "../index";
import { FormulaData } from "./data";

export class FormulaSF2e extends ItemSF2e {
    static override get schema(): typeof FormulaData {
        return FormulaData;
    }
}

export interface FormulaSF2e {
    readonly data: FormulaData;
}
