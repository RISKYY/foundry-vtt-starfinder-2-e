import { FormulaSF2e, ItemSF2e } from "@item";
import { ItemSheetSF2e } from "@item/sheet/base";
import { ItemSheetDataSF2e } from "@item/sheet/data-types";

export class FormulaSheetSF2e extends ItemSheetSF2e<FormulaSF2e> {
    override getData() {
        const data: ItemSheetDataSF2e<FormulaSF2e> = super.getData();
        (async () => await this.updateData(data))();
        return { ...data };
    }

    async updateData(data: ItemSheetDataSF2e<FormulaSF2e>) {
        if (data.item.data.craftedItem.uuid) {
            try {
                const compendiumItem = await fromUuid(data.item.data.craftedItem.uuid);
                if (compendiumItem) {
                    const item = compendiumItem as ItemSF2e;
                    data.item.img = item.img;
                    data.item.name = game.i18n.format("SF2E.FormulaSheet.NamePrefix", { name: item.name });
                    data.item.data.description.value = item.description;
                }
            } catch {
                data.item.img = "systems/sf2e/icons/default-icons/lore.svg";
                data.item.name = game.i18n.localize("SF2E.FormulaSheet.NameUnknown");
                data.item.data.description.value = game.i18n.localize("SF2E.FormulaSheet.DescriptionUnknown");
            }
        } else {
            data.item.img = "systems/sf2e/icons/default-icons/lore.svg";
            data.item.name = game.i18n.localize("SF2E.FormulaSheet.NameEmpty");
            data.item.data.description.value = "";
        }
    }
}
