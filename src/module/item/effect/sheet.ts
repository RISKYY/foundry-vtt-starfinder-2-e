import { EffectSF2e } from ".";
import { ItemSheetDataSF2e } from "../sheet/data-types";
import { ItemSheetSF2e } from "../sheet/base";
import { RuleElementSource } from "@module/rules/rules-data-definitions";

export class EffectSheetSF2e extends ItemSheetSF2e<EffectSF2e> {
    override getData(): EffectSheetData {
        const data: ItemSheetDataSF2e<EffectSF2e> = super.getData();
        type EffectTargetSource = RuleElementSource & { scope: string };
        const effectTargetRules = data.item.data.rules.filter(
            (rule): rule is EffectTargetSource => rule.key.endsWith("EffectTarget") && typeof rule.scope === "string"
        );
        const scopes = new Set(effectTargetRules.map((rule) => rule.scope));
        const targets = (this.actor?.items.contents ?? [])
            .filter((item) => scopes.has(item.type))
            .map((item) => ({ id: item.id, name: item.name }));

        return { ...data, targets };
    }
}

interface EffectSheetData extends ItemSheetDataSF2e<EffectSF2e> {
    targets: { id: string; name: string }[];
}
