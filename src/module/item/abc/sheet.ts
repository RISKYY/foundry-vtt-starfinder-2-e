import { AbilityString } from "@actor/data";
import { ABCFeatureEntryData } from "@item/abc/data";
import { FeatType } from "@item/feat/data";
import { AncestrySF2e, BackgroundSF2e, ClassSF2e, FeatSF2e, ItemSF2e } from "@item/index";
import { LocalizeSF2e } from "@system/localize";
import { ItemSheetSF2e } from "../sheet/base";
import { ABCSheetData } from "../sheet/data-types";

type ABCItem = AncestrySF2e | BackgroundSF2e | ClassSF2e;

export abstract class ABCSheetSF2e<TItem extends ABCItem> extends ItemSheetSF2e<TItem> {
    static override get defaultOptions() {
        return {
            ...super.defaultOptions,
            scrollY: [".item-details"],
            dragDrop: [{ dropSelector: ".item-details" }],
        };
    }

    override getData(): ABCSheetData<TItem> {
        const itemType = this.item.type;

        return {
            ...this.getBaseData(),
            hasSidebar: itemType !== "class",
            sidebarTemplate: () => `systems/sf2e/templates/items/${itemType}-sidebar.html`,
            hasDetails: true,
            detailsTemplate: () => `systems/sf2e/templates/items/${itemType}-details.html`,
        };
    }

    protected getLocalizedAbilities(traits: { value: AbilityString[] }): { [key: string]: string } {
        if (traits !== undefined && traits.value) {
            if (traits.value.length === 6) return { free: game.i18n.localize("SF2E.AbilityFree") };
            return Object.fromEntries(traits.value.map((x: AbilityString) => [x, CONFIG.SF2E.abilities[x]]));
        }

        return {};
    }

    /** Is the dropped feat or feature valid for the given section? */
    private isValidDrop(event: ElementDragEvent, feat: FeatSF2e): boolean {
        const validFeatTypes: FeatType[] = $(event.target).closest(".abc-list").data("valid-drops")?.split(" ") ?? [];
        if (validFeatTypes.includes(feat.featType.value)) {
            return true;
        }

        const goodTypes = validFeatTypes.map((featType) => game.i18n.localize(CONFIG.SF2E.featTypes[featType]));
        if (goodTypes.length === 1) {
            const badType = feat.featType.label;
            const warning = game.i18n.format(LocalizeSF2e.translations.SF2E.Item.ABC.InvalidDrop, {
                badType,
                goodType: goodTypes[0],
            });
            ui.notifications.warn(warning);
            return false;
        }

        // No feat/feature type restriction value, so let it through
        return true;
    }

    protected override async _onDrop(event: ElementDragEvent): Promise<void> {
        event.preventDefault();
        const dataString = event.dataTransfer?.getData("text/plain");
        const dropData = JSON.parse(dataString ?? "");
        const item = await ItemSF2e.fromDropData(dropData);

        if (!(item instanceof FeatSF2e) || !this.isValidDrop(event, item)) {
            return;
        }

        const entry: ABCFeatureEntryData = {
            pack: dropData.pack || undefined,
            id: dropData.id,
            img: item.data.img,
            name: item.name,
            level: item.data.data.level.value,
        };

        const items = this.item.data.data.items;
        const pathPrefix = "data.items";

        let id: string;
        do {
            id = randomID(5);
        } while (items[id]);

        await this.item.update({
            [`${pathPrefix}.${id}`]: entry,
        });
    }

    private removeItem(event: JQuery.ClickEvent) {
        event.preventDefault();
        const target = $(event.target).parents("li");
        const containerId = target.parents("[data-container-id]").data("containerId");
        let path = `-=${target.data("index")}`;
        if (containerId) {
            path = `${containerId}.items.${path}`;
        }

        this.item.update({
            [`data.items.${path}`]: null,
        });
    }

    override activateListeners(html: JQuery): void {
        super.activateListeners(html);
        html.on("click", "[data-action=remove]", (ev) => this.removeItem(ev));
    }
}
