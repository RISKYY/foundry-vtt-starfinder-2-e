import { FeatSF2e, ClassSF2e, ItemSF2e, ABCItemSF2e } from "@item/index";
import { AncestrySource, BackgroundSource, ClassSource, ItemSourceSF2e } from "@item/data";
import { ABCFeatureEntryData } from "@item/abc/data";
import { CharacterSF2e } from "@actor/index";
import type { FeatSource } from "@item/feat/data";
import { ErrorSF2e, sluggify } from "@module/utils";

export interface ABCManagerOptions {
    assurance?: string[];
}

export class AncestryBackgroundClassManager {
    static async addABCItem(
        source: AncestrySource | BackgroundSource | ClassSource,
        actor: CharacterSF2e,
        options?: ABCManagerOptions
    ): Promise<ItemSF2e[]> {
        switch (source.type) {
            case "ancestry": {
                await actor.ancestry?.delete();
                return this.addFeatures(source, actor, true, options);
            }
            case "background": {
                await actor.background?.delete();
                return this.addFeatures(source, actor, true, options);
            }
            case "class": {
                await actor.class?.delete();

                source._id = randomID(16);
                source.flags.sf2e ??= {};
                source.flags.sf2e.insertedClassFeaturesLevel = actor.level;

                const itemsToCreate = await this.getClassFeaturesForLevel(source, 0, actor.level);
                itemsToCreate.sort((featureA, featureB) => {
                    const levelA = featureA.data.level.value;
                    const levelB = featureB.data.level.value;
                    if (levelA > levelB) {
                        return 1;
                    } else if (levelA === levelB) {
                        return featureA.name > featureB.name ? 1 : -1;
                    } else {
                        return -1;
                    }
                });
                return actor.createEmbeddedDocuments("Item", [...itemsToCreate, source], { keepId: true });
            }
            default:
                throw ErrorSF2e("Invalid item type for ABC creation!");
        }
    }

    static async ensureClassFeaturesForLevel(
        classItem: ClassSF2e,
        actor: CharacterSF2e,
        minLevelInput?: number
    ): Promise<void> {
        const minLevel: number = minLevelInput ?? classItem.data.flags.sf2e?.insertedClassFeaturesLevel ?? 0;
        const classFeaturesToCreate = await this.getClassFeaturesForLevel(classItem, minLevel, actor.level);

        if (classFeaturesToCreate.length > 0) {
            await actor.createEmbeddedDocuments("Item", classFeaturesToCreate, { keepId: true, render: false });
            classItem.setFlag(game.system.id, "insertedClassFeaturesLevel", actor.level);
        }
    }

    static async getItemSource(packName: "sf2e.ancestries", name: string): Promise<AncestrySource>;
    static async getItemSource(packName: "sf2e.backgrounds", name: string): Promise<BackgroundSource>;
    static async getItemSource(packName: "sf2e.classes", name: string): Promise<ClassSource>;
    static async getItemSource(packName: "sf2e.feats-srd", name: string): Promise<FeatSource>;
    static async getItemSource(
        packName: "sf2e.ancestries" | "sf2e.backgrounds" | "sf2e.classes" | "sf2e.feats-srd",
        name: string
    ): Promise<AncestrySource | BackgroundSource | ClassSource | FeatSource> {
        const slug = sluggify(name);
        const pack = game.packs.get<CompendiumCollection<ABCItemSF2e>>(packName, { strict: true });
        const docs = await pack.getDocuments({ "data.slug": { $in: [slug] } });
        if (docs.length === 1) {
            return docs[0].toObject();
        } else {
            throw ErrorSF2e(`Cannot find '${name}' in pack '${packName}'`);
        }
    }

    protected static async getClassFeaturesForLevel(
        item: ClassSF2e | ClassSource,
        minLevel: number,
        actorLevel: number
    ): Promise<FeatSource[]> {
        const classFeatsToAdd = item instanceof ClassSF2e ? item.data.data.items : item.data.items;
        const itemId = item instanceof ClassSF2e ? item.id : item._id;

        if (minLevel >= actorLevel) {
            // no need to do anything, since we've seen it all
            return [];
        }

        const featuresToAdd = Object.values(classFeatsToAdd).filter(
            (entryData) => actorLevel >= entryData.level && entryData.level > minLevel
        );

        return this.getFeatures(featuresToAdd, itemId);
    }

    protected static async getFeatures(entries: ABCFeatureEntryData[], locationId: string): Promise<FeatSource[]> {
        const feats: FeatSource[] = [];
        if (entries.length > 0) {
            // Get feats from a compendium pack
            const packEntries = entries.filter((entry) => !!entry.pack);
            if (packEntries.length > 0) {
                const compendiumFeats = await this.getFromCompendium(packEntries);
                feats.push(
                    ...compendiumFeats.map((feat) => {
                        if (feat instanceof FeatSF2e) {
                            const featSource = feat.toObject();
                            featSource._id = randomID(16);
                            featSource.data.location = locationId;
                            // There are some classes (e.g. Investigator) where multiple active effects have the same id
                            featSource.effects.forEach((effect) => {
                                effect._id = randomID(16);
                            });
                            return featSource;
                        } else {
                            throw ErrorSF2e("Invalid item type referenced in ABCFeatureEntryData");
                        }
                    })
                );
            }
            // Get feats from the game.items collection
            const gameEntries = entries.filter((entry) => !entry.pack);
            feats.push(
                ...gameEntries.map((entry) => {
                    const item = game.items.get(entry.id);
                    if (item instanceof FeatSF2e) {
                        const itemData = item.toObject();
                        itemData._id = randomID(16);
                        return itemData;
                    } else {
                        throw ErrorSF2e("Invalid item type referenced in ABCFeatureEntryData");
                    }
                })
            );
        }
        return feats;
    }

    protected static async getFromCompendium(entries: ABCFeatureEntryData[]): Promise<FeatSF2e[]> {
        const feats: FeatSF2e[] = [];
        // Entries can be from different packs
        const packs = new Set(entries.map((entry) => entry.pack!));
        for await (const pack of packs) {
            // Only fetch feats that are in this pack
            const entryIds = entries.filter((entry) => entry.pack === pack).map((entry) => entry.id);
            feats.push(
                ...(await game.packs
                    .get<CompendiumCollection<FeatSF2e>>(pack, { strict: true })
                    // Use NeDB query to fetch all needed feats in one transaction
                    .getDocuments({ _id: { $in: entryIds } }))
            );
        }
        return feats;
    }

    protected static async addFeatures(
        itemSource: AncestrySource | BackgroundSource,
        actor: CharacterSF2e,
        createSource = false,
        options?: ABCManagerOptions
    ): Promise<ItemSF2e[]> {
        const itemsToCreate: ItemSourceSF2e[] = [];
        if (createSource) {
            itemSource._id = randomID(16);
            itemsToCreate.push(itemSource);
        }
        const entriesData = Object.values(itemSource.data.items);
        itemsToCreate.push(...(await this.getFeatures(entriesData, itemSource._id)));

        if (options?.assurance) {
            for (const skill of options.assurance) {
                const index = itemsToCreate.findIndex((item) => item.data.slug === "assurance");
                if (index > -1) {
                    const location = (itemsToCreate[index] as FeatSource).data.location;
                    itemsToCreate[index] = await this.getItemSource("sf2e.feats-srd", `Assurance (${skill})`);
                    itemsToCreate[index]._id = randomID(16);
                    (itemsToCreate[index] as FeatSource).data.location = location;
                }
            }
        }
        return actor.createEmbeddedDocuments("Item", itemsToCreate, { keepId: true });
    }
}
