import {
    BasePhysicalItemData,
    BasePhysicalItemSource,
    MagicItemSystemData,
    PhysicalItemTraits,
} from "@item/physical/data";
import { ZeroToFour } from "@module/data";
import type { LocalizeSF2e } from "@module/system/localize";
import type { ArmorSF2e } from ".";

export type ArmorSource = BasePhysicalItemSource<"armor", ArmorSystemData>;

export class ArmorData extends BasePhysicalItemData<ArmorSF2e> {
    static override DEFAULT_ICON: ImagePath = "systems/sf2e/icons/default-icons/armor.svg";
}

export interface ArmorData extends Omit<ArmorSource, "effects" | "flags"> {
    type: ArmorSource["type"];
    data: ArmorSource["data"];
    readonly _source: ArmorSource;
}

export type ArmorTrait = keyof ConfigSF2e["SF2E"]["armorTraits"];
type ArmorTraits = PhysicalItemTraits<ArmorTrait>;

export type ArmorCategory = keyof ConfigSF2e["SF2E"]["armorTypes"];
export type ArmorGroup = keyof ConfigSF2e["SF2E"]["armorGroups"];
export type BaseArmorType = keyof typeof LocalizeSF2e.translations.SF2E.Item.Armor.Base;
export type ResilientRuneType = "" | "resilient" | "greaterResilient" | "majorResilient";

interface ArmorSystemData extends MagicItemSystemData {
    traits: ArmorTraits;
    armor: {
        value: number;
    };
    armorType: {
        value: ArmorCategory;
    };
    baseItem: BaseArmorType | null;

    group: {
        value: ArmorGroup | null;
    };
    strength: {
        value: number;
    };
    dex: {
        value: number;
    };
    check: {
        value: number;
    };
    speed: {
        value: number;
    };
    potencyRune: {
        value: ZeroToFour;
    };
    resiliencyRune: {
        value: ResilientRuneType | "";
    };
    propertyRune1: {
        value: string;
    };
    propertyRune2: {
        value: string;
    };
    propertyRune3: {
        value: string;
    };
    propertyRune4: {
        value: string;
    };
}

export const ARMOR_CATEGORIES = ["unarmored", "light", "medium", "heavy", "powered"] as const;
