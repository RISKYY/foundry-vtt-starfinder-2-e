import { ItemSheetSF2e } from "../sheet/base";
import { PhysicalItemSF2e } from "@item/physical";
import { ItemSheetDataSF2e } from "@item/sheet/data-types";

export class PhysicalItemSheetSF2e<TItem extends PhysicalItemSF2e = PhysicalItemSF2e> extends ItemSheetSF2e<TItem> {
    /** Show the identified data for editing purposes */
    override getData(): ItemSheetDataSF2e<TItem> {
        const sheetData: ItemSheetDataSF2e<TItem> = super.getData();

        // Set the source item data for editing
        const identifiedData = this.item.getMystifiedData("identified", { source: true });
        mergeObject(sheetData.item, identifiedData, { insertKeys: false, insertValues: false });

        return sheetData;
    }

    /** Normalize nullable fields to actual `null`s */
    protected override async _updateObject(event: Event, formData: Record<string, unknown>): Promise<void> {
        const propertyPaths = [
            "data.baseItem",
            "data.preciousMaterial.value",
            "data.preciousMaterialGrade.value",
            "data.group.value",
        ];
        for (const path of propertyPaths) {
            if (formData[path] === "") formData[path] = null;
        }

        super._updateObject(event, formData);
    }
}
