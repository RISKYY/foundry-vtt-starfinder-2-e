import { CreatureTrait } from "@actor/creature/data";
import type { ItemSF2e } from "@item/base";
import { FeatTrait } from "@item/feat/data";
import { SpellTrait } from "@item/spell/data";
import type { ActiveEffectSF2e } from "@module/active-effect";
import { RuleElementSource } from "@module/rules/rules-data-definitions";
import { DocumentSchemaRecord, Rarity, ValuesList } from "@module/data";
import { ItemType } from ".";
import { PhysicalItemTrait } from "../physical/data";

export interface BaseItemSourceSF2e<
    TItemType extends ItemType = ItemType,
    TSystemData extends ItemSystemData = ItemSystemData
> extends foundry.data.ItemSource {
    type: TItemType;
    data: TSystemData;
    flags: DeepPartial<ItemFlagsSF2e>;
}

export abstract class BaseItemDataSF2e<TItem extends ItemSF2e = ItemSF2e> extends foundry.data.ItemData<
    TItem,
    ActiveEffectSF2e
> {
    /** Is this physical item data? */
    abstract isPhysical: boolean;
}

export interface BaseItemDataSF2e extends Omit<BaseItemSourceSF2e, "effects"> {
    type: BaseItemSourceSF2e["type"];
    data: BaseItemSourceSF2e["data"];
    flags: ItemFlagsSF2e;

    readonly _source: BaseItemSourceSF2e;
}

export type ItemTrait = CreatureTrait | FeatTrait | PhysicalItemTrait | SpellTrait;

export type ActionType = keyof ConfigSF2e["SF2E"]["actionTypes"];

export interface ItemTraits<T extends ItemTrait = ItemTrait> extends ValuesList<T> {
    rarity: { value: Rarity };
}

export interface ItemFlagsSF2e extends foundry.data.ItemFlags {
    sf2e: {
        [key: string]: unknown;
    };
}

export interface ItemLevelData {
    level: {
        value: number;
    };
}

export interface ItemSystemData {
    description: {
        value: string;
    };
    source: {
        value: string;
    };
    traits: ItemTraits;
    options?: {
        value: string[];
    };
    rules: RuleElementSource[];
    slug: string | null;
    schema: DocumentSchemaRecord;
}
