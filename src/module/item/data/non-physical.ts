import type { ItemSF2e } from "@item/base";
import { BaseItemDataSF2e, BaseItemSourceSF2e, ItemSystemData } from "./base";

export type NonPhysicalItemType =
    | "action"
    | "ancestry"
    | "background"
    | "class"
    | "condition"
    | "effect"
    | "feat"
    | "kit"
    | "lore"
    | "martial"
    | "melee"
    | "spell"
    | "spellcastingEntry"
    | "formula";

export type BaseNonPhysicalItemSource<
    TItemType extends NonPhysicalItemType = NonPhysicalItemType,
    TSystemData extends ItemSystemData = ItemSystemData
> = BaseItemSourceSF2e<TItemType, TSystemData>;

export class BaseNonPhysicalItemData<TItem extends ItemSF2e> extends BaseItemDataSF2e<TItem> {
    readonly isPhysical: false = false;
}

export interface BaseNonPhysicalItemData<TItem extends ItemSF2e>
    extends Omit<BaseNonPhysicalItemSource, "effects" | "flags"> {
    type: BaseNonPhysicalItemSource["type"];
    data: BaseNonPhysicalItemSource["data"];

    readonly document: TItem;
}
