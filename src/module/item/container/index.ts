import { EquipmentTrait } from "@item/equipment/data";
import { PhysicalItemSF2e } from "@item/physical";
import { ContainerData } from "./data";

export class ContainerSF2e extends PhysicalItemSF2e {
    /** This container's contents, reloaded every data preparation cycle */
    contents: Collection<Embedded<PhysicalItemSF2e>> = new Collection();

    static override get schema(): typeof ContainerData {
        return ContainerData;
    }

    /** Reload this container's contents following Actor embedded-document preparation */
    override prepareSiblingData(this: Embedded<ContainerSF2e>): void {
        this.contents = new Collection(
            this.actor.physicalItems.filter((item) => item.container?.id === this.id).map((item) => [item.id, item])
        );
    }

    override getChatData(this: Embedded<ContainerSF2e>, htmlOptions: EnrichHTMLOptions = {}): Record<string, unknown> {
        const data = this.data.data;
        const traits = this.traitChatData(CONFIG.SF2E.equipmentTraits);

        return this.processChatData(htmlOptions, { ...data, traits });
    }
}

export interface ContainerSF2e {
    readonly data: ContainerData;

    get traits(): Set<EquipmentTrait>;
}
