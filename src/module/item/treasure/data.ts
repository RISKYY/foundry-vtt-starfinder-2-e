import { BasePhysicalItemData, BasePhysicalItemSource, PhysicalSystemData } from "@item/physical/data";
import { TreasureSF2e } from ".";

export type TreasureSource = BasePhysicalItemSource<"treasure", TreasureSystemData>;

export class TreasureData extends BasePhysicalItemData<TreasureSF2e> {
    static override DEFAULT_ICON: ImagePath = "systems/sf2e/icons/default-icons/treasure.svg";
}

export interface TreasureData extends Omit<TreasureSource, "effects" | "flags"> {
    type: TreasureSource["type"];
    data: TreasureSource["data"];
    readonly _source: TreasureSource;

    isInvested: null;
}

export interface TreasureSystemData extends PhysicalSystemData {
    denomination: {
        value: "pp" | "gp" | "sp" | "cp";
    };
    value: {
        value: number;
    };
}
