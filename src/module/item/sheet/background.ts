import { ABCSheetSF2e } from "../abc/sheet";
import { BackgroundSF2e } from "@item/background";
import { BackgroundSheetData } from "./data-types";

export class BackgroundSheetSF2e extends ABCSheetSF2e<BackgroundSF2e> {
    override getData(): BackgroundSheetData {
        const data = super.getData();
        const itemData = data.item;

        return {
            ...data,
            rarities: this.prepareOptions(CONFIG.SF2E.rarityTraits, { value: [itemData.data.traits.rarity.value] }),
            trainedSkills: this.prepareOptions(CONFIG.SF2E.skills, itemData.data.trainedSkills),
            selectedBoosts: Object.fromEntries(
                Object.entries(itemData.data.boosts).map(([k, b]) => [k, this.getLocalizedAbilities(b)])
            ),
        };
    }
}
