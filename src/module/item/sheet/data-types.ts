import { ItemSF2e } from "@item";
import { ABCFeatureEntryData } from "@item/abc/data";
import { AncestrySF2e } from "@item/ancestry";
import { BackgroundSF2e } from "@item/background";
import { ClassSF2e } from "@item/class";
import { FeatSF2e } from "@item/feat";
import { SpellSF2e } from "@item/spell";

export interface SheetOption {
    value: string;
    label: string;
    selected: boolean;
}

export type SheetOptions = Record<string, SheetOption>;

export interface SheetSelections {
    value: (string | number)[];
    custom?: string;
}

interface ActiveEffectSummary {
    id: string;
    iconPath: string | null;
    name: string;
    duration: string;
    enabled: boolean;
}

export interface AESheetData {
    showAEs: boolean;
    canEdit: boolean;
    effects: ActiveEffectSummary[];
}

export interface ItemSheetDataSF2e<TItem extends ItemSF2e> extends ItemSheetData<TItem> {
    hasSidebar: boolean;
    hasDetails: boolean;
    sidebarTemplate?: () => string;
    detailsTemplate?: () => string;
    item: TItem["data"];
    data: TItem["data"]["data"];
    user: { isGM: boolean };
    enabledRulesUI: boolean;
    activeEffects: AESheetData;
}

export interface ABCSheetData<TItem extends AncestrySF2e | BackgroundSF2e | ClassSF2e>
    extends ItemSheetDataSF2e<TItem> {
    hasDetails: true;
}

export interface AncestrySheetData extends ABCSheetData<AncestrySF2e> {
    selectedBoosts: Record<string, Record<string, string>>;
    selectedFlaws: Record<string, Record<string, string>>;
    rarities: SheetOptions;
    sizes: SheetOptions;
    traits: SheetOptions;
    languages: SheetOptions;
    additionalLanguages: SheetOptions;
}

export interface BackgroundSheetData extends ABCSheetData<BackgroundSF2e> {
    rarities: SheetOptions;
    trainedSkills: SheetOptions;
    selectedBoosts: Record<string, Record<string, string>>;
}

export interface ClassSheetData extends ABCSheetData<ClassSF2e> {
    rarities: SheetOptions;
    items: { key: string; item: ABCFeatureEntryData }[];
    skills: typeof CONFIG.SF2E.skills;
    proficiencyChoices: typeof CONFIG.SF2E.proficiencyLevels;
    selectedKeyAbility: Record<string, string>;
    ancestryTraits: SheetOptions;
    trainedSkills: SheetOptions;
    ancestryFeatLevels: SheetOptions;
    classFeatLevels: SheetOptions;
    generalFeatLevels: SheetOptions;
    skillFeatLevels: SheetOptions;
    skillIncreaseLevels: SheetOptions;
    abilityBoostLevels: SheetOptions;
}

export interface FeatSheetData extends ItemSheetDataSF2e<FeatSF2e> {
    featTypes: ConfigSF2e["SF2E"]["featTypes"];
    actionTypes: ConfigSF2e["SF2E"]["actionTypes"];
    actionsNumber: ConfigSF2e["SF2E"]["actionsNumber"];
    damageTypes: ConfigSF2e["SF2E"]["damageTypes"] & ConfigSF2e["SF2E"]["healingTypes"];
    categories: ConfigSF2e["SF2E"]["actionCategories"];
    prerequisites: string;
    rarities: SheetOptions;
    traits: SheetOptions;
}

export interface SpellSheetData extends ItemSheetDataSF2e<SpellSF2e> {
    levelLabel: string;
    magicSchools: ConfigSF2e["SF2E"]["magicSchools"];
    spellCategories: ConfigSF2e["SF2E"]["spellCategories"];
    spellLevels: ConfigSF2e["SF2E"]["spellLevels"];
    spellTypes: ConfigSF2e["SF2E"]["spellTypes"];
    magicTraditions: SheetOptions;
    damageCategories: ConfigSF2e["SF2E"]["damageCategories"];
    damageSubtypes: ConfigSF2e["SF2E"]["damageSubtypes"];
    spellComponents: string[];
    traits: SheetOptions;
    rarities: SheetOptions;
    areaSizes: ConfigSF2e["SF2E"]["areaSizes"];
    areaTypes: ConfigSF2e["SF2E"]["areaTypes"];
    spellScalingModes: ConfigSF2e["SF2E"]["spellScalingModes"];
}
