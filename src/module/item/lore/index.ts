import { ItemSF2e } from "@item/base";
import { LoreData } from "./data";

export class LoreSF2e extends ItemSF2e {
    static override get schema(): typeof LoreData {
        return LoreData;
    }
}

export interface LoreSF2e {
    readonly data: LoreData;
}
