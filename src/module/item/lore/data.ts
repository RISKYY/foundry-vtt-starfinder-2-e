import { ItemSystemData } from "@item/data/base";
import { BaseNonPhysicalItemData, BaseNonPhysicalItemSource } from "@item/data/non-physical";
import { ZeroToFour } from "@module/data";
import type { LoreSF2e } from ".";

export type LoreSource = BaseNonPhysicalItemSource<"lore", LoreSystemData>;

export class LoreData extends BaseNonPhysicalItemData<LoreSF2e> {
    static override DEFAULT_ICON: ImagePath = "systems/sf2e/icons/default-icons/lore.svg";
}

export interface LoreData extends Omit<LoreSource, "effects" | "flags"> {
    type: LoreSource["type"];
    data: LoreSource["data"];
    readonly _source: LoreSource;
}

interface LoreSystemData extends ItemSystemData {
    mod: {
        value: number;
    };
    proficient: {
        value: ZeroToFour;
    };
    variants?: Record<string, { label: string; options: string }>;
}
