import {
    ActivatedEffectData,
    BasePhysicalItemData,
    BasePhysicalItemSource,
    PhysicalItemTraits,
    PhysicalSystemData,
} from "@item/physical/data";
import { SpellSource } from "@item/spell/data";
import type { ConsumableSF2e } from ".";

export type ConsumableSource = BasePhysicalItemSource<"consumable", ConsumableSystemData>;

export class ConsumableData extends BasePhysicalItemData<ConsumableSF2e> {
    static override DEFAULT_ICON: ImagePath = "systems/sf2e/icons/default-icons/consumable.svg";
}

export interface ConsumableData extends Omit<ConsumableSource, "effects" | "flags"> {
    type: ConsumableSource["type"];
    data: ConsumableSource["data"];
    readonly _source: ConsumableSource;
}

export type ConsumableType = keyof ConfigSF2e["SF2E"]["consumableTypes"];
export type ConsumableTrait = keyof ConfigSF2e["SF2E"]["consumableTraits"];
type ConsumableTraits = PhysicalItemTraits<ConsumableTrait>;

interface ConsumableSystemData extends PhysicalSystemData, ActivatedEffectData {
    traits: ConsumableTraits;

    consumableType: {
        value: ConsumableType;
    };
    uses: {
        value: number;
        max: number;
        per: any;
        autoUse: boolean;
        autoDestroy: boolean;
    };
    charges: {
        value: number;
        max: number;
    };
    consume: {
        value: string;
        _deprecated: boolean;
    };
    autoUse: {
        value: boolean;
    };
    autoDestroy: {
        value: boolean;
        _deprecated: boolean;
    };
    spell: {
        data?: SpellSource | null;
        heightenedLevel?: number | null;
    };
}
