import { SkillAbbreviation } from "@actor/creature/data";
import { AbilityString } from "@actor/data/base";
import { ABCSystemData } from "@item/abc/data";
import { BaseNonPhysicalItemData, BaseNonPhysicalItemSource } from "@item/data/non-physical";
import { BackgroundSF2e } from ".";

export type BackgroundSource = BaseNonPhysicalItemSource<"background", BackgroundSystemData>;

export class BackgroundData extends BaseNonPhysicalItemData<BackgroundSF2e> {
    static override DEFAULT_ICON: ImagePath = "systems/sf2e/icons/default-icons/background.svg";
}

export interface BackgroundData extends Omit<BackgroundSource, "effects" | "flags"> {
    type: BackgroundSource["type"];
    data: BackgroundSource["data"];
    readonly _source: BackgroundSource;
}

interface BackgroundSystemData extends ABCSystemData {
    boosts: { [key: string]: { value: AbilityString[] } };
    trainedLore: string;
    trainedSkills: {
        value: SkillAbbreviation[];
    };
}
