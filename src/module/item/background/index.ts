import { CharacterSF2e } from "@actor";
import { OneToFour } from "@module/data";
import { ABCItemSF2e } from "../abc";
import { BackgroundData } from "./data";

export class BackgroundSF2e extends ABCItemSF2e {
    static override get schema(): typeof BackgroundData {
        return BackgroundData;
    }

    override prepareActorData(this: Embedded<BackgroundSF2e>): void {
        if (!(this.actor instanceof CharacterSF2e)) {
            console.error("Only a character can have a background");
            return;
        }

        const { trainedSkills } = this.data.data;
        if (trainedSkills.value.length === 1) {
            const key = trainedSkills.value[0];
            const skill = this.actor.data.data.skills[key];
            skill.rank = Math.max(skill.rank, 1) as OneToFour;
        }
    }
}

export interface BackgroundSF2e {
    readonly data: BackgroundData;
}
