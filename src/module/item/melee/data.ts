import { ItemSystemData } from "@item/data/base";
import { BaseNonPhysicalItemData, BaseNonPhysicalItemSource } from "@item/data/non-physical";
import type { MeleeSF2e } from ".";

export type MeleeSource = BaseNonPhysicalItemSource<"melee", MeleeSystemData>;

export class MeleeData extends BaseNonPhysicalItemData<MeleeSF2e> {
    static override DEFAULT_ICON: ImagePath = "systems/sf2e/icons/default-icons/melee.svg";
}

export interface MeleeData extends Omit<MeleeSource, "effects" | "flags"> {
    type: MeleeSource["type"];
    data: MeleeSource["data"];
    readonly _source: MeleeSource;
}

export interface MeleeDamageRoll {
    damage: string;
    damageType: string;
}

export interface MeleeSystemData extends ItemSystemData {
    attack: {
        value: string;
    };
    damageRolls: Record<string, MeleeDamageRoll>;
    bonus: {
        value: number;
    };
    attackEffects: {
        value: string[];
    };
    weaponType: {
        value: "melee" | "ranged";
    };
}
