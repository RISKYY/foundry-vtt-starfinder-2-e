import { ItemSF2e } from "../base";
import { ConditionData, ConditionType } from "./data";

export class ConditionSF2e extends ItemSF2e {
    static override get schema(): typeof ConditionData {
        return ConditionData;
    }

    get value(): number | null {
        return this.data.data.value.value;
    }

    get duration(): number | null {
        return this.data.data.duration.perpetual ? null : this.data.data.duration.value;
    }

    /** Is the condition currently active? */
    get isActive(): boolean {
        return this.data.data.active;
    }

    /** Is the condition from the sf2e system or a module? */
    get fromSystem(): boolean {
        return !!this.getFlag("sf2e", "condition");
    }

    /** Is the condition found in the token HUD menu? */
    get isInHUD(): boolean {
        return this.data.data.sources.hud;
    }

    /** Ensure value.isValued and value.value are in sync */
    override prepareBaseData() {
        super.prepareBaseData();
        const systemData = this.data.data;
        systemData.value.value = systemData.value.isValued ? Number(systemData.value.value) || 1 : null;
    }
}

export interface ConditionSF2e {
    readonly data: ConditionData;

    get slug(): ConditionType;

    getFlag(scope: "core", key: "sourceId"): string | undefined;
    getFlag(scope: "sf2e", key: "constructing"): true | undefined;
    getFlag(scope: "sf2e", key: "condition"): true | undefined;
    getFlag(scope: string, key: string): any;
}
