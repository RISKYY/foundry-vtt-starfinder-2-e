import { ActionType, ItemSystemData } from "@item/data/base";
import { BaseNonPhysicalItemData, BaseNonPhysicalItemSource } from "@item/data/non-physical";
import { ActionSF2e } from ".";
import { OneToThree } from "@module/data";

export type ActionSource = BaseNonPhysicalItemSource<"action", ActionSystemData>;

export class ActionData extends BaseNonPhysicalItemData<ActionSF2e> {
    static override DEFAULT_ICON: ImagePath = "systems/sf2e/icons/default-icons/action.svg";
}

export interface ActionData extends Omit<ActionSource, "effects" | "flags"> {
    type: ActionSource["type"];
    data: ActionSource["data"];
    readonly _source: ActionSource;
}

interface ActionSystemData extends ItemSystemData {
    actionType: {
        value: ActionType;
    };
    actionCategory: {
        value: string;
    };
    actions: {
        value: OneToThree | null;
    };
    requirements: {
        value: string;
    };
    trigger: {
        value: string;
    };
}
