import { ActionSF2e } from "@item/action";
import { ItemSheetDataSF2e } from "@item/sheet/data-types";
import { ItemSheetSF2e } from "../sheet/base";

export class ActionSheetSF2e extends ItemSheetSF2e<ActionSF2e> {
    override getData() {
        const data: ItemSheetDataSF2e<ActionSF2e> = super.getData();
        const actorWeapons = this.actor?.itemTypes.weapon.map((weapon) => weapon.data) ?? [];
        const actionType = data.data.actionType.value || "action";
        const actionImg = (() => {
            switch (actionType) {
                case "action":
                    return (data.data.actions.value || 1).toString();
                case "reaction":
                case "free":
                case "passive":
                    return actionType;
                default:
                    return "passive";
            }
        })();

        data.item.img = this.getActionImg(actionImg);

        return {
            ...data,
            categories: CONFIG.SF2E.actionCategories,
            weapons: actorWeapons,
            actionTypes: CONFIG.SF2E.actionTypes,
            actionsNumber: CONFIG.SF2E.actionsNumber,
            featTraits: CONFIG.SF2E.featTraits,
            skills: CONFIG.SF2E.skillList,
            proficiencies: CONFIG.SF2E.proficiencyLevels,
            traits: this.prepareOptions(CONFIG.SF2E.featTraits, data.data.traits),
        };
    }
}
