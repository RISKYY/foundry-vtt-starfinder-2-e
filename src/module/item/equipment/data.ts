import {
    BasePhysicalItemData,
    BasePhysicalItemSource,
    MagicItemSystemData,
    PhysicalItemTraits,
} from "@item/physical/data";
import type { EquipmentSF2e } from ".";

export type EquipmentSource = BasePhysicalItemSource<"equipment", EquipmentSystemData>;

export class EquipmentData extends BasePhysicalItemData<EquipmentSF2e> {
    static override DEFAULT_ICON: ImagePath = "systems/sf2e/icons/default-icons/equipment.svg";
}

export interface EquipmentData extends Omit<EquipmentSource, "effects" | "flags"> {
    type: EquipmentSource["type"];
    data: EquipmentSource["data"];
    readonly _source: EquipmentSource;
}

export type EquipmentTrait = keyof ConfigSF2e["SF2E"]["equipmentTraits"];
type EquipmentTraits = PhysicalItemTraits<EquipmentTrait>;

interface EquipmentSystemData extends MagicItemSystemData {
    traits: EquipmentTraits;
}
