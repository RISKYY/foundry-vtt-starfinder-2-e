import { LocalizeSF2e } from "@module/system/localize";
import { objectHasKey } from "@module/utils";
import { PhysicalItemSF2e } from "../physical";
import { EquipmentData, EquipmentTrait } from "./data";

export class EquipmentSF2e extends PhysicalItemSF2e {
    static override get schema(): typeof EquipmentData {
        return EquipmentData;
    }

    override getChatData(this: Embedded<EquipmentSF2e>, htmlOptions: EnrichHTMLOptions = {}): Record<string, unknown> {
        const data = this.data.data;
        const traits = this.traitChatData(CONFIG.SF2E.equipmentTraits);
        const properties = [data.equipped.value ? game.i18n.localize("SF2E.EquipmentEquippedLabel") : null].filter(
            (p) => p
        );
        return this.processChatData(htmlOptions, { ...data, properties, traits });
    }

    override generateUnidentifiedName({ typeOnly = false }: { typeOnly?: boolean } = { typeOnly: false }): string {
        const translations = LocalizeSF2e.translations.SF2E.identification;
        const slotType = /book\b/.test(this.slug ?? "")
            ? "Book"
            : /\bring\b/.test(this.slug ?? "")
            ? "Ring"
            : this.data.data.usage.value?.replace(/^worn/, "").capitalize() ?? "";

        const itemType = objectHasKey(translations.UnidentifiedType, slotType)
            ? translations.UnidentifiedType[slotType]
            : translations.UnidentifiedType.Object;

        if (typeOnly) return itemType;

        const formatString = translations.UnidentifiedItem;
        return game.i18n.format(formatString, { item: itemType });
    }
}

export interface EquipmentSF2e {
    readonly data: EquipmentData;

    get traits(): Set<EquipmentTrait>;
}
