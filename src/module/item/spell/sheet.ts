import { SpellSF2e } from "@item/spell";
import { ItemSheetSF2e } from "../sheet/base";
import { ItemSheetDataSF2e, SpellSheetData } from "../sheet/data-types";
import { SpellDamage, SpellSystemData } from "./data";
import { objectHasKey } from "@module/utils";

const DEFAULT_INTERVAL_SCALING: SpellSystemData["scaling"] = {
    interval: 1,
    damage: {},
};

export class SpellSheetSF2e extends ItemSheetSF2e<SpellSF2e> {
    override getData(): SpellSheetData {
        const data: ItemSheetDataSF2e<SpellSF2e> = super.getData();

        // Create a level label to show in the summary.
        // This one is a longer version than the chat card
        const levelLabel = (() => {
            const category =
                this.item.isCantrip && this.item.isFocusSpell
                    ? game.i18n.localize("SF2E.SpellCategoryFocusCantrip")
                    : this.item.isCantrip
                    ? game.i18n.localize("SF2E.TraitCantrip")
                    : game.i18n.localize(CONFIG.SF2E.spellCategories[this.item.data.data.category.value]);
            return game.i18n.format("SF2E.SpellLevel", { category, level: this.item.level });
        })();

        return {
            ...data,
            levelLabel,
            spellCategories: CONFIG.SF2E.spellCategories,
            spellTypes: CONFIG.SF2E.spellTypes,
            magicSchools: CONFIG.SF2E.magicSchools,
            spellLevels: CONFIG.SF2E.spellLevels,
            magicTraditions: this.prepareOptions(CONFIG.SF2E.magicTraditions, data.data.traditions),
            damageSubtypes: CONFIG.SF2E.damageSubtypes,
            damageCategories: CONFIG.SF2E.damageCategories,
            traits: this.prepareOptions(CONFIG.SF2E.spellTraits, data.data.traits, { selectedOnly: true }),
            rarities: this.prepareOptions(CONFIG.SF2E.rarityTraits, { value: [data.data.traits.rarity.value] }),
            spellComponents: this.formatSpellComponents(data.data),
            areaSizes: CONFIG.SF2E.areaSizes,
            areaTypes: CONFIG.SF2E.areaTypes,
            spellScalingModes: CONFIG.SF2E.spellScalingModes,
        };
    }

    override activateListeners(html: JQuery<HTMLElement>): void {
        super.activateListeners(html);

        html.find(".toggle-trait").on("change", (evt) => {
            const target = evt.target as HTMLInputElement;
            const trait = target.dataset.trait ?? "";
            if (!objectHasKey(CONFIG.SF2E.spellTraits, trait)) {
                console.warn("Toggled trait is invalid");
                return;
            }

            if (target.checked && !this.item.traits.has(trait)) {
                const newTraits = this.item.data.data.traits.value.concat([trait]);
                this.item.update({ "data.traits.value": newTraits });
            } else if (!target.checked && this.item.traits.has(trait)) {
                const newTraits = this.item.data.data.traits.value.filter((t) => t !== trait);
                this.item.update({ "data.traits.value": newTraits });
            }
        });

        html.find("[data-action='damage-create']").on("click", (event) => {
            event.preventDefault();
            const damage: SpellDamage = { value: "", type: { value: "bludgeoning", categories: [] } };
            this.item.update({
                "data.damage.value": { ...this.item.damage.concat(damage) },
            });
        });

        html.find("[data-action='damage-delete']").on("click", (event) => {
            event.preventDefault();
            const idx = Number($(event.target).closest("[data-action='damage-delete']").attr("data-idx"));
            const data = this.item.toObject();
            delete data.data.damage.value[idx];
            data.data.damage.value = { ...Object.values(data.data.damage.value) };
            this.item.update(data, { recursive: false, diff: false });
        });

        html.find("[data-action='scaling-create']").on("click", (event) => {
            event.preventDefault();
            this.item.update({ "data.scaling": DEFAULT_INTERVAL_SCALING });
        });

        html.find("[data-action='scaling-delete']").on("click", (event) => {
            event.preventDefault();
            this.item.update({ "data.-=scaling": null });
        });
    }

    private formatSpellComponents(data: SpellSystemData): string[] {
        if (!data.components) return [];
        const comps: string[] = [];
        if (data.components.focus) comps.push(game.i18n.localize(CONFIG.SF2E.spellComponents.F));
        if (data.components.material) comps.push(game.i18n.localize(CONFIG.SF2E.spellComponents.M));
        if (data.components.somatic) comps.push(game.i18n.localize(CONFIG.SF2E.spellComponents.S));
        if (data.components.verbal) comps.push(game.i18n.localize(CONFIG.SF2E.spellComponents.V));
        if (data.materials.value) comps.push(data.materials.value);
        return comps;
    }
}
