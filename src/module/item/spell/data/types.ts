import { SaveType } from "@actor/data";
import { AbilityString } from "@actor/data/base";
import { TrickMagicItemCastData } from "@item/data";
import { ItemLevelData, ItemSystemData, ItemTraits } from "@item/data/base";
import { BaseNonPhysicalItemData, BaseNonPhysicalItemSource } from "@item/data/non-physical";
import { MagicTradition } from "@item/spellcasting-entry/data";
import { DamageType } from "@module/damage-calculation";
import { ValuesList, OneToTen } from "@module/data";
import type { SpellSF2e } from "@item";
import { MAGIC_SCHOOLS } from "./values";

export type SpellSource = BaseNonPhysicalItemSource<"spell", SpellSystemData>;

export class SpellData extends BaseNonPhysicalItemData<SpellSF2e> {
    /** Prepared data */
    isCantrip!: boolean;
    isFocusSpell!: boolean;
    isRitual!: boolean;

    static override DEFAULT_ICON: ImagePath = "systems/sf2e/icons/default-icons/spell.svg";
}

export interface SpellData extends Omit<SpellSource, "effects" | "flags"> {
    type: SpellSource["type"];
    data: SpellSource["data"];
    readonly _source: SpellSource;
}

export type MagicSchool = typeof MAGIC_SCHOOLS[number];

export type SpellTrait = keyof ConfigSF2e["SF2E"]["spellTraits"] | MagicSchool | MagicTradition;
export type SpellTraits = ItemTraits<SpellTrait>;
type SpellDamageCategory = keyof ConfigSF2e["SF2E"]["damageCategories"];

export interface SpellDamageType {
    value: DamageType;
    subtype?: "persistent" | "splash";
    categories: SpellDamageCategory[];
}

export interface SpellDamage {
    value: string;
    applyMod?: boolean;
    type: SpellDamageType;
}

export interface SpellSystemData extends ItemSystemData, ItemLevelData {
    traits: SpellTraits;
    level: {
        value: OneToTen;
    };
    spellType: {
        value: string;
    };
    category: {
        value: keyof ConfigSF2e["SF2E"]["spellCategories"];
    };
    traditions: ValuesList<MagicTradition>;
    school: {
        value: MagicSchool;
    };
    components: {
        focus: boolean;
        material: boolean;
        somatic: boolean;
        verbal: boolean;
    };
    materials: {
        value: string;
    };
    target: {
        value: string;
    };
    range: {
        value: string;
    };
    area: {
        value: keyof ConfigSF2e["SF2E"]["areaSizes"];
        areaType: keyof ConfigSF2e["SF2E"]["areaTypes"];
    };
    time: {
        value: string;
    };
    duration: {
        value: string;
    };
    damage: {
        value: Record<number, SpellDamage>;
    };
    scaling?: {
        interval: number;
        damage: Record<number, string>;
    };
    save: {
        basic: string;
        value: SaveType | "";
        dc?: number;
        str?: string;
    };
    sustained: {
        value: false;
    };
    cost: {
        value: string;
    };
    ability: {
        value: AbilityString;
    };
    location: {
        value: string;
    };
    heightenedLevel: {
        value: number;
    };
    hasCounteractCheck: {
        value: boolean;
    };
    isSave?: boolean;
    damageLabel?: string;
    isAttack?: boolean;
    spellLvl?: string;
    properties?: (number | string)[];
    item?: string;
    trickMagicItemData?: TrickMagicItemCastData;
    isSignatureSpell?: boolean;
    autoHeightenLevel: {
        value: OneToTen | null;
    };
}
