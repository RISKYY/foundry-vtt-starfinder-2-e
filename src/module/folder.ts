import { ActorSF2e } from "@actor";
import { ItemSF2e } from "@item";

type EnfolderableDocumentSF2e = ActorSF2e | ItemSF2e | Exclude<EnfolderableDocument, Actor | Item>;

/** An empty subclass, used in the past to work around a Foundry bug and kept in place for later needs */
export class FolderSF2e<
    TDocument extends EnfolderableDocumentSF2e = EnfolderableDocumentSF2e
> extends Folder<TDocument> {
    get flattenedContents(): TDocument[] {
        return [this, ...this.getSubfolders()].flatMap((folder) => folder.contents);
    }
}
