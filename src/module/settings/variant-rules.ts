const SETTINGS = {
    staminaVariant: {
        name: "SF2E.SETTINGS.Variant.Stamina.Name",
        hint: "SF2E.SETTINGS.Variant.Stamina.Hint",
        scope: "world",
        config: false,
        default: 0,
        type: Number,
        choices: {
            0: "SF2E.SETTINGS.Variant.Stamina.Choices.0",
            1: "SF2E.SETTINGS.Variant.Stamina.Choices.1", // I plan to expand this, hence the dropdown.
        },
    },
    ancestryParagonVariant: {
        name: "SF2E.SETTINGS.Variant.AncestryParagon.Name",
        hint: "SF2E.SETTINGS.Variant.AncestryParagon.Hint",
        scope: "world",
        config: false,
        default: 0,
        type: Boolean,
    },
    freeArchetypeVariant: {
        name: "SF2E.SETTINGS.Variant.FreeArchetype.Name",
        hint: "SF2E.SETTINGS.Variant.FreeArchetype.Hint",
        scope: "world",
        config: false,
        default: 0,
        type: Boolean,
    },
    automaticBonusVariant: {
        name: "SF2E.SETTINGS.Variant.AutomaticBonus.Name",
        hint: "SF2E.SETTINGS.Variant.AutomaticBonus.Hint",
        scope: "world",
        config: false,
        default: "noABP",
        type: String,
        choices: {
            noABP: "SF2E.SETTINGS.Variant.AutomaticBonus.Choices.noABP",
            ABPRulesAsWritten: "SF2E.SETTINGS.Variant.AutomaticBonus.Choices.ABPRulesAsWritten",
            ABPFundamentalPotency: "SF2E.SETTINGS.Variant.AutomaticBonus.Choices.ABPFundamentalPotency",
        },
    },
    proficiencyVariant: {
        name: "SF2E.SETTINGS.Variant.Proficiency.Name",
        hint: "SF2E.SETTINGS.Variant.Proficiency.Hint",
        scope: "world",
        config: false,
        default: "ProficiencyWithLevel",
        type: String,
        choices: {
            ProficiencyWithLevel: "SF2E.SETTINGS.Variant.Proficiency.Choices.ProficiencyWithLevel",
            ProficiencyWithoutLevel: "SF2E.SETTINGS.Variant.Proficiency.Choices.ProficiencyWithoutLevel",
        },
    },
    proficiencyUntrainedModifier: {
        name: "SF2E.SETTINGS.Variant.UntrainedModifier.Name",
        hint: "SF2E.SETTINGS.Variant.UntrainedModifier.Hint",
        scope: "world",
        config: false,
        default: 0,
        type: Number,
    },
    proficiencyTrainedModifier: {
        name: "SF2E.SETTINGS.Variant.TrainedModifier.Name",
        hint: "SF2E.SETTINGS.Variant.TrainedModifier.Hint",
        scope: "world",
        config: false,
        default: 2,
        type: Number,
    },
    proficiencyExpertModifier: {
        name: "SF2E.SETTINGS.Variant.ExpertModifier.Name",
        hint: "SF2E.SETTINGS.Variant.ExpertModifier.Hint",
        scope: "world",
        config: false,
        default: 4,
        type: Number,
    },
    proficiencyMasterModifier: {
        name: "SF2E.SETTINGS.Variant.MasterModifier.Name",
        hint: "SF2E.SETTINGS.Variant.MasterModifier.Hint",
        scope: "world",
        config: false,
        default: 6,
        type: Number,
    },
    proficiencyLegendaryModifier: {
        name: "SF2E.SETTINGS.Variant.LegendaryModifier.Name",
        hint: "SF2E.SETTINGS.Variant.LegendaryModifier.Hint",
        scope: "world",
        config: false,
        default: 8,
        type: Number,
    },
} as const;

export class VariantRulesSettings extends FormApplication {
    static override get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            title: "SF2E.SETTINGS.Variant.Title",
            id: "variant-rules-settings",
            template: "systems/sf2e/templates/system/settings/variant-rules-settings.html",
            width: 550,
            height: "auto",
            closeOnSubmit: true,
        });
    }

    override getData() {
        const data: any = {};
        for (const [k, v] of Object.entries(SETTINGS)) {
            data[k] = {
                value: game.settings.get("sf2e", k),
                setting: v,
            };
        }
        return data;
    }

    static registerSettings() {
        for (const [k, v] of Object.entries(SETTINGS)) {
            game.settings.register("sf2e", k, v);
        }
    }

    /* -------------------------------------------- */
    /*  Event Listeners and Handlers                */
    /* -------------------------------------------- */

    override activateListeners(html: JQuery) {
        super.activateListeners(html);
        html.find('button[name="reset"]').on("click", (event) => this.onResetDefaults(event));
    }

    /**
     * Handle button click to reset default settings
     * @param event The initial button click event
     */
    private async onResetDefaults(event: JQuery.ClickEvent): Promise<this> {
        event.preventDefault();
        for await (const [k, v] of Object.entries(SETTINGS)) {
            await game.settings.set("sf2e", k, v?.default);
        }
        return this.render();
    }

    protected override async _onSubmit(
        event: Event,
        options: OnSubmitFormOptions = {}
    ): Promise<Record<string, unknown>> {
        event.preventDefault();
        return super._onSubmit(event, options);
    }

    protected override async _updateObject(_event: Event, data: Record<string, unknown>): Promise<void> {
        for await (const key of Object.keys(SETTINGS)) {
            game.settings.set("sf2e", key, data[key]);
        }
    }
}
