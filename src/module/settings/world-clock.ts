import { DateTime } from "luxon";
import { LocalizeSF2e } from "../system/localize";

type SettingsKey =
    | "dateTheme"
    | "timeConvention"
    | "playersCanView"
    | "syncDarkness"
    | "worldCreatedOn"
    | "showClockButton";

interface FormInputData extends Omit<ClientSettingsData, "scope"> {
    key: string;
    value: unknown;
    isSelect: boolean;
    isCheckbox: boolean;
    isDateTime: boolean;
    scope?: "world" | "client";
}

interface TemplateData extends FormApplicationData {
    settings: FormInputData[];
}

interface UpdateData {
    dateTheme: string;
    timeConvention: boolean;
    playersCanView: boolean;
    syncDarkness: boolean;
    syncDarknessScene: boolean;
    worldCreatedOn: string;
    showClockButton: boolean;
}

export class WorldClockSettings extends FormApplication {
    static override get defaultOptions(): FormApplicationOptions {
        return mergeObject(super.defaultOptions, {
            title: CONFIG.SF2E.SETTINGS.worldClock.name,
            id: "world-clock-settings",
            template: "systems/sf2e/templates/system/settings/world-clock/index.html",
            width: 550,
            height: "auto",
            closeOnSubmit: true,
        });
    }

    override getData(): TemplateData {
        const worldDefault = game.settings.get("sf2e", "worldClock.syncDarkness")
            ? game.i18n.localize(CONFIG.SF2E.SETTINGS.worldClock.syncDarknessScene.enabled)
            : game.i18n.localize(CONFIG.SF2E.SETTINGS.worldClock.syncDarknessScene.disabled);
        const sceneSetting: [string, Omit<ClientSettingsData, "scope">] = [
            "syncDarknessScene",
            {
                name: CONFIG.SF2E.SETTINGS.worldClock.syncDarknessScene.name,
                hint: CONFIG.SF2E.SETTINGS.worldClock.syncDarknessScene.hint,
                default: "default",
                type: String,
                choices: {
                    enabled: CONFIG.SF2E.SETTINGS.worldClock.syncDarknessScene.enabled,
                    disabled: CONFIG.SF2E.SETTINGS.worldClock.syncDarknessScene.disabled,
                    default: game.i18n.format(CONFIG.SF2E.SETTINGS.worldClock.syncDarknessScene.default, {
                        worldDefault,
                    }),
                },
            },
        ];

        const visibleSettings = [
            ...Object.entries(WorldClockSettings.settings).filter(([key]) => key !== "worldCreatedOn"),
            sceneSetting,
        ];

        const settings: FormInputData[] = visibleSettings.map(([key, setting]) => {
            const value = ((): unknown => {
                if (key === "syncDarknessScene") return canvas.scene?.data.flags.sf2e.syncDarkness;
                const rawValue = game.settings.get("sf2e", `worldClock.${key}`);

                // Present the world-creation timestamp as an HTML datetime-locale input
                if (key === "worldCreatedOn" && typeof rawValue === "string") {
                    return DateTime.fromISO(rawValue).toFormat("yyyy-MM-dd'T'HH:mm");
                }
                return rawValue;
            })();

            return {
                ...setting,
                key: key,
                value: value,
                isSelect: "choices" in setting,
                isCheckbox: setting.type === Boolean,
                isDateTime: setting.type === String && !("choices" in setting),
            };
        });
        return mergeObject(super.getData(), { settings });
    }

    /** Register World Clock settings */
    static registerSettings(): void {
        game.settings.register("sf2e", "worldClock.dateTheme", this.settings.dateTheme);
        game.settings.register("sf2e", "worldClock.timeConvention", this.settings.timeConvention);
        game.settings.register("sf2e", "worldClock.playersCanView", this.settings.playersCanView);
        game.settings.register("sf2e", "worldClock.syncDarkness", this.settings.syncDarkness);
        game.settings.register("sf2e", "worldClock.worldCreatedOn", this.settings.worldCreatedOn);
        game.settings.register("sf2e", "worldClock.showClockButton", this.settings.showClockButton);
    }

    override activateListeners($html: JQuery): void {
        super.activateListeners($html);

        const translations = LocalizeSF2e.translations.SF2E.SETTINGS.WorldClock;
        const title = translations.ResetWorldTime.Name;
        $html.find("button.reset-world-time").on("click", async () => {
            const template = await renderTemplate(
                "systems/sf2e/templates/system/settings/world-clock/confirm-reset.html"
            );
            Dialog.confirm({
                title: title,
                content: template,
                yes: () => {
                    game.time.advance(-1 * game.time.worldTime);
                    this.close();
                },
                defaultYes: false,
            });
        });

        $html.find<HTMLInputElement>('input[name="syncDarkness"]').on("change", (event) => {
            const worldDefault = $(event.currentTarget)[0].checked
                ? translations.SyncDarknessScene.Enabled
                : translations.SyncDarknessScene.Disabled;
            const optionSelector = 'select[name="syncDarknessScene"] > option[value="default"]';
            $html.find(optionSelector).text(game.i18n.format(translations.SyncDarknessScene.Default, { worldDefault }));
        });
    }

    protected override async _updateObject(_event: Event, data: Record<string, unknown> & UpdateData): Promise<void> {
        const keys: (keyof UpdateData)[] = [
            "dateTheme",
            "timeConvention",
            "playersCanView",
            "syncDarkness",
            "showClockButton",
        ];
        for await (const key of keys) {
            const settingKey = `worldClock.${key}`;
            const newValue = key === "worldCreatedOn" ? DateTime.fromISO(data[key]).toUTC() : data[key];
            await game.settings.set("sf2e", settingKey, newValue);
        }

        await canvas.scene?.setFlag("sf2e", "syncDarkness", data.syncDarknessScene ?? "default");
        delete (data as { syncDarknessScene?: unknown }).syncDarknessScene;

        game.sf2e.worldClock.render(false);
    }

    /** Settings to be registered and also later referenced during user updates */
    private static get settings(): Record<SettingsKey, ClientSettingsData> {
        return {
            // Date theme, currently one of Golarion (Absalom Reckoning), Earth (Material Plane, 95 years ago), or
            // Earth (real world)
            dateTheme: {
                name: CONFIG.SF2E.SETTINGS.worldClock.dateTheme.name,
                hint: CONFIG.SF2E.SETTINGS.worldClock.dateTheme.hint,
                scope: "world",
                config: false,
                default: "AR",
                type: String,
                choices: {
                    AR: CONFIG.SF2E.SETTINGS.worldClock.dateTheme.AR,
                    AD: CONFIG.SF2E.SETTINGS.worldClock.dateTheme.AD,
                    CE: CONFIG.SF2E.SETTINGS.worldClock.dateTheme.CE,
                },
            },
            timeConvention: {
                name: CONFIG.SF2E.SETTINGS.worldClock.timeConvention.name,
                hint: CONFIG.SF2E.SETTINGS.worldClock.timeConvention.hint,
                scope: "world",
                config: false,
                default: 24,
                type: Number,
                choices: {
                    24: CONFIG.SF2E.SETTINGS.worldClock.timeConvention.twentyFour,
                    12: CONFIG.SF2E.SETTINGS.worldClock.timeConvention.twelve,
                },
            },
            // Show the World Clock
            showClockButton: {
                name: CONFIG.SF2E.SETTINGS.worldClock.showClockButton.name,
                hint: CONFIG.SF2E.SETTINGS.worldClock.showClockButton.hint,
                scope: "world",
                config: false,
                default: true,
                type: Boolean,
                onChange: () => {
                    game.settings.set(
                        "sf2e",
                        "worldClock.playersCanView",
                        game.settings.get("sf2e", "worldClock.showClockButton")
                    );
                },
            },
            // Players can view the World Clock
            playersCanView: {
                name: CONFIG.SF2E.SETTINGS.worldClock.playersCanView.name,
                hint: CONFIG.SF2E.SETTINGS.worldClock.playersCanView.hint,
                scope: "world",
                config: false,
                default: false,
                type: Boolean,
            },
            // Synchronize a scene's Darkness Level with the time of day, given Global Illumination is turned on
            syncDarkness: {
                name: CONFIG.SF2E.SETTINGS.worldClock.syncDarkness.name,
                hint: CONFIG.SF2E.SETTINGS.worldClock.syncDarkness.hint,
                scope: "world",
                config: false,
                default: false,
                type: Boolean,
            },
            // The Unix timestamp of the world's creation date
            worldCreatedOn: {
                name: CONFIG.SF2E.SETTINGS.worldClock.worldCreatedOn.name,
                hint: CONFIG.SF2E.SETTINGS.worldClock.worldCreatedOn.hint,
                scope: "world",
                config: false,
                default: DateTime.utc().toISO(),
                type: String,
            },
        };
    }
}
