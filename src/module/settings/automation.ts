import { PartialSettingsData, SettingsMenuSF2e } from "./menu";

type ConfigSF2eListName = typeof AutomationSettings.SETTINGS[number];

export class AutomationSettings extends SettingsMenuSF2e {
    static override readonly namespace = "automation";

    static override readonly SETTINGS = [
        "rulesBasedVision",
        "effectExpiration",
        "lootableNPCs",
        "experimentalDamageFormatting",
    ] as const;

    protected static override get settings(): Record<ConfigSF2eListName, PartialSettingsData> {
        return {
            rulesBasedVision: {
                name: CONFIG.SF2E.SETTINGS.automation.rulesBasedVision.name,
                hint: CONFIG.SF2E.SETTINGS.automation.rulesBasedVision.hint,
                default: false,
                type: Boolean,
                onChange: () => {
                    window.location.reload();
                },
            },
            effectExpiration: {
                name: CONFIG.SF2E.SETTINGS.automation.effectExpiration.name,
                hint: CONFIG.SF2E.SETTINGS.automation.effectExpiration.hint,
                default: true,
                type: Boolean,
                onChange: () => {
                    game.actors.forEach((actor) => {
                        actor.prepareData();
                        actor.sheet.render(false);
                        actor.getActiveTokens().forEach((token) => token.drawEffects());
                    });
                },
            },
            lootableNPCs: {
                name: CONFIG.SF2E.SETTINGS.automation.lootableNPCs.name,
                hint: CONFIG.SF2E.SETTINGS.automation.lootableNPCs.hint,
                default: false,
                type: Boolean,
            },
            experimentalDamageFormatting: {
                name: CONFIG.SF2E.SETTINGS.automation.experimentalDamageFormatting.name,
                hint: CONFIG.SF2E.SETTINGS.automation.experimentalDamageFormatting.hint,
                default: true,
                type: Boolean,
            },
        };
    }
}
