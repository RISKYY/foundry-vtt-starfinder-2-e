export type PartialSettingsData = Omit<ClientSettingsData, "scope" | "config">;

interface SettingsTemplateData extends PartialSettingsData {
    key: string;
    value: unknown;
}

export interface MenuTemplateData extends FormApplicationData {
    settings: SettingsTemplateData[];
}

export abstract class SettingsMenuSF2e extends FormApplication {
    static readonly namespace: string;

    static override get defaultOptions() {
        const options = super.defaultOptions;
        options.classes.push("settings-menu");

        return mergeObject(options, {
            title: `SF2E.SETTINGS.${this.namespace.titleCase()}.Name`,
            id: `${this.namespace}-settings`,
            template: `systems/sf2e/templates/system/settings/menu.html`,
            width: 550,
            height: "auto",
            closeOnSubmit: true,
        });
    }

    get namespace(): string {
        return (this.constructor as typeof SettingsMenuSF2e).namespace;
    }

    static readonly SETTINGS: ReadonlyArray<string>;

    /** Settings to be registered and also later referenced during user updates */
    protected static get settings(): Record<string, PartialSettingsData> {
        return {};
    }

    static registerSettings(): void {
        const settings = this.settings;
        for (const setting of this.SETTINGS) {
            game.settings.register("sf2e", `${this.namespace}.${setting}`, {
                ...settings[setting],
                scope: "world",
                config: false,
            });
        }
    }

    override getData(): MenuTemplateData {
        const settings = (this.constructor as typeof SettingsMenuSF2e).settings;
        const templateData: SettingsTemplateData[] = Object.entries(settings).map(([key, setting]) => {
            const value = game.settings.get("sf2e", `${this.namespace}.${key}`);
            return {
                ...setting,
                key,
                value,
                isSelect: !!setting.choices,
                isCheckbox: setting.type === Boolean,
            };
        });
        return mergeObject(super.getData(), {
            settings: templateData,
            instructions: `SF2E.SETTINGS.${this.namespace.titleCase()}.Hint`,
        });
    }

    protected override async _updateObject(_event: Event, data: Record<string, unknown>): Promise<void> {
        for await (const key of (this.constructor as typeof SettingsMenuSF2e).SETTINGS) {
            const settingKey = `${this.namespace}.${key}`;
            await game.settings.set("sf2e", settingKey, data[key]);
        }
    }
}
