import { VariantRulesSettings } from "./variant-rules";
import { WorldClockSettings } from "./world-clock";
import { HomebrewElements } from "./homebrew";
import { StatusEffects } from "@scripts/actor/status-effects";
import { objectHasKey } from "@module/utils";
import { MigrationRunner } from "@module/migration/runner";
import { AutomationSettings } from "./automation";
import { JournalSheetSF2e } from "@module/journal-entry/sheet";
import { MetagameSettings } from "./metagame";

export function registerSettings() {
    if (BUILD_MODE === "development") {
        registerWorldSchemaVersion();
    }

    game.settings.register("sf2e", "defaultTokenSettings", {
        name: "SF2E.SETTINGS.DefaultTokenSettings.Name",
        hint: "SF2E.SETTINGS.DefaultTokenSettings.Hint",
        scope: "world",
        config: false,
        default: true,
        type: Boolean,
    });

    game.settings.register("sf2e", "defaultTokenSettingsName", {
        name: "SF2E.SETTINGS.DefaultTokenSettingsName.Name",
        hint: "SF2E.SETTINGS.DefaultTokenSettingsName.Hint",
        scope: "world",
        config: true,
        default: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,
        type: Number,
        choices: {
            [CONST.TOKEN_DISPLAY_MODES.NONE]: "TOKEN.DISPLAY_NONE",
            [CONST.TOKEN_DISPLAY_MODES.CONTROL]: "TOKEN.DISPLAY_CONTROL",
            [CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER]: "TOKEN.DISPLAY_OWNER_HOVER",
            [CONST.TOKEN_DISPLAY_MODES.HOVER]: "TOKEN.DISPLAY_HOVER",
            [CONST.TOKEN_DISPLAY_MODES.OWNER]: "TOKEN.DISPLAY_OWNER",
            [CONST.TOKEN_DISPLAY_MODES.ALWAYS]: "TOKEN.DISPLAY_ALWAYS",
        },
    });

    game.settings.register("sf2e", "defaultTokenSettingsBar", {
        name: "SF2E.SETTINGS.DefaultTokenSettingsBar.Name",
        hint: "SF2E.SETTINGS.DefaultTokenSettingsBar.Hint",
        scope: "world",
        config: true,
        default: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,
        type: Number,
        choices: {
            [CONST.TOKEN_DISPLAY_MODES.NONE]: "TOKEN.DISPLAY_NONE",
            [CONST.TOKEN_DISPLAY_MODES.CONTROL]: "TOKEN.DISPLAY_CONTROL",
            [CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER]: "TOKEN.DISPLAY_OWNER_HOVER",
            [CONST.TOKEN_DISPLAY_MODES.HOVER]: "TOKEN.DISPLAY_HOVER",
            [CONST.TOKEN_DISPLAY_MODES.OWNER]: "TOKEN.DISPLAY_OWNER",
            [CONST.TOKEN_DISPLAY_MODES.ALWAYS]: "TOKEN.DISPLAY_ALWAYS",
        },
    });

    game.settings.register("sf2e", "ignoreCoinBulk", {
        name: "SF2E.SETTINGS.IgnoreCoinBulk.Name",
        hint: "SF2E.SETTINGS.IgnoreCoinBulk.Hint",
        scope: "world",
        config: true,
        default: false,
        type: Boolean,
    });

    game.settings.register("sf2e", "identifyMagicNotMatchingTraditionModifier", {
        name: "SF2E.SETTINGS.IdentifyMagicNotMatchingTraditionModifier.Name",
        hint: "SF2E.SETTINGS.IdentifyMagicNotMatchingTraditionModifier.Hint",
        choices: {
            0: "SF2E.SETTINGS.IdentifyMagicNotMatchingTraditionModifier.Choices.0",
            2: "SF2E.SETTINGS.IdentifyMagicNotMatchingTraditionModifier.Choices.2",
            5: "SF2E.SETTINGS.IdentifyMagicNotMatchingTraditionModifier.Choices.5",
            10: "SF2E.SETTINGS.IdentifyMagicNotMatchingTraditionModifier.Choices.10",
        },
        type: Number,
        default: 5,
        scope: "world",
        config: true,
    });

    game.settings.register("sf2e", "critRule", {
        name: "SF2E.SETTINGS.CritRule.Name",
        hint: "SF2E.SETTINGS.CritRule.Hint",
        scope: "world",
        config: true,
        default: "doubledamage",
        type: String,
        choices: {
            doubledamage: "SF2E.SETTINGS.CritRule.Choices.Doubledamage",
            doubledice: "SF2E.SETTINGS.CritRule.Choices.Doubledice",
        },
    });

    game.settings.register("sf2e", "compendiumBrowserPacks", {
        name: "SF2E.SETTINGS.CompendiumBrowserPacks.Name",
        hint: "SF2E.SETTINGS.CompendiumBrowserPacks.Hint",
        default: "{}",
        type: String,
        scope: "world",
        onChange: () => {
            game.sf2e.compendiumBrowser.loadSettings();
        },
    });

    game.settings.register("sf2e", "pfsSheetTab", {
        name: "SF2E.SETTINGS.PFSSheetTab.Name",
        hint: "SF2E.SETTINGS.PFSSheetTab.Hint",
        scope: "world",
        config: true,
        default: true,
        type: Boolean,
        onChange: () => {
            const actors = game.actors.filter((actor) => actor.type === "character");
            const sheets = actors.flatMap((actor) => Object.values(actor.apps));
            for (const sheet of sheets) {
                sheet.render();
            }
        },
    });

    game.settings.register("sf2e", "journalEntryTheme", {
        name: "SF2E.SETTINGS.JournalEntryTheme.Name",
        hint: "SF2E.SETTINGS.JournalEntryTheme.Hint",
        scope: "world",
        config: true,
        default: "sf2eTheme",
        type: String,
        choices: {
            sf2eTheme: "SF2E.SETTINGS.JournalEntryTheme.SF2E",
            foundry: "SF2E.SETTINGS.JournalEntryTheme.Foundry",
        },
        onChange: () => {
            const sheets = Object.values(ui.windows).filter(
                (app): app is JournalSheetSF2e => app instanceof JournalSheetSF2e
            );
            for (const sheet of sheets) {
                sheet.element.toggleClass("sf2e");
                sheet.render(false);
            }
        },
    });

    game.settings.register("sf2e", "enabledRulesUI", {
        name: "SF2E.SETTINGS.EnabledRulesUI.Name",
        hint: "SF2E.SETTINGS.EnabledRulesUI.Hint",
        scope: "world",
        config: true,
        default: false,
        type: Boolean,
    });

    game.settings.register("sf2e", "critFumbleButtons", {
        name: game.i18n.localize("SF2E.SETTINGS.critFumbleCardButtons.name"),
        hint: game.i18n.localize("SF2E.SETTINGS.critFumbleCardButtons.hint"),
        scope: "world",
        config: true,
        default: false,
        type: Boolean,
        onChange: () => {
            window.location.reload();
        },
    });

    game.settings.register("sf2e", "drawCritFumble", {
        name: game.i18n.localize("SF2E.SETTINGS.critFumbleCards.name"),
        hint: game.i18n.localize("SF2E.SETTINGS.critFumbleCards.hint"),
        scope: "world",
        config: true,
        default: false,
        type: Boolean,
        onChange: () => {
            window.location.reload();
        },
    });

    const iconChoices = {
        blackWhite: "SF2E.SETTINGS.statusEffectType.blackWhite",
        default: "SF2E.SETTINGS.statusEffectType.default",
        legacy: "SF2E.SETTINGS.statusEffectType.legacy",
    };
    game.settings.register("sf2e", "statusEffectType", {
        name: "SF2E.SETTINGS.statusEffectType.name",
        hint: "SF2E.SETTINGS.statusEffectType.hint",
        scope: "world",
        config: true,
        default: "blackWhite",
        type: String,
        choices: iconChoices,
        onChange: (iconType = "") => {
            if (objectHasKey(iconChoices, iconType)) {
                StatusEffects.migrateStatusEffectUrls(iconType);
            }
        },
    });

    game.settings.register("sf2e", "statusEffectShowCombatMessage", {
        name: "SF2E.SETTINGS.statusEffectShowCombatMessage.name",
        hint: "SF2E.SETTINGS.statusEffectShowCombatMessage.hint",
        scope: "client",
        config: true,
        default: true,
        type: Boolean,
    });

    game.settings.registerMenu("sf2e", "automation", {
        name: "SF2E.SETTINGS.Automation.Name",
        label: "SF2E.SETTINGS.Automation.Label",
        hint: "SF2E.SETTINGS.Automation.Hint",
        icon: "fas fa-robot",
        type: AutomationSettings,
        restricted: true,
    });
    AutomationSettings.registerSettings();

    game.settings.registerMenu("sf2e", "metagame", {
        name: "SF2E.SETTINGS.Metagame.Name",
        label: "SF2E.SETTINGS.Metagame.Label",
        hint: "SF2E.SETTINGS.Metagame.Hint",
        icon: "fas fa-brain",
        type: MetagameSettings,
        restricted: true,
    });
    MetagameSettings.registerSettings();

    game.settings.registerMenu("sf2e", "variantRules", {
        name: "SF2E.SETTINGS.Variant.Name",
        label: "SF2E.SETTINGS.Variant.Label",
        hint: "SF2E.SETTINGS.Variant.Hint",
        icon: "fas fa-book",
        type: VariantRulesSettings,
        restricted: true,
    });
    VariantRulesSettings.registerSettings();

    game.settings.registerMenu("sf2e", "homebrew", {
        name: "SF2E.SETTINGS.Homebrew.Name",
        label: "SF2E.SETTINGS.Homebrew.Label",
        hint: "SF2E.SETTINGS.Homebrew.Hint",
        icon: "fas fa-beer",
        type: HomebrewElements,
        restricted: true,
    });
    HomebrewElements.registerSettings();

    game.settings.registerMenu("sf2e", "worldClock", {
        name: game.i18n.localize(CONFIG.SF2E.SETTINGS.worldClock.name),
        label: game.i18n.localize(CONFIG.SF2E.SETTINGS.worldClock.label),
        hint: game.i18n.localize(CONFIG.SF2E.SETTINGS.worldClock.hint),
        icon: "far fa-clock",
        type: WorldClockSettings,
        restricted: true,
    });
    WorldClockSettings.registerSettings();

    // this section starts questionable rule settings, all of them should have a 'RAI.' at the start of their name
    game.settings.register("sf2e", "RAI.TreatWoundsAltSkills", {
        name: "SF2E.SETTINGS.RAI.TreatWoundsAltSkills.Name",
        hint: "SF2E.SETTINGS.RAI.TreatWoundsAltSkills.Hint",
        scope: "world",
        config: true,
        default: true,
        type: Boolean,
    });

    if (BUILD_MODE === "production") {
        registerWorldSchemaVersion();
    }
}

function registerWorldSchemaVersion(): void {
    game.settings.register("sf2e", "worldSchemaVersion", {
        name: "SF2E.SETTINGS.WorldSchemaVersion.Name",
        hint: "SF2E.SETTINGS.WorldSchemaVersion.Hint",
        scope: "world",
        config: true,
        default: MigrationRunner.LATEST_SCHEMA_VERSION,
        type: Number,
    });
}
