import { PartialSettingsData, SettingsMenuSF2e } from "./menu";

type ConfigSF2eListName = typeof MetagameSettings.SETTINGS[number];

export class MetagameSettings extends SettingsMenuSF2e {
    static override readonly namespace = "metagame";

    static override readonly SETTINGS = [
        "showDC",
        "showResults",
        "secretDamage",
        "secretCondition",
        "partyVision",
    ] as const;

    protected static override get settings(): Record<ConfigSF2eListName, PartialSettingsData> {
        return {
            showDC: {
                name: "SF2E.SETTINGS.Metagame.ShowDC.Name",
                hint: "SF2E.SETTINGS.Metagame.ShowDC.Hint",
                default: "gm",
                type: String,
                choices: {
                    none: "SF2E.SETTINGS.Metagame.ShowDC.None",
                    gm: "SF2E.SETTINGS.Metagame.ShowDC.Gm",
                    owner: "SF2E.SETTINGS.Metagame.ShowDC.Owner",
                    all: "SF2E.SETTINGS.Metagame.ShowDC.All",
                },
            },
            showResults: {
                name: "SF2E.SETTINGS.Metagame.ShowResults.Name",
                hint: "SF2E.SETTINGS.Metagame.ShowResults.Hint",
                default: "gm",
                type: String,
                choices: {
                    none: "SF2E.SETTINGS.Metagame.ShowResults.None",
                    gm: "SF2E.SETTINGS.Metagame.ShowResults.Gm",
                    owner: "SF2E.SETTINGS.Metagame.ShowResults.Owner",
                    all: "SF2E.SETTINGS.Metagame.ShowResults.All",
                },
            },
            secretDamage: {
                name: "SF2E.SETTINGS.Metagame.SecretDamage.Name",
                hint: "SF2E.SETTINGS.Metagame.SecretDamage.Hint",
                default: false,
                type: Boolean,
            },
            secretCondition: {
                name: "SF2E.SETTINGS.Metagame.SecretCondition.Name",
                hint: "SF2E.SETTINGS.Metagame.SecretCondition.Hint",
                default: false,
                type: Boolean,
            },
            partyVision: {
                name: "SF2E.SETTINGS.Metagame.PartyVision.Name",
                hint: "SF2E.SETTINGS.Metagame.PartyVision.Hint",
                default: false,
                type: Boolean,
            },
        };
    }
}
