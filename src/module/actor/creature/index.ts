import { ActorSF2e } from "@actor/base";
import { CreatureData } from "@actor/data";
import { ModifierSF2e, RawModifier, StatisticModifier } from "@module/modifiers";
import { ItemSF2e, ArmorSF2e } from "@item";
import { prepareMinions } from "@scripts/actor/prepare-minions";
import { RuleElementSF2e } from "@module/rules/rule-element";
import { RollNoteSF2e } from "@module/notes";
import { RuleElementSynthetics } from "@module/rules/rules-data-definitions";
import { ActiveEffectSF2e } from "@module/active-effect";
import { hasInvestedProperty } from "@item/data/helpers";
import { DegreeOfSuccessAdjustment, CheckDC } from "@system/check-degree-of-success";
import { CheckSF2e } from "@system/rolls";
import {
    Alignment,
    AlignmentComponent,
    CreatureSpeeds,
    LabeledSpeed,
    MovementType,
    VisionLevel,
    VisionLevels,
} from "./data";
import { LightLevels } from "@module/scene/data";
import { Statistic, StatisticBuilder } from "@system/statistic";
import { MeasuredTemplateSF2e, TokenSF2e } from "@module/canvas";
import { TokenDocumentSF2e } from "@scene";
import { ErrorSF2e } from "@module/utils";
import { PredicateSF2e, RawPredicate } from "@system/predication";

/** An "actor" in a Pathfinder sense rather than a Foundry one: all should contain attributes and abilities */
export abstract class CreatureSF2e extends ActorSF2e {
    /** Used as a lock to prevent multiple asynchronous redraw requests from triggering an error */
    redrawingTokenEffects = false;

    /** The creature's position on the alignment axes */
    get alignment(): Alignment {
        return this.data.data.details.alignment.value;
    }

    override get visionLevel(): VisionLevel {
        const senses = this.data.data.traits.senses;
        const senseTypes = senses
            .map((sense) => sense.type)
            .filter((senseType) => ["lowLightVision", "darkvision"].includes(senseType));
        return this.getCondition("blinded")
            ? VisionLevels.BLINDED
            : senseTypes.includes("darkvision")
            ? VisionLevels.DARKVISION
            : senseTypes.includes("lowLightVision")
            ? VisionLevels.LOWLIGHT
            : VisionLevels.NORMAL;
    }

    get hasDarkvision(): boolean {
        return this.visionLevel === VisionLevels.DARKVISION;
    }

    get hasLowLightVision(): boolean {
        return this.visionLevel >= VisionLevels.LOWLIGHT;
    }

    override get canSee(): boolean {
        if (!canvas.scene) return true;
        if (this.visionLevel === VisionLevels.BLINDED) return false;

        const lightLevel = canvas.scene.lightLevel;
        return lightLevel > LightLevels.DARKNESS || this.hasDarkvision;
    }

    get isDead(): boolean {
        const hasDeathOverlay = !this.getActiveTokens().some(
            (token) => token.data.overlayEffect !== "icons/svg/skull.svg"
        );
        return (this.hitPoints.value === 0 || hasDeathOverlay) && !this.hasCondition("dying");
    }

    get hitPoints(): { value: number; max: number; negativeHealing: boolean } {
        return {
            value: this.data.data.attributes.hp.value,
            max: this.data.data.attributes.hp.max,
            negativeHealing: this.data.data.attributes.hp.negativeHealing,
        };
    }

    get attributes(): this["data"]["data"]["attributes"] {
        return this.data.data.attributes;
    }

    get perception(): Statistic {
        const stat = this.data.data.attributes.perception as StatisticModifier;
        return this.buildStatistic(stat, "perception", "SF2E.PerceptionCheck", "perception-check");
    }

    get fortitude(): Statistic {
        return this.buildSavingThrowStatistic("fortitude");
    }

    get reflex(): Statistic {
        return this.buildSavingThrowStatistic("reflex");
    }

    get will(): Statistic {
        return this.buildSavingThrowStatistic("will");
    }

    get deception(): Statistic {
        const stat = this.data.data.skills.dec as StatisticModifier;
        return this.buildStatistic(stat, "deception", "SF2E.ActionsCheck.deception", "skill-check");
    }

    get stealth(): Statistic {
        const stat = this.data.data.skills.ste as StatisticModifier;
        return this.buildStatistic(stat, "stealth", "SF2E.ActionsCheck.stealth", "skill-check");
    }

    get wornArmor(): Embedded<ArmorSF2e> | null {
        return this.itemTypes.armor.find((armor) => armor.isEquipped && armor.isArmor) ?? null;
    }

    /** Get the held shield of most use to the wielder */
    override get heldShield(): Embedded<ArmorSF2e> | null {
        const heldShields = this.itemTypes.armor.filter((armor) => armor.isEquipped && armor.isShield);
        return heldShields.length === 0
            ? null
            : heldShields.slice(0, -1).reduce((bestShield, shield) => {
                  if (bestShield === shield) return bestShield;

                  const withBetterAC =
                      bestShield.acBonus > shield.acBonus
                          ? bestShield
                          : shield.acBonus > bestShield.acBonus
                          ? shield
                          : null;
                  const withMoreHP =
                      bestShield.hitPoints.current > shield.hitPoints.current
                          ? bestShield
                          : shield.hitPoints.current > bestShield.hitPoints.current
                          ? shield
                          : null;
                  const withBetterHardness =
                      bestShield.hardness > shield.hardness
                          ? bestShield
                          : shield.hardness > bestShield.hardness
                          ? shield
                          : null;

                  return withBetterAC ?? withMoreHP ?? withBetterHardness ?? bestShield;
              }, heldShields.slice(-1)[0]);
    }

    /** Setup base ephemeral data to be modified by active effects and derived-data preparation */
    override prepareBaseData(): void {
        super.prepareBaseData();
        const attributes = this.data.data.attributes;
        const hitPoints: { modifiers: Readonly<ModifierSF2e[]>; negativeHealing: boolean } = attributes.hp;
        hitPoints.negativeHealing = false;
        hitPoints.modifiers = [];

        // Bless raw custom modifiers as `ModifierSF2e`s
        const customModifiers = (this.data.data.customModifiers ??= {});
        Object.values(customModifiers).forEach((modifiers: RawModifier[]) => {
            [...modifiers].forEach((modifier: RawModifier) => {
                const index = modifiers.indexOf(modifier);
                modifiers[index] = ModifierSF2e.fromObject(modifier);
            });
        });
    }

    /** Apply ActiveEffect-Like rule elements immediately after application of actual `ActiveEffect`s */
    override prepareEmbeddedEntities(): void {
        super.prepareEmbeddedEntities();

        for (const rule of this.rules) {
            rule.onApplyActiveEffects();
        }
    }

    // Set whether this actor is wearing armor
    override prepareDerivedData(): void {
        super.prepareDerivedData();
        this.rollOptions.all["self:armored"] = !!this.wornArmor && this.wornArmor.category !== "unarmored";
    }

    /** Compute custom stat modifiers provided by users or given by conditions. */
    protected prepareCustomModifiers(rules: RuleElementSF2e[]): RuleElementSynthetics {
        // Collect all sources of modifiers for statistics and damage in these two maps, which map ability -> modifiers.
        const actorData = this.data;
        const synthetics: RuleElementSynthetics = {
            damageDice: {},
            statisticsModifiers: {},
            strikes: [],
            rollNotes: {},
            weaponPotency: {},
            striking: {},
            multipleAttackPenalties: {},
        };
        const statisticsModifiers = synthetics.statisticsModifiers;

        for (const rule of rules) {
            try {
                rule.onBeforePrepareData(actorData, synthetics);
            } catch (error) {
                // ensure that a failing rule element does not block actor initialization
                console.error(`SF2e | Failed to execute onBeforePrepareData on rule element ${rule}.`, error);
            }
        }

        // Get all of the active conditions (from the item array), and add their modifiers.
        const conditions = this.itemTypes.condition
            .filter((c) => c.data.flags.sf2e?.condition && c.data.data.active)
            .map((c) => c.data);

        for (const [key, value] of game.sf2e.ConditionManager.getModifiersFromConditions(conditions.values())) {
            statisticsModifiers[key] = (statisticsModifiers[key] || []).concat(value);
        }

        // Character-specific custom modifiers & custom damage dice.
        if (["character", "familiar", "npc"].includes(actorData.type)) {
            const { data } = actorData;

            // Custom Modifiers (which affect d20 rolls and damage).
            data.customModifiers = data.customModifiers ?? {};
            for (const [statistic, modifiers] of Object.entries(data.customModifiers)) {
                statisticsModifiers[statistic] = (statisticsModifiers[statistic] ?? []).concat(modifiers);
            }

            // Damage Dice (which add dice to damage rolls).
            data.damageDice = data.damageDice ?? {};
            const damageDice = synthetics.damageDice;
            for (const [attack, dice] of Object.entries(data.damageDice)) {
                damageDice[attack] = (damageDice[attack] || []).concat(dice);
            }
        }

        return synthetics;
    }

    /**
     * Adds a custom modifier that will be included when determining the final value of a stat. The
     * name parameter must be unique for the custom modifiers for the specified stat, or it will be
     * ignored.
     */
    async addCustomModifier(
        stat: string,
        name: string,
        value: number,
        type: string,
        predicate?: RawPredicate,
        damageType?: string,
        damageCategory?: string
    ) {
        // TODO: Consider adding another 'addCustomModifier' function in the future which takes a full PF2Modifier object,
        // similar to how addDamageDice operates.
        const customModifiers = duplicate(this.data.data.customModifiers ?? {});
        if (!(customModifiers[stat] ?? []).find((m) => m.name === name)) {
            const modifier = new ModifierSF2e(name, value, type);
            if (damageType) {
                modifier.damageType = damageType;
            }
            if (damageCategory) {
                modifier.damageCategory = damageCategory;
            }
            modifier.custom = true;

            // modifier predicate
            modifier.predicate = predicate instanceof PredicateSF2e ? predicate : new PredicateSF2e(predicate);
            modifier.ignored = !modifier.predicate.test!();

            customModifiers[stat] = (customModifiers[stat] ?? []).concat([modifier]);
            await this.update({ "data.customModifiers": customModifiers });
        }
    }

    /** Removes a custom modifier by name. */
    async removeCustomModifier(stat: string, modifier: number | string) {
        const customModifiers = duplicate(this.data.data.customModifiers ?? {});
        if (typeof modifier === "number" && customModifiers[stat] && customModifiers[stat].length > modifier) {
            customModifiers[stat].splice(modifier, 1);
            await this.update({ "data.customModifiers": customModifiers });
        } else if (typeof modifier === "string" && customModifiers[stat]) {
            customModifiers[stat] = customModifiers[stat].filter((m) => m.name !== modifier);
            await this.update({ "data.customModifiers": customModifiers });
        } else {
            throw Error("Custom modifiers can only be removed by name (string) or index (number)");
        }
    }

    prepareSpeed(movementType: "land", synthetics: RuleElementSynthetics): CreatureSpeeds;
    prepareSpeed(
        movementType: Exclude<MovementType, "land">,
        synthetics: RuleElementSynthetics
    ): LabeledSpeed & StatisticModifier;
    prepareSpeed(
        movementType: MovementType,
        synthetics: RuleElementSynthetics
    ): CreatureSpeeds | (LabeledSpeed & StatisticModifier);
    prepareSpeed(
        movementType: MovementType,
        synthetics: RuleElementSynthetics
    ): CreatureSpeeds | (LabeledSpeed & StatisticModifier) {
        const systemData = this.data.data;
        const rollOptions = this.getRollOptions(["all", "speed", `${movementType}-speed`]);
        const modifiers: ModifierSF2e[] = [`${movementType}-speed`, "speed"]
            .map((key) => (synthetics.statisticsModifiers[key] || []).map((modifier) => modifier.clone()))
            .flat()
            .map((modifier) => {
                modifier.ignored = !modifier.predicate.test(modifier.defaultRollOptions ?? rollOptions);
                return modifier;
            });

        if (movementType === "land") {
            const label = game.i18n.localize("SF2E.SpeedTypesLand");
            const base = Number(systemData.attributes.speed.value ?? 0);
            const stat = mergeObject(
                new StatisticModifier(game.i18n.format("SF2E.SpeedLabel", { type: label }), modifiers),
                systemData.attributes.speed,
                { overwrite: false }
            );
            stat.total = base + stat.totalModifier;
            stat.type = "land";
            stat.breakdown = [`${game.i18n.format("SF2E.SpeedBaseLabel", { type: label })} ${base}`]
                .concat(
                    stat.modifiers
                        .filter((m) => m.enabled)
                        .map((m) => `${game.i18n.localize(m.name)} ${m.modifier < 0 ? "" : "+"}${m.modifier}`)
                )
                .join(", ");
            return stat;
        } else {
            const speed = systemData.attributes.speed.otherSpeeds.find(
                (otherSpeed) => otherSpeed.type === movementType
            );
            if (!speed) throw ErrorSF2e("Unexpected missing speed");
            const base = Number(speed.value ?? 0);
            const stat = mergeObject(
                new StatisticModifier(game.i18n.format("SF2E.SpeedLabel", { type: speed.label }), modifiers),
                speed,
                { overwrite: false }
            );
            stat.total = base + stat.totalModifier;
            stat.breakdown = [`${game.i18n.format("SF2E.SpeedBaseLabel", { type: speed.label })} ${base}`]
                .concat(
                    stat.modifiers
                        .filter((m) => m.enabled)
                        .map((m) => `${game.i18n.localize(m.name)} ${m.modifier < 0 ? "" : "+"}${m.modifier}`)
                )
                .join(", ");
            return stat;
        }
    }

    override async updateEmbeddedDocuments(
        embeddedName: "ActiveEffect" | "Item",
        data: EmbeddedDocumentUpdateData<ActiveEffectSF2e | ItemSF2e>[],
        options: DocumentModificationContext = {}
    ): Promise<ActiveEffectSF2e[] | ItemSF2e[]> {
        const equippingUpdates = data.filter(
            (update) => "data.equipped.value" in update && typeof update["data.equipped.value"] === "boolean"
        );
        const wornArmor = this.wornArmor;

        for (const update of equippingUpdates) {
            if (!("data.equipped.value" in update)) continue;

            const item = this.physicalItems.get(update._id)!;
            // Allow no more than one article of armor to be equipped at a time
            if (wornArmor && item instanceof ArmorSF2e && item.isArmor && item.id !== wornArmor.id) {
                data.push({ _id: wornArmor.id, "data.equipped.value": false, "data.invested.value": false });
            }

            // Uninvested items as they're unequipped
            if (update["data.equipped.value"] === false && hasInvestedProperty(item.data)) {
                update["data.invested.value"] = false;
            }
        }

        return super.updateEmbeddedDocuments(embeddedName, data, options);
    }

    /* -------------------------------------------- */
    /*  Rolls                                       */
    /* -------------------------------------------- */

    /**
     * Roll a Recovery Check
     * Prompt the user for input regarding Advantage/Disadvantage and any Situational Bonus
     */
    rollRecovery() {
        if (this.data.type !== "character") {
            throw Error("Recovery rolls are only applicable to characters");
        }

        const dying = this.data.data.attributes.dying.value;
        // const wounded = this.data.data.attributes.wounded.value; // not needed currently as the result is currently not automated
        const recoveryMod = getProperty(this.data.data.attributes, "dying.recoveryMod") || 0;

        const dc: CheckDC = {
            label: game.i18n.format("SF2E.Recovery.rollingDescription", {
                dying,
                dc: "{dc}", // Replace variable with variable, which will be replaced with the actual value in CheckModifiersDialog.Roll()
            }),
            value: 10 + recoveryMod + dying,
            visibility: "all",
        };

        const notes: RollNoteSF2e[] = [
            new RollNoteSF2e("all", game.i18n.localize("SF2E.Recovery.critSuccess"), undefined, ["criticalSuccess"]),
            new RollNoteSF2e("all", game.i18n.localize("SF2E.Recovery.success"), undefined, ["success"]),
            new RollNoteSF2e("all", game.i18n.localize("SF2E.Recovery.failure"), undefined, ["failure"]),
            new RollNoteSF2e("all", game.i18n.localize("SF2E.Recovery.critFailure"), undefined, ["criticalFailure"]),
        ];

        const modifier = new StatisticModifier(game.i18n.localize("SF2E.FlatCheck"), []);

        CheckSF2e.roll(modifier, { actor: this, dc, notes });

        // No automated update yet, not sure if Community wants that.
        // return this.update({[`data.attributes.dying.value`]: dying}, [`data.attributes.wounded.value`]: wounded});
    }

    /** Redraw token effect icons after adding/removing partial ActiveEffects to Actor#temporaryEffects */
    redrawTokenEffects() {
        if (!(game.ready && canvas.scene) || this.redrawingTokenEffects) return;
        this.redrawingTokenEffects = true;
        const promises = Promise.allSettled(this.getActiveTokens().map((token) => token.drawEffects()));
        promises.then(() => {
            this.redrawingTokenEffects = false;
        });
    }

    protected buildStatistic(
        stat: { adjustments?: DegreeOfSuccessAdjustment[]; modifiers: readonly ModifierSF2e[]; notes?: RollNoteSF2e[] },
        name: string,
        label: string,
        type: string
    ): Statistic {
        return StatisticBuilder.from(this, {
            name: name,
            check: { adjustments: stat.adjustments, label, type },
            dc: {},
            modifiers: [...stat.modifiers],
            notes: stat.notes,
        });
    }

    private buildSavingThrowStatistic(savingThrow: "fortitude" | "reflex" | "will"): Statistic {
        const label = game.i18n.format("SF2E.SavingThrowWithName", {
            saveName: game.i18n.localize(CONFIG.SF2E.saves[savingThrow]),
        });
        return this.buildStatistic(this.data.data.saves[savingThrow], savingThrow, label, "saving-throw");
    }

    protected createAttackRollContext(event: JQuery.Event, rollNames: string[]) {
        const ctx = this.createStrikeRollContext(rollNames);
        let dc: CheckDC | undefined;
        let distance: number | undefined;
        if (ctx.target?.actor instanceof CreatureSF2e) {
            dc = {
                label: game.i18n.format("SF2E.CreatureStatisticDC.ac", {
                    creature: ctx.target.name,
                    dc: "{dc}",
                }),
                scope: "AttackOutcome",
                value: ctx.target.actor.data.data.attributes.ac.value,
            };

            // calculate distance
            const self = canvas.tokens.controlled.find((token) => token.actor?.id === this.id);
            if (self && canvas.grid?.grid instanceof SquareGrid) {
                const groundDistance = MeasuredTemplateSF2e.measureDistance(self.position, ctx.target.position);
                const elevationDiff = Math.abs(self.data.elevation - ctx.target.data.elevation);
                distance = Math.floor(Math.sqrt(Math.pow(groundDistance, 2) + Math.pow(elevationDiff, 2)));
            }
        }
        return {
            event,
            options: Array.from(new Set(ctx.options)), // de-duplication
            targets: ctx.targets,
            dc,
            distance,
        };
    }

    protected createDamageRollContext(event: JQuery.Event) {
        const ctx = this.createStrikeRollContext(["all", "damage-roll"]);
        return {
            event,
            options: Array.from(new Set(ctx.options)), // de-duplication
            targets: ctx.targets,
        };
    }

    private createStrikeRollContext(rollNames: string[]) {
        const options = this.getRollOptions(rollNames);

        const getAlignmentTraits = (alignment: Alignment): AlignmentComponent[] => {
            return [
                ...new Set([
                    ...(["LG", "NG", "CG"].includes(alignment) ? (["good"] as const) : []),
                    ...(["LE", "NE", "CE"].includes(alignment) ? (["evil"] as const) : []),
                    ...(["LG", "LN", "LE"].includes(alignment) ? (["lawful"] as const) : []),
                    ...(["CG", "CN", "CE"].includes(alignment) ? (["chaotic"] as const) : []),
                ]),
            ];
        };
        const conditions = this.itemTypes.condition.filter((condition) => condition.fromSystem);
        options.push(
            ...conditions
                .map((condition) => [`self:${condition.data.data.hud.statusName}`, `condition:${condition.slug}`])
                .flat(),
            ...getAlignmentTraits(this.alignment).map((alignment) => `trait:${alignment}`)
        );
        if (this.hitPoints.negativeHealing) {
            options.push("negative-healing");
        }

        const targets: TokenSF2e[] = Array.from(game.user.targets).filter(
            (token) => token.actor instanceof CreatureSF2e
        );
        const target = targets.length === 1 && targets[0].actor instanceof CreatureSF2e ? targets[0] : undefined;
        if (target?.actor instanceof CreatureSF2e) {
            if (target.actor.hitPoints.negativeHealing) {
                options.push("target:negative-healing");
            }

            const { itemTypes } = target.actor;
            const targetConditions = itemTypes.condition.filter((condition) => condition.fromSystem);
            const targetIsSpellcaster = itemTypes.spellcastingEntry.length > 0 && itemTypes.spell.length > 0;
            options.push(
                ...targetConditions
                    .map((condition) => [
                        `target:${condition.data.data.hud.statusName}`,
                        `target:condition:${condition.slug}`,
                    ])
                    .flat(),
                ...getAlignmentTraits(target.actor.alignment).map((alignment) => `target:trait:${alignment}`),
                ...(targetIsSpellcaster ? ["target:caster"] : []).flat()
            );
        }

        const traits = (target?.actor?.data.data.traits.traits.custom ?? "")
            .split(/[;,\\|]/)
            .map((value) => value.trim())
            .concat(target?.actor?.data.data.traits.traits.value ?? [])
            .filter((value) => !!value)
            .map((trait) => [`target:${trait}`, `target:trait:${trait}`])
            .flat();
        options.push(...traits);

        return {
            options: [...new Set(options)],
            targets: new Set(targets),
            target,
        };
    }

    /** Work around bug in which creating embedded items via actor.update doesn't trigger _onCreateEmbeddedDocuments */
    override async update(data: DocumentUpdateData<this>, options?: DocumentModificationContext): Promise<this> {
        await super.update(data, options);

        const hasItemInserts =
            Array.isArray(data.items) &&
            data.items.some((item) => item instanceof Object && !this.items.get(item.id ?? ""));
        if (hasItemInserts && this.parent instanceof TokenDocumentSF2e) {
            this.redrawTokenEffects();
        }

        return this;
    }

    /* -------------------------------------------- */
    /*  Event Listeners and Handlers                */
    /* -------------------------------------------- */

    /** Re-prepare familiars when their masters are updated */
    protected override _onUpdate(
        changed: DeepPartial<this["data"]["_source"]>,
        options: DocumentModificationContext,
        userId: string
    ): void {
        super._onUpdate(changed, options, userId);
        prepareMinions(this);
        if (changed.items && this.isToken && userId !== game.user.id) {
            this.redrawTokenEffects();
        }
    }

    protected override async _preUpdate(
        data: DeepPartial<CreatureSF2e["data"]["_source"]>,
        options: DocumentModificationContext,
        user: foundry.documents.BaseUser
    ) {
        // Clamp focus points
        const focus = data.data?.resources?.focus;
        if (focus) {
            if (typeof focus.max === "number") {
                focus.max = Math.clamped(focus.max, 0, 3);
            }

            const currentPoints = focus.value ?? this.data.data.resources.focus?.value ?? 0;
            const currentMax = focus.max ?? this.data.data.resources.focus?.max ?? 0;
            focus.value = Math.clamped(currentPoints, 0, currentMax);
        }

        // Clamp HP
        const hitPoints = data.data?.attributes?.hp;
        if (typeof hitPoints?.value === "number") {
            hitPoints.value = Math.clamped(hitPoints.value, 0, this.hitPoints.max);
        }

        await super._preUpdate(data, options, user);
    }
}

export interface CreatureSF2e {
    readonly data: CreatureData;

    /** See implementation in class */
    updateEmbeddedDocuments(
        embeddedName: "ActiveEffect",
        updateData: EmbeddedDocumentUpdateData<this>[],
        options?: DocumentModificationContext
    ): Promise<ActiveEffectSF2e[]>;
    updateEmbeddedDocuments(
        embeddedName: "Item",
        updateData: EmbeddedDocumentUpdateData<this>[],
        options?: DocumentModificationContext
    ): Promise<ItemSF2e[]>;
    updateEmbeddedDocuments(
        embeddedName: "ActiveEffect" | "Item",
        updateData: EmbeddedDocumentUpdateData<this>[],
        options?: DocumentModificationContext
    ): Promise<ActiveEffectSF2e[] | ItemSF2e[]>;
}
