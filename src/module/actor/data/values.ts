import { SkillAbbreviation } from "@actor/creature/data";
import { AbilityString } from "./base";
import { ATTACK_TYPES, DAMAGE_CATEGORIES, DAMAGE_TRAITS, DAMAGE_TYPES } from "@module/damage-calculation";

export const ABILITY_ABBREVIATIONS = ["str", "dex", "con", "int", "wis", "cha"] as const;

export const CREATURE_ACTOR_TYPES = ["character", "npc", "familiar"] as const;

export const SAVE_TYPES = ["fortitude", "reflex", "will"] as const;

export const CONDITION_TYPES = [
    "blinded",
    "broken",
    "clumsy",
    "concealed",
    "confused",
    "controlled",
    "dazzled",
    "deafened",
    "doomed",
    "drained",
    "dying",
    "encumbered",
    "enfeebled",
    "fascinated",
    "fatigued",
    "flat-footed",
    "fleeing",
    "friendly",
    "frightened",
    "grabbed",
    "helpful",
    "hidden",
    "hostile",
    "immobilized",
    "indifferent",
    "invisible",
    "observed",
    "paralyzed",
    "persistent-damage",
    "petrified",
    "prone",
    "quickened",
    "restrained",
    "sickened",
    "slowed",
    "stunned",
    "stupefied",
    "unconscious",
    "undetected",
    "unfriendly",
    "unnoticed",
    "wounded",
] as const;

export const IMMUNITY_TYPES = new Set([
    ...CONDITION_TYPES,
    ...DAMAGE_CATEGORIES,
    ...DAMAGE_TRAITS,
    ...DAMAGE_TYPES,
    "area-damage",
    "auditory",
    "confusion",
    "critical-hits",
    "curse",
    "detection",
    "death-effects",
    "disease",
    "emotion",
    "evocation",
    "fear-effects",
    "healing",
    "inhaled",
    "necromancy",
    "nonlethal-attacks",
    "object-immunities",
    "olfactory",
    "polymorph",
    "possession",
    "precision",
    "scrying",
    "sleep",
    "spellDeflection",
    "swarm-attacks",
    "swarm-mind",
    "trip",
    "visual",
] as const);

export const WEAKNESS_TYPES = new Set([
    ...ATTACK_TYPES,
    ...DAMAGE_CATEGORIES,
    ...DAMAGE_TRAITS,
    ...DAMAGE_TYPES,
    "area-damage",
    "axe",
    "critical-hits",
    "emotion",
    "precision",
    "splash-damage",
    "vampire-weaknesses",
    "vorpal",
    "vorpal-fear",
] as const);

export const RESISTANCE_TYPES = new Set([
    ...ATTACK_TYPES,
    ...DAMAGE_TRAITS,
    ...DAMAGE_TYPES,
    ...DAMAGE_CATEGORIES,
    "all",
    "area-damage",
    "critical-hits",
    "protean anatomy",
] as const);

export const SKILL_ABBREVIATIONS = [
    "acr",
    "ath",
    "com",
    "cul",
    "dec",
    "eng",
    "fin",
    "inf",
    "med",
    "mys",
    "pil",
    "sci",
    "ste",
    "sur",
    "tec",
] as const;

export const SKILL_DICTIONARY = {
    acr: "acrobatics",
    ath: "athletics",
    com: "computers",
    cul: "culture",
    dec: "deception",
    eng: "engineering",
    fin: "finesse",
    inf: "influence",
    med: "medicine",
    mys: "mysticism",
    pil: "piloting",
    sci: "science",
    ste: "stealth",
    sur: "survival",
    tec: "techlore",
};

interface SkillExpanded {
    ability: AbilityString;
    shortform: SkillAbbreviation;
}

export const SKILL_EXPANDED: Record<string, SkillExpanded> = {
    acrobatics: { ability: "dex", shortform: "acr" },
    athletics: { ability: "str", shortform: "ath" },
    computers: { ability: "int", shortform: "com" },
    culture: { ability: "cha", shortform: "cul" },
    deception: { ability: "cha", shortform: "dec" },
    engineering: { ability: "int", shortform: "eng" },
    finesse: { ability: "dex", shortform: "fin" },
    influence: { ability: "cha", shortform: "inf" },
    medicine: { ability: "wis", shortform: "med" },
    mysticism: { ability: "wis", shortform: "mys" },
    piloting: { ability: "dex", shortform: "pil" },
    science: { ability: "int", shortform: "sci" },
    stealth: { ability: "dex", shortform: "ste" },
    survival: { ability: "wis", shortform: "sur" },
    techlore: { ability: "int", shortform: "tec" },
};

export const SUPPORTED_ROLL_OPTIONS = [
    "all",
    "attack-roll",
    "damage-roll",
    "saving-throw",
    "fortitude",
    "reflex",
    "will",
    "perception",
    "initiative",
    "skill-check",
    "counteract-check",
];

export const SENSE_TYPES = [
    "darkvision",
    "greaterDarkvision",
    "lowLightVision",
    "motionsense",
    "scent",
    "echolocation",
    "tremorsense",
    "lifesense",
    "wavesense",
] as const;

export const MOVEMENT_TYPES = ["land", "burrow", "climb", "fly", "swim"] as const;
