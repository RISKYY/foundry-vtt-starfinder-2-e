import { ActorDataSF2e, CreatureData } from ".";

export function isCreatureData(actorData: ActorDataSF2e): actorData is CreatureData {
    return ["character", "npc", "familiar"].includes(actorData.type);
}
