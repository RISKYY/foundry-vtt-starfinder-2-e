import { NPCLegacyEditSheetSF2e } from "./legacy-edit-sheet";
import { DiceSF2e } from "@scripts/dice";
import { ActorSF2e } from "../base";
import { NPCSheetSF2e } from "./sheet";
import { SheetInventory } from "../sheet/data-types";
import { ActionSource, ItemDataSF2e } from "@item/data";
import { ActionSF2e } from "@item/action";
import { MeleeSF2e } from "@item/melee";
import { DamageType } from "@module/damage-calculation";
import { MeleeSystemData } from "@item/melee/data";
import { ActorDataSF2e } from "@actor/data";

interface LootSheetData {
    actor: { name: string; items: ItemDataSF2e[] };
    options: { classes: string[] };
    inventory: SheetInventory;
}

export class NPCLegacySheetSF2e extends NPCLegacyEditSheetSF2e {
    override get template() {
        if (this.isLootSheet) {
            return "systems/sf2e/templates/actors/npc/loot-sheet.html";
        }

        const path = "systems/sf2e/templates/actors/";
        if (this.actor.getFlag("sf2e", "editNPC.value")) return `${path}npc-sheet.html`;
        return `${path}npc-sheet-no-edit.html`;
    }

    static override get defaultOptions() {
        const options = super.defaultOptions;
        mergeObject(options, {
            classes: options.classes.concat("updatedNPCSheet"),
            width: 650,
            height: 680,
            showUnpreparedSpells: true,
        });
        return options;
    }

    override get title() {
        if (this.isLootSheet) {
            const actorName = this.token?.name ?? this.actor.name;
            return `${actorName} [${game.i18n.localize("SF2E.NPC.Dead")}]`;
        }
        return super.title;
    }

    override getData() {
        const sheetData = super.getData();
        /** Use the simple NPC loot-sheet variant if in loot mode */
        if (this.isLootSheet) {
            return this.getLootData(sheetData);
        }

        sheetData.flags = sheetData.actor.flags;
        if (sheetData.flags.sf2e_updatednpcsheet === undefined) sheetData.flags.sf2e_updatednpcsheet = {};
        if (sheetData.flags.sf2e_updatednpcsheet.editNPC === undefined)
            sheetData.flags.sf2e_updatednpcsheet.editNPC = { value: false };
        if (sheetData.flags.sf2e_updatednpcsheet.allSaveDetail === undefined)
            sheetData.flags.sf2e_updatednpcsheet.allSaveDetail = { value: "" };

        // Elite or Weak adjustment
        const { isElite, isWeak } = this.actor;
        sheetData.npcEliteActive = isElite ? " active" : "";
        sheetData.npcWeakActive = isWeak ? " active" : "";
        sheetData.npcEliteHidden = isWeak ? " hidden" : "";
        sheetData.npcWeakHidden = isElite ? " hidden" : "";
        sheetData.notAdjusted = !(isElite || isWeak);

        // rarity
        sheetData.actorRarities = CONFIG.SF2E.rarityTraits;
        sheetData.actorRarity = sheetData.actorRarities[sheetData.data.traits.rarity.value];
        sheetData.isNotCommon = sheetData.data.traits.rarity.value !== "common";
        // size
        sheetData.actorSize = sheetData.actorSizes[sheetData.data.traits.size.value];
        sheetData.actorTraits = (sheetData.data.traits.traits || {}).value;
        sheetData.actorAlignment = sheetData.data.details.alignment.value;
        sheetData.actorAttitudes = CONFIG.SF2E.attitude;
        sheetData.actorAttitude = sheetData.actorAttitudes[sheetData.data.traits.attitude?.value ?? "indifferent"];
        // languages
        sheetData.hasLanguages = false;
        if (
            sheetData.data.traits.languages.value &&
            Array.isArray(sheetData.data.traits.languages.value) &&
            sheetData.actor.data.traits.languages.value.length > 0
        ) {
            sheetData.hasLanguages = true;
        }

        // Skills
        sheetData.hasSkills = sheetData.actor.lores.length > 0;

        // AC Details
        sheetData.hasACDetails = sheetData.data.attributes.ac.details && sheetData.data.attributes.ac.details !== "";
        // HP Details
        sheetData.hasHPDetails = sheetData.data.attributes.hp.details && sheetData.data.attributes.hp.details !== "";

        // ********** This section needs work *************
        // Fort Details
        sheetData.hasFortDetails =
            sheetData.data.saves.fortitude.saveDetail && sheetData.data.saves.fortitude.saveDetail !== "";
        // Reflex Details
        sheetData.hasRefDetails =
            sheetData.data.saves.reflex.saveDetail && sheetData.data.saves.reflex.saveDetail !== "";
        // Will Details
        sheetData.hasWillDetails = sheetData.data.saves.will.saveDetail && sheetData.data.saves.will.saveDetail !== "";
        // All Save Details
        sheetData.hasAllSaveDetails =
            (sheetData.data.attributes.allSaves || {}).value && (sheetData.data.attributes.allSaves || {}).value !== "";

        // Immunities check
        sheetData.hasImmunities = sheetData.data.traits.di.value.length ? sheetData.data.traits.di.value : false;
        // Resistances check
        sheetData.hasResistances = sheetData.data.traits.dr.length ? Array.isArray(sheetData.data.traits.dr) : false;
        // Weaknesses check
        sheetData.hasWeaknesses = sheetData.data.traits.dv.length ? Array.isArray(sheetData.data.traits.dv) : false;

        // Speed Details check
        if (sheetData.data.attributes.speed && sheetData.data.attributes.speed.otherSpeeds)
            sheetData.hasSpeedDetails = sheetData.data.attributes.speed.otherSpeeds.length
                ? sheetData.data.attributes.speed.otherSpeeds
                : false;

        // Spellbook
        sheetData.hasSpells = sheetData.actor.spellcastingEntries.length ? sheetData.actor.spellcastingEntries : false;
        // sheetData.spellAttackBonus = sheetData.data.attributes.spelldc.value;

        const equipment: unknown[] = [];
        interface SheetActions {
            label: string;
            actions: ItemDataSF2e[];
        }
        interface ActionCategories {
            label: string;
            actions: {
                action: SheetActions;
                reaction: SheetActions;
                free: SheetActions;
                passive: SheetActions;
            };
        }
        const reorgActions: Record<"interaction" | "defensive" | "offensive", ActionCategories> = {
            interaction: {
                label: "Interaction Actions",
                actions: {
                    action: { label: "Actions", actions: [] },
                    reaction: { label: "Reactions", actions: [] },
                    free: { label: "Free Actions", actions: [] },
                    passive: { label: "Passive Actions", actions: [] },
                },
            },
            defensive: {
                label: "Defensive Actions",
                actions: {
                    action: { label: "Actions", actions: [] },
                    reaction: { label: "Reactions", actions: [] },
                    free: { label: "Free Actions", actions: [] },
                    passive: { label: "Passive Actions", actions: [] },
                },
            },
            offensive: {
                label: "Offensive Actions",
                actions: {
                    action: { label: "Actions", actions: [] },
                    reaction: { label: "Reactions", actions: [] },
                    free: { label: "Free Actions", actions: [] },
                    passive: { label: "Passive Actions", actions: [] },
                },
            },
        };
        sheetData.hasInteractionActions = false;
        sheetData.hasDefensiveActions = false;
        sheetData.hasOffensiveActions = false;
        sheetData.hasEquipment = false;
        const items: ItemDataSF2e[] = sheetData.actor.items;
        for (const itemData of items) {
            // Equipment
            if (itemData.isPhysical) {
                // non-strict because `quantity.value` can be a string
                // eslint-disable-next-line eqeqeq
                if (itemData.data.quantity.value != 1) {
                    // `i` is a copy, so we can append the quantity to it without updating the original
                    itemData.name += ` (${itemData.data.quantity.value})`;
                }
                equipment.push(itemData);
                sheetData.hasEquipment = true;
            }
            // Actions
            else if (itemData.type === "action") {
                const actionType = itemData.data.actionType.value || "action";
                const actionCategory = itemData.data.actionCategory?.value || "offensive";
                switch (actionCategory) {
                    case "interaction":
                        reorgActions.interaction.actions[actionType].actions.push(itemData);
                        sheetData.hasInteractionActions = true;
                        break;
                    case "defensive":
                        reorgActions.defensive.actions[actionType].actions.push(itemData);
                        sheetData.hasDefensiveActions = true;
                        break;
                    // Should be offensive but throw anything else in there too
                    default:
                        reorgActions.offensive.actions[actionType].actions.push(itemData);
                        sheetData.hasOffensiveActions = true;
                }
            }
            // Give Melee/Ranged an img
            else if (itemData.type === "melee") {
                itemData.img = ActorSF2e.getActionGraphics("action", 1).imageUrl;
            }
        }
        sheetData.actor.reorgActions = reorgActions;
        sheetData.actor.equipment = equipment;

        // Return data for rendering
        return sheetData;
    }

    private getLootData(data: LootSheetData) {
        data.actor.name = this.token?.name ?? this.actor.name;
        data.options.classes = data.options.classes
            .filter((cls) => !["npc-sheet", "updatedNPCSheet"].includes(cls))
            .concat("npc");
        data.inventory = NPCSheetSF2e.prototype.prepareInventory({ items: data.actor.items });

        return data;
    }

    override get isLootSheet(): boolean {
        return this.actor.isLootable && !this.actor.isOwner && this.actor.isLootableBy(game.user);
    }

    /** Increases the NPC via the Elite/Weak adjustment rules */
    npcAdjustment(increase: boolean) {
        let traits = duplicate(this.actor.data.data.traits.traits.value) ?? [];
        const isElite = traits.some((trait) => trait === "elite");
        const isWeak = traits.some((trait) => trait === "weak");

        if (increase) {
            if (isWeak) {
                console.log(`SF2e System | Adjusting NPC to become less powerful`);
                traits = traits.filter((trait) => trait !== "weak");
            } else if (!isWeak && !isElite) {
                console.log(`SF2e System | Adjusting NPC to become more powerful`);
                traits.push("elite");
            }
        } else {
            if (isElite) {
                console.log(`SF2e System | Adjusting NPC to become less powerful`);
                traits = traits.filter((trait) => trait !== "elite");
            } else if (!isElite && !isWeak) {
                console.log(`SF2e System | Adjusting NPC to become less powerful`);
                traits.push("weak");
            }
        }
        this.actor.update({ ["data.traits.traits.value"]: traits });
    }

    /**
     * Roll NPC Damage using DamageRoll
     * Rely upon the DiceSF2e.damageRoll logic for the core implementation
     */
    rollNPCDamageRoll(
        event: JQuery.ClickEvent,
        damageRoll: { die: string; damageType: DamageType },
        item: Embedded<MeleeSF2e>
    ) {
        const itemData = item.data.data;
        const rollData: ActorDataSF2e["data"] & { item?: MeleeSystemData } = duplicate(item.actor.data.data);
        const weaponDamage = damageRoll.die;
        const parts = [weaponDamage];
        const dtype = CONFIG.SF2E.damageTypes[damageRoll.damageType];

        // Append damage type to title
        let title = `${item.name} - Damage`;
        if (dtype) title += ` (${dtype})`;

        // Call the roll helper utility
        rollData.item = itemData;
        DiceSF2e.damageRoll({
            event,
            parts,
            actor: item.actor,
            data: rollData,
            title,
            speaker: ChatMessage.getSpeaker({ actor: item.actor }),
            dialogOptions: {
                width: 400,
                top: event.clientY - 80,
                left: window.innerWidth - 710,
            },
        });
    }

    /** Toggle expansion of an attackEffect ability if it exists. */
    expandAttackEffect(attackEffectName: string, event: JQuery.TriggeredEvent) {
        const actionList = $(event.currentTarget).parents("form").find(".item.action-item");
        let toggledAnything = false;
        interface MonsterAbility {
            actionType: string;
            actionCost: number;
            description: string;
        }
        const mAbilities: Record<string, MonsterAbility | undefined> = CONFIG.SF2E.monsterAbilities();
        actionList.each((_index, element) => {
            // 'this' = element found
            if ($(element).attr("data-item-name")?.trim().toLowerCase() === attackEffectName.trim().toLowerCase()) {
                $(element).find("h4").trigger("click");
                toggledAnything = true;
            }
        });
        if (!toggledAnything) {
            const newAbilityInfo = mAbilities[attackEffectName];
            if (newAbilityInfo) {
                const newAction = {
                    name: attackEffectName,
                    type: "action",
                    data: {
                        actionType: { value: newAbilityInfo.actionType },
                        actionCategory: { value: "offensive" },
                        source: { value: "" },
                        description: { value: newAbilityInfo.description },
                        traits: { value: [] },
                        actions: { value: newAbilityInfo.actionCost },
                    },
                } as unknown as PreCreate<ActionSource>;

                const traitRegEx = /(?:Traits.aspx.+?">)(?:<\w+>)*(.+?)(?:<\/\w+>)*(?:<\/a>)/g;
                const matchTraits: any[][] = [...newAbilityInfo.description.matchAll(traitRegEx)];

                for (let i = 0; i < matchTraits.length; i++) {
                    if (matchTraits[i] && matchTraits[i].length >= 2 && matchTraits[i][1]) {
                        if (!newAction.data?.traits?.value?.includes(matchTraits[i][1]))
                            newAction.data?.traits?.value?.push(matchTraits[i][1]);
                    }
                }

                ActionSF2e.create(newAction, { parent: this.actor, renderSheet: false });
            }
        }
    }

    /* -------------------------------------------- */
    /*  Event Listeners and Handlers
    /* -------------------------------------------- */

    /**
     * Activate event listeners using the prepared sheet HTML
     * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
     */
    override activateListeners(html: JQuery) {
        super.activateListeners(html);

        // Set the inventory tab as active on a loot-sheet rendering.
        if (this.isLootSheet) {
            html.find(".tab.inventory").addClass("active");
        }

        if (!this.options.editable) return;

        html.find(".npc-detail-text textarea").on("focusout", async (event) => {
            event.target.style.height = "5px";
            event.target.style.height = `${event.target.scrollHeight}px`;
        });

        html.find(".npc-detail-text textarea").each((_index, element) => {
            element.style.height = "5px";
            element.style.height = `${element.scrollHeight}px`;
        });

        html.find<HTMLInputElement>(".isNPCEditable").on("change", (event) => {
            this.actor.setFlag("sf2e", "editNPC", { value: event.target.checked });
        });

        // Attack effects
        html.find("button.npc-attackEffect").on("click", (event) => {
            event.preventDefault();
            event.stopPropagation();

            const itemId = $(event.currentTarget).parents(".item").attr("data-item-id") ?? "";
            const aId = Number($(event.currentTarget).attr("data-attackEffect"));
            const item = this.actor.items.get(itemId);
            if (!(item instanceof MeleeSF2e)) {
                console.debug("SF2e System | clicked an attackEffect, but item was not a melee");
                return;
            }

            const attackEffect = item.data.data.attackEffects.value[aId];
            console.debug("SF2e System | clicked an attackEffect:", attackEffect, event);

            // which function gets called depends on the type of button stored in the dataset attribute action
            switch (event.target.dataset.action) {
                case "npcAttackEffect":
                    this.expandAttackEffect(attackEffect, event);
                    break;
                default:
            }
        });

        html.find("a.npc-elite-adjustment").on("click", (event) => {
            event.preventDefault();
            console.log(`SF2e System | Adding Elite adjustment to NPC`);
            const eliteButton = $(event.currentTarget);
            const weakButton = eliteButton.siblings(".npc-weak-adjustment");
            eliteButton.toggleClass("active");
            weakButton.toggleClass("hidden");
            this.npcAdjustment(eliteButton.hasClass("active"));
        });
        html.find("a.npc-weak-adjustment").on("click", (event) => {
            event.preventDefault();
            console.log(`SF2e System | Adding Weak adjustment to NPC`);
            const weakButton = $(event.currentTarget);
            const eliteButton = weakButton.siblings(".npc-elite-adjustment");
            weakButton.toggleClass("active");
            eliteButton.toggleClass("hidden");
            this.npcAdjustment(!weakButton.hasClass("active"));
        });
    }
}
