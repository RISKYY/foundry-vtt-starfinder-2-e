import { ActorSheetSF2e } from "../sheet/base";
import { ErrorSF2e } from "@module/utils";
import { HazardSF2e } from ".";
import { ConsumableSF2e, SpellSF2e } from "@item";
import { ItemDataSF2e } from "@item/data";
import { SAVE_TYPES } from "@actor/data";
import { HazardSystemData } from "./data";

export class HazardSheetSF2e extends ActorSheetSF2e<HazardSF2e> {
    static override get defaultOptions() {
        const options = super.defaultOptions;
        mergeObject(options, {
            classes: options.classes.concat("hazard"),
            width: 650,
            height: 680,
        });
        return options;
    }

    /** Get the HTML template path to use depending on whether this sheet is in edit mode */
    override get template(): string {
        const path = "systems/sf2e/templates/actors/";
        if (this.actor.getFlag("sf2e", "editHazard.value")) return `${path}hazard-sheet.html`;
        return `${path}hazard-sheet-no-edit.html`;
    }

    override getData(): any {
        const sheetData = super.getData();

        // Update save labels
        for (const key of SAVE_TYPES) {
            sheetData.data.saves[key].label = CONFIG.SF2E.saves[key];
        }
        sheetData.actor.flags.editHazard ??= { value: false };
        const systemData: HazardSystemData = sheetData.data;

        for (const weakness of systemData.traits.dv) {
            weakness.label = CONFIG.SF2E.weaknessTypes[weakness.type];
        }
        for (const resistance of systemData.traits.dr) {
            resistance.label = CONFIG.SF2E.resistanceTypes[resistance.type];
        }

        return {
            ...sheetData,
            flags: sheetData.actor.flags,
            hazardTraits: CONFIG.SF2E.hazardTraits,
            actorTraits: systemData.traits.traits.value,
            actorRarities: CONFIG.SF2E.rarityTraits,
            actorRarity: CONFIG.SF2E.rarityTraits[this.actor.rarity],
            stealthDC: (systemData.attributes.stealth?.value ?? 0) + 10,
            hasStealthDescription: systemData.attributes.stealth?.details || false,
            hasImmunities: systemData.traits.di.value.length ? systemData.traits.di.value : false,
            hasResistances: systemData.traits.dr.length > 0,
            hasWeaknesses: systemData.traits.dv.length > 0,
            hasDescription: systemData.details.description || false,
            hasDisable: systemData.details.disable || false,
            hasRoutineDetails: systemData.details.routine || false,
            hasResetDetails: systemData.details.reset || false,
            hasHPDetails: systemData.attributes.hp.details || false,
            hasWillSave: systemData.saves.will.value !== 0 || false,
            brokenThreshold: Math.floor(systemData.attributes.hp.max / 2),
        };
    }

    override prepareItems(sheetData: any): void {
        const actorData = sheetData.actor;
        // Actions
        type AttackData = { label: string; items: ItemDataSF2e[]; type: "melee" };
        const attacks: Record<"melee" | "ranged", AttackData> = {
            melee: { label: "NPC Melee Attack", items: [], type: "melee" },
            ranged: { label: "NPC Ranged Attack", items: [], type: "melee" },
        };

        // Actions
        type ActionData = { label: string; actions: ItemDataSF2e[] };
        const actions: Record<string, ActionData> = {
            action: { label: "Actions", actions: [] },
            reaction: { label: "Reactions", actions: [] },
            free: { label: "Free Actions", actions: [] },
            passive: { label: "Passive Actions", actions: [] },
        };

        // Iterate through items, allocating to containers
        const weaponTraits: Record<string, string> = CONFIG.SF2E.weaponTraits;
        const traitsDescriptions: Record<string, string | undefined> = CONFIG.SF2E.traitsDescriptions;
        for (const itemData of actorData.items) {
            itemData.img = itemData.img || CONST.DEFAULT_TOKEN;

            // NPC Generic Attacks
            if (itemData.type === "melee") {
                const weaponType: "melee" | "ranged" = itemData.data.weaponType.value || "melee";
                const traits: string[] = itemData.data.traits.value;
                const isAgile = traits.includes("agile");
                itemData.data.bonus.total = Number(itemData.data.bonus.value) || 0;
                itemData.data.isAgile = isAgile;

                // get formated traits for read-only npc sheet
                itemData.traits = traits.map((trait) => ({
                    label: weaponTraits[trait] ?? trait.charAt(0).toUpperCase() + trait.slice(1),
                    description: traitsDescriptions[trait] ?? "",
                }));
                attacks[weaponType].items.push(itemData);
            }

            // Actions
            else if (itemData.type === "action") {
                const actionType = itemData.data.actionType.value || "action";
                itemData.img = HazardSF2e.getActionGraphics(
                    actionType,
                    Number(itemData.data.actions.value) || 1
                ).imageUrl;

                // get formated traits for read-only npc sheet
                const traits: string[] = itemData.data.traits.value;
                const traitObjects = traits.map((trait) => ({
                    label: weaponTraits[trait] || trait.charAt(0).toUpperCase() + trait.slice(1),
                    description: traitsDescriptions[trait] ?? "",
                }));
                if (itemData.data.actionType.value) {
                    const actionType: string = itemData.data.actionType.value;
                    traitObjects.push({
                        label: weaponTraits[actionType] || actionType.charAt(0).toUpperCase() + actionType.slice(1),
                        description: traitsDescriptions[actionType] ?? "",
                    });
                }
                itemData.traits = traitObjects;

                actions[actionType].actions.push(itemData);
            }
        }

        // Assign and return
        actorData.actions = actions;
        actorData.attacks = attacks;
    }

    /* -------------------------------------------- */
    /*  Event Listeners and Handlers                */
    /* -------------------------------------------- */

    override activateListeners(html: JQuery): void {
        super.activateListeners(html);

        // Melee Attack summaries
        html.find(".item .melee-name h4").on("click", (event) => {
            this.onItemSummary(event);
        });

        // NPC Weapon Rolling
        html.find("button").on("click", (event) => {
            event.preventDefault();
            event.stopPropagation();

            const itemId = $(event.currentTarget).parents(".item").attr("data-item-id") ?? "";
            const item = this.actor.items.get(itemId);
            if (!item) {
                throw ErrorSF2e(`Item ${itemId} not found`);
            }
            const spell = item instanceof SpellSF2e ? item : item instanceof ConsumableSF2e ? item.embeddedSpell : null;

            // which function gets called depends on the type of button stored in the dataset attribute action
            switch (event.target.dataset.action) {
                case "npcAttack":
                    item.rollNPCAttack(event);
                    break;
                case "npcAttack2":
                    item.rollNPCAttack(event, 2);
                    break;
                case "npcAttack3":
                    item.rollNPCAttack(event, 3);
                    break;
                case "npcDamage":
                    item.rollNPCDamage(event);
                    break;
                case "npcDamageCritical":
                    item.rollNPCDamage(event, true);
                    break;
                case "spellAttack": {
                    spell?.rollAttack(event);
                    break;
                }
                case "spellDamage": {
                    spell?.rollDamage(event);
                    break;
                }
                case "consume":
                    if (item instanceof ConsumableSF2e) item.consume();
                    break;
                default:
                    throw new Error("Unknown action type");
            }
        });

        if (!this.options.editable) return;

        html.find<HTMLInputElement>(".isHazardEditable").on("change", (event) => {
            this.actor.setFlag("sf2e", "editHazard", { value: event.target.checked });
        });
    }
}
