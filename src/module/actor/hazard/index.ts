import { HazardData } from "./data";
import { ActorSF2e } from "@actor/index";
import { Rarity } from "@module/data";

export class HazardSF2e extends ActorSF2e {
    static override get schema(): typeof HazardData {
        return HazardData;
    }

    get rarity(): Rarity {
        return this.data.data.traits.rarity.value;
    }

    get isComplex(): boolean {
        return this.data.data.details.isComplex;
    }
}

export interface HazardSF2e {
    readonly data: HazardData;
}
