import { SaveData } from "@actor/creature/data";
import { SaveType } from "@actor/data";
import { ActorSystemData, BaseActorDataSF2e, BaseActorSourceSF2e, BaseTraitsData } from "@actor/data/base";
import { HazardSF2e } from ".";

/** The stored source data of a hazard actor */
export type HazardSource = BaseActorSourceSF2e<"hazard", HazardSystemData>;

export class HazardData extends BaseActorDataSF2e<HazardSF2e> {
    static override DEFAULT_ICON: ImagePath = "systems/sf2e/icons/default-icons/hazard.svg";
}

/** Wrapper type for hazard-specific data. */
export interface HazardData extends Omit<HazardSource, "effects" | "flags" | "items" | "token"> {
    type: HazardSource["type"];
    data: HazardSource["data"];
    readonly _source: HazardSource;
}

interface HazardAttributes {
    ac: {
        value: number;
    };
    hasHealth: boolean;
    hp: {
        value: number;
        max: number;
        temp: number;
        details: string;
    };
    hardness: number;
    stealth: {
        value: number;
        details: string;
    };
}

/** The raw information contained within the actor data object for hazards. */
export interface HazardSystemData extends ActorSystemData {
    details: {
        isComplex: boolean;
        level: {
            value: number;
        };
        disable: string;
        description: string;
        reset: string;
        routine: string;
    };
    attributes: HazardAttributes;
    saves: Record<SaveType, SaveData>;
    /** Traits, languages, and other information. */
    traits: BaseTraitsData;
    // Fall-through clause which allows arbitrary data access; we can remove this once typing is more prevalent.
    [key: string]: any;
}
