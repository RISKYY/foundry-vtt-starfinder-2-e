import { DegreeOfSuccessString } from "@system/check-degree-of-success";
import { PredicateSF2e } from "@system/predication";

export class RollNoteSF2e {
    /** The selector used to determine on which rolls the note will be shown for. */
    selector: string;
    /** The text content of this note. */
    text: string;
    /** If true, these dice are user-provided/custom. */
    predicate?: PredicateSF2e;
    /** List of outcomes to show this note for; or all outcomes if none are specified */
    outcome: DegreeOfSuccessString[];

    constructor(selector: string, text: string, predicate?: PredicateSF2e, outcome: DegreeOfSuccessString[] = []) {
        this.selector = selector;
        this.text = text;
        this.predicate = predicate;
        this.outcome = outcome;
    }
}
