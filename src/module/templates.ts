/** Handlebars template subcomponents */
export function loadSF2ETemplates() {
    const templatePaths = [
        // effect panel
        "systems/sf2e/templates/system/effect-panel.html",

        // world clock
        "systems/sf2e/templates/system/world-clock.html",

        // Actor Sheets Partials (CRB-Style Tooltip)
        "systems/sf2e/templates/actors/crb-style/partials/modifiers-tooltip.html",

        // Actor Sheets Partials (CRB-Syle Sidebar)
        "systems/sf2e/templates/actors/crb-style/sidebar/actor-armorclass.html",
        "systems/sf2e/templates/actors/crb-style/sidebar/actor-class-dc.html",
        "systems/sf2e/templates/actors/crb-style/sidebar/actor-health.html",
        "systems/sf2e/templates/actors/crb-style/sidebar/actor-stamina.html",
        "systems/sf2e/templates/actors/crb-style/sidebar/actor-resistances.html",
        "systems/sf2e/templates/actors/crb-style/sidebar/actor-perception.html",
        "systems/sf2e/templates/actors/crb-style/sidebar/actor-initiative.html",
        "systems/sf2e/templates/actors/crb-style/sidebar/actor-saves.html",

        // Actor Sheets Partials (CRB-Style Main Section)
        "systems/sf2e/templates/actors/crb-style/actor-header.html",
        "systems/sf2e/templates/actors/crb-style/tabs/actor-character.html",
        "systems/sf2e/templates/actors/crb-style/tabs/actor-actions.html",
        "systems/sf2e/templates/actors/crb-style/tabs/actor-biography.html",
        "systems/sf2e/templates/actors/crb-style/tabs/actor-effects.html",
        "systems/sf2e/templates/actors/crb-style/tabs/actor-feats.html",
        "systems/sf2e/templates/actors/crb-style/tabs/inventory.html",
        "systems/sf2e/templates/actors/crb-style/tabs/actor-pfs.html",
        "systems/sf2e/templates/actors/crb-style/tabs/actor-skills.html",
        "systems/sf2e/templates/actors/crb-style/tabs/actor-spellbook.html",
        "systems/sf2e/templates/actors/crb-style/tabs/item-line.html",
        "systems/sf2e/templates/actors/crb-style/character-traits.html",
        "systems/sf2e/templates/actors/crb-style/character-background.html",
        "systems/sf2e/templates/actors/crb-style/character-abilities.html",

        // Actor Sheet Partials (General)
        "systems/sf2e/templates/actors/spellcasting-spell-list.html",
        "systems/sf2e/templates/actors/crb-style/partials/proficiencylevels-dropdown.html",

        // SVG icons
        "systems/sf2e/templates/actors/crb-style/icons/d20.html",
        "systems/sf2e/templates/actors/crb-style/icons/plus.html",

        // NPC partials
        "systems/sf2e/templates/actors/npc/tabs/main.html",
        "systems/sf2e/templates/actors/npc/tabs/inventory.html",
        "systems/sf2e/templates/actors/npc/tabs/spells.html",
        "systems/sf2e/templates/actors/npc/tabs/notes.html",
        "systems/sf2e/templates/actors/npc/partials/header.html",
        "systems/sf2e/templates/actors/npc/partials/sidebar.html",
        "systems/sf2e/templates/actors/npc/partials/action.html",
        "systems/sf2e/templates/actors/npc/partials/attack.html",
        "systems/sf2e/templates/actors/npc/partials/item.html",

        // Item Sheet Partials
        "systems/sf2e/templates/items/ae-tab.html",
        "systems/sf2e/templates/items/rules-panel.html",
        "systems/sf2e/templates/items/action-details.html",
        "systems/sf2e/templates/items/action-sidebar.html",
        "systems/sf2e/templates/items/ancestry-details.html",
        "systems/sf2e/templates/items/ancestry-sidebar.html",
        "systems/sf2e/templates/items/armor-details.html",
        "systems/sf2e/templates/items/armor-sidebar.html",
        "systems/sf2e/templates/items/background-details.html",
        "systems/sf2e/templates/items/background-sidebar.html",
        "systems/sf2e/templates/items/backpack-details.html",
        "systems/sf2e/templates/items/backpack-sidebar.html",
        "systems/sf2e/templates/items/treasure-sidebar.html",
        "systems/sf2e/templates/items/class-details.html",
        "systems/sf2e/templates/items/consumable-details.html",
        "systems/sf2e/templates/items/consumable-sidebar.html",
        "systems/sf2e/templates/items/condition-details.html",
        "systems/sf2e/templates/items/condition-sidebar.html",
        "systems/sf2e/templates/items/effect-sidebar.html",
        "systems/sf2e/templates/items/equipment-details.html",
        "systems/sf2e/templates/items/equipment-sidebar.html",
        "systems/sf2e/templates/items/feat-details.html",
        "systems/sf2e/templates/items/feat-sidebar.html",
        "systems/sf2e/templates/items/formula-sidebar.html",
        "systems/sf2e/templates/items/kit-details.html",
        "systems/sf2e/templates/items/kit-sidebar.html",
        "systems/sf2e/templates/items/lore-details.html",
        "systems/sf2e/templates/items/lore-sidebar.html",
        "systems/sf2e/templates/items/mystify-panel.html",
        "systems/sf2e/templates/items/spell-details.html",
        "systems/sf2e/templates/items/spell-sidebar.html",
        "systems/sf2e/templates/items/melee-details.html",
        "systems/sf2e/templates/items/weapon-details.html",
        "systems/sf2e/templates/items/weapon-sidebar.html",

        // Loot partials
        "systems/sf2e/templates/actors/loot/inventory.html",
        "systems/sf2e/templates/actors/loot/sidebar.html",

        // Vehicle partials
        "systems/sf2e/templates/actors/vehicle/vehicle-sheet.html",
        "systems/sf2e/templates/actors/vehicle/vehicle-header.html",
        "systems/sf2e/templates/actors/vehicle/sidebar/vehicle-health.html",
        "systems/sf2e/templates/actors/vehicle/sidebar/vehicle-armorclass.html",
        "systems/sf2e/templates/actors/vehicle/sidebar/vehicle-saves.html",
        "systems/sf2e/templates/actors/vehicle/sidebar/vehicle-resistances.html",
        "systems/sf2e/templates/actors/vehicle/tabs/vehicle-details.html",
        "systems/sf2e/templates/actors/vehicle/tabs/vehicle-actions.html",
        "systems/sf2e/templates/actors/vehicle/tabs/vehicle-inventory.html",
        "systems/sf2e/templates/actors/vehicle/tabs/vehicle-description.html",

        // Compendium Browser Partials
        "systems/sf2e/templates/packs/action-browser.html",
        "systems/sf2e/templates/packs/bestiary-browser.html",
        "systems/sf2e/templates/packs/inventory-browser.html",
        "systems/sf2e/templates/packs/feat-browser.html",
        "systems/sf2e/templates/packs/hazard-browser.html",
        "systems/sf2e/templates/packs/spell-browser.html",
        "systems/sf2e/templates/packs/browser-settings.html",
    ];
    return loadTemplates(templatePaths);
}
