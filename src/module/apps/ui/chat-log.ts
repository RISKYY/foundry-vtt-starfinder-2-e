import { ActorSF2e } from "@actor";
import { ChatMessageSF2e } from "@module/chat-message";
import { CheckSF2e } from "@system/rolls";

export class ChatLogSF2e extends ChatLog<ChatMessageSF2e> {
    protected override _getEntryContextOptions(): EntryContextOption[] {
        const options = super._getEntryContextOptions();

        const canApplyDamage: ContextOptionCondition = ($html) => {
            const messageId = $html.attr("data-message-id") ?? "";
            const message = game.messages.get(messageId, { strict: true });

            return (
                canvas.tokens.controlled.length > 0 && message.isRoll && $html.find(".chat-damage-buttons").length === 1
            );
        };

        const canApplyTripleDamage: ContextOptionCondition = ($li) =>
            canApplyDamage($li) && $li.find("button.triple-damage").length === 1;

        const canApplyInitiative: ContextOptionCondition = ($li) => {
            const messageId = $li.attr("data-message-id") ?? "";
            const message = game.messages.get(messageId, { strict: true });

            // Rolling PC initiative from a regular skill is difficult because of bonuses that can apply to initiative specifically (e.g. Harmlessly Cute)
            // Avoid potential confusion and misunderstanding by just allowing NPCs to roll
            const validActor = canvas.tokens.controlled[0]?.actor?.data.type === "npc";
            const validRollType = $li.find(".dice-total-setInitiative-btn").length > 0;
            return validActor && message.isRoll && validRollType;
        };

        const canHeroPointReroll: ContextOptionCondition = (li): boolean => {
            const message = game.messages.get(li.data("messageId"), { strict: true });

            const actorId = message.data.speaker.actor ?? "";
            const actor = game.actors.get(actorId);
            const canReroll = !message.getFlag("sf2e", "context")?.isReroll === true;
            return (
                canReroll &&
                actor?.data.type === "character" &&
                actor.isOwner &&
                actor.data.data.attributes.heroPoints?.rank >= 1 &&
                (message.isAuthor || game.user.isGM)
            );
        };
        const canReroll: ContextOptionCondition = (li): boolean => {
            const message = game.messages.get(li.data("messageId"), { strict: true });
            const actorId = message.data.speaker.actor ?? "";
            const isOwner = !!game.actors.get(actorId)?.isOwner;
            const canRerollMessage = !message.getFlag("sf2e", "context")?.isReroll === true;
            return canRerollMessage && isOwner && (message.isAuthor || game.user.isGM);
        };

        options.push(
            {
                name: "SF2E.DamageButton.FullContext",
                icon: '<i class="fas fa-heart-broken"></i>',
                condition: canApplyDamage,
                callback: (li: JQuery) => ActorSF2e.applyDamage(li, 1),
            },
            {
                name: "SF2E.DamageButton.HalfContext",
                icon: '<i class="fas fa-heart-broken"></i>',
                condition: canApplyDamage,
                callback: (li) => ActorSF2e.applyDamage(li, 0.5),
            },
            {
                name: "SF2E.DamageButton.DoubleContext",
                icon: '<i class="fas fa-heart-broken"></i>',
                condition: canApplyDamage,
                callback: (li) => ActorSF2e.applyDamage(li, 2),
            },
            {
                name: "SF2E.DamageButton.TripleContext",
                icon: '<i class="fas fa-heart-broken"></i>',
                condition: canApplyTripleDamage,
                callback: (li) => ActorSF2e.applyDamage(li, 3),
            },
            {
                name: "SF2E.DamageButton.HealingContext",
                icon: '<i class="fas fa-heart"></i>',
                condition: canApplyDamage,
                callback: (li: JQuery) => ActorSF2e.applyDamage(li, -1),
            },
            {
                name: "SF2E.ClickToSetInitiativeContext",
                icon: '<i class="fas fa-fist-raised"></i>',
                condition: canApplyInitiative,
                callback: (li) => ActorSF2e.setCombatantInitiative(li),
            },
            {
                name: "SF2E.RerollMenu.HeroPoint",
                icon: '<i class="fas fa-hospital-symbol"></i>',
                condition: canHeroPointReroll,
                callback: (li) =>
                    CheckSF2e.rerollFromMessage(game.messages.get(li.data("messageId"), { strict: true }), {
                        heroPoint: true,
                    }),
            },
            {
                name: "SF2E.RerollMenu.KeepNew",
                icon: '<i class="fas fa-dice"></i>',
                condition: canReroll,
                callback: (li) =>
                    CheckSF2e.rerollFromMessage(game.messages.get(li.data("messageId"), { strict: true })),
            },
            {
                name: "SF2E.RerollMenu.KeepWorst",
                icon: '<i class="fas fa-dice-one"></i>',
                condition: canReroll,
                callback: (li) =>
                    CheckSF2e.rerollFromMessage(game.messages.get(li.data("messageId"), { strict: true }), {
                        keep: "worst",
                    }),
            },
            {
                name: "SF2E.RerollMenu.KeepBest",
                icon: '<i class="fas fa-dice-six"></i>',
                condition: canReroll,
                callback: (li) =>
                    CheckSF2e.rerollFromMessage(game.messages.get(li.data("messageId"), { strict: true }), {
                        keep: "best",
                    }),
            }
        );

        return options;
    }
}
