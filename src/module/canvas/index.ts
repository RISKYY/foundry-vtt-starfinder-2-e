import { SceneSF2e } from "@module/scene";
import { AmbientLightSF2e } from "./ambient-light";
import { DarkvisionLayerSF2e } from "./darkvision-layer";
import { LightingLayerSF2e } from "./lighting-layer";
import { MeasuredTemplateSF2e } from "./measured-template";
import { SightLayerSF2e } from "./sight-layer";
import { TemplateLayerSF2e } from "./template-layer";
import { TokenSF2e } from "./token";

export interface CanvasSF2e
    extends Canvas<SceneSF2e, AmbientLightSF2e, MeasuredTemplateSF2e, TokenSF2e, SightLayerSF2e> {
    darkvision: DarkvisionLayerSF2e;
}

export {
    AmbientLightSF2e,
    DarkvisionLayerSF2e,
    MeasuredTemplateSF2e,
    TokenSF2e,
    LightingLayerSF2e,
    SightLayerSF2e,
    TemplateLayerSF2e,
};
