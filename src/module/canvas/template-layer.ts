import { MeasuredTemplateSF2e } from "./";

export class TemplateLayerSF2e<
    TMeasuredTemplate extends MeasuredTemplateSF2e = MeasuredTemplateSF2e
> extends TemplateLayer<TMeasuredTemplate> {
    protected override _onMouseWheel(event: WheelEvent) {
        // Abort if there's no hovered template
        const template = this._hover;
        if (!template) return;

        // Determine the incremental angle of rotation from event data
        const snap = template.type === "cone" ? (event.shiftKey ? 45 : 15) : event.shiftKey ? 15 : 5;
        const delta = snap * Math.sign(event.deltaY);
        return template.rotate(template.data.direction + delta, snap);
    }
}
