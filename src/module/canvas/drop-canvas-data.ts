import type { ItemSF2e } from "@item";

export type DropCanvasItemDataSF2e = DropCanvasData<"Item", ItemSF2e> & {
    value?: number;
    level?: number;
};

export type DropCanvasDataSF2e<T extends string = string, D extends object = object> = T extends "Item"
    ? DropCanvasItemDataSF2e
    : DropCanvasData<T, D>;
