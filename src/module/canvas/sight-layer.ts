import { FogExplorationSF2e } from "@module/fog-exploration";
import { TokenSF2e } from "./token";

export class SightLayerSF2e extends SightLayer<TokenSF2e, FogExplorationSF2e> {
    /** Is the rules-based vision setting enabled? */
    get rulesBasedVision(): boolean {
        const settingEnabled = game.settings.get("sf2e", "automation.rulesBasedVision");
        return canvas.ready && !!canvas.scene && this.tokenVision && settingEnabled;
    }

    get hasLowLightVision(): boolean {
        return this.rulesBasedVision && this.sources.some((source) => source.object.hasLowLightVision);
    }

    get hasDarkvision(): boolean {
        return this.rulesBasedVision && this.sources.some((source) => source.object.hasDarkvision);
    }

    /** Re-prepare scene data prior to this layer's initialization */
    override async initialize(): Promise<void> {
        canvas.scene?.prepareData();
        await super.initialize();
    }

    override refresh({ forceUpdateFog = false, noUpdateFog = false } = {}): void {
        super.refresh({ forceUpdateFog, noUpdateFog });
        canvas.darkvision.refresh();
    }
}
