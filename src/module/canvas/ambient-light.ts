import { AmbientLightDocumentSF2e } from "@module/scene";
import { LightingLayerSF2e } from "./lighting-layer";

export class AmbientLightSF2e extends AmbientLight<AmbientLightDocumentSF2e> {
    /** Return a bright radius of the greater of dim and bright radii if any controlled tokens have low-light vision */
    override get brightRadius(): number {
        const ignoreLight = this.source.isDarkness || !this.visible;
        if (!canvas.sight.rulesBasedVision || ignoreLight) {
            return super.brightRadius;
        }

        const lowLightTokens = canvas.tokens.controlled.filter((token) => token.hasLowLightVision);
        return lowLightTokens.length > 0 ? Math.max(this.dimRadius, super.brightRadius) : super.brightRadius;
    }

    /** Is this light actually a source of darkness? */
    get isDarkness() {
        return this.source.isDarkness;
    }
}

export interface AmbientLightSF2e {
    get layer(): LightingLayerSF2e<this>;
}
