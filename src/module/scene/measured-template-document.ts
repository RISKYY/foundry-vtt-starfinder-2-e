import { MeasuredTemplateSF2e } from "@module/canvas/measured-template";
import { SceneSF2e } from "./document";

export class MeasuredTemplateDocumentSF2e extends MeasuredTemplateDocument {}

export interface MeasuredTemplateDocumentSF2e extends MeasuredTemplateDocument {
    readonly parent: SceneSF2e | null;

    readonly _object: MeasuredTemplateSF2e | null;
}
