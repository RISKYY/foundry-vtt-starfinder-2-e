import { LightLevels, SceneDataSF2e } from "./data";
import { SceneConfigSF2e } from "./sheet";
import { AmbientLightDocumentSF2e, MeasuredTemplateDocumentSF2e, TileDocumentSF2e, TokenDocumentSF2e } from ".";

export class SceneSF2e extends Scene<
    AmbientLightDocumentSF2e,
    MeasuredTemplateDocumentSF2e,
    TileDocumentSF2e,
    TokenDocumentSF2e
> {
    /** Toggle Unrestricted Global Vision according to scene darkness level */
    override prepareBaseData() {
        super.prepareBaseData();
        if (canvas.sight?.rulesBasedVision) {
            this.data.globalLightThreshold = 1 - LightLevels.DARKNESS;
            this.data.globalLight = true;
        }

        this.data.flags.sf2e ??= { syncDarkness: "default" };
        this.data.flags.sf2e.syncDarkness ??= "default";
    }

    get lightLevel(): number {
        return 1 - this.data.darkness;
    }

    /** Redraw the darkvision layer if the scene background or dimensions were changed */
    protected override _onUpdate(
        changed: DeepPartial<this["data"]["_source"]>,
        options: DocumentModificationContext,
        userId: string
    ) {
        super._onUpdate(changed, options, userId);
        if (["img", "width", "height", "padding"].some((key) => key in changed)) {
            canvas.darkvision.draw();
        }
    }
}

export interface SceneSF2e {
    _sheet: SceneConfigSF2e | null;

    readonly data: SceneDataSF2e<this>;

    get sheet(): SceneConfigSF2e;

    getFlag(scope: "sf2e", key: "syncDarkness"): "enabled" | "disabled" | "default";
    getFlag(scope: string, key: string): unknown;
}
