import { TokenDocumentSF2e } from "@scene";

export interface TokenDataSF2e<T extends TokenDocumentSF2e> extends foundry.data.TokenData<T> {
    flags: {
        sf2e: {
            [key: string]: unknown;
            linkToActorSize: boolean;
        };
        [key: string]: Record<string, unknown>;
    };
}
