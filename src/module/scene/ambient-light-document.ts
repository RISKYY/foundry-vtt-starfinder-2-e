import { AmbientLightSF2e } from "@module/canvas";
import { SceneSF2e } from ".";

class AmbientLightDocumentSF2e extends AmbientLightDocument {
    /** Is this light actually a source of darkness? */
    get isDarkness(): boolean {
        return this.object.source.isDarkness;
    }

    protected override _onCreate(
        data: this["data"]["_source"],
        options: SceneEmbeddedModificationContext,
        userId: string
    ) {
        super._onCreate(data, options, userId);
        canvas.darkvision.draw();
    }

    protected override _onUpdate(
        changed: DeepPartial<this["data"]["_source"]>,
        options: SceneEmbeddedModificationContext,
        userId: string
    ) {
        super._onUpdate(changed, options, userId);
        canvas.darkvision.draw();
    }

    protected override _onDelete(options: SceneEmbeddedModificationContext, userId: string) {
        super._onDelete(options, userId);
        canvas.darkvision.draw();
    }
}

interface AmbientLightDocumentSF2e extends AmbientLightDocument {
    readonly data: foundry.data.AmbientLightData<AmbientLightDocumentSF2e>;

    readonly parent: SceneSF2e | null;

    get object(): AmbientLightSF2e;
}

export { AmbientLightDocumentSF2e };
