import { ZeroToTwo } from "@module/data";
import type {
    AmbientLightDocumentSF2e,
    MeasuredTemplateDocumentSF2e,
    SceneSF2e,
    TileDocumentSF2e,
    TokenDocumentSF2e,
} from ".";

export interface SceneDataSF2e<T extends SceneSF2e>
    extends foundry.data.SceneData<
        T,
        TokenDocumentSF2e,
        AmbientLightDocumentSF2e,
        AmbientSoundDocument,
        DrawingDocument,
        MeasuredTemplateDocumentSF2e,
        NoteDocument,
        TileDocumentSF2e,
        WallDocument
    > {
    flags: {
        sf2e: {
            [key: string]: unknown;
            syncDarkness: "enabled" | "disabled" | "default";
        };
        [key: string]: Record<string, unknown>;
    };
}

export enum LightLevels {
    DARKNESS = 1 / 4,
    DIM_LIGHT = 3 / 4,
    BRIGHT_LIGHT = 1,
}

export type LightLevel = ZeroToTwo;
