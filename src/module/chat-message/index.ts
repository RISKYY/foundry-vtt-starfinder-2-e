import type { ActorSF2e } from "@actor";
import { CheckModifiersContext, RollDataSF2e } from "@system/rolls";
import { ChatCards } from "./listeners/cards";
import { CriticalHitAndFumbleCards } from "./crit-fumble-cards";
import { ItemSF2e } from "@item";
import { TokenSF2e } from "@module/canvas";
import { ModifierSF2e } from "@module/modifiers";
import { InlineRollsLinks } from "@scripts/ui/inline-roll-links";
import { DamageButtons } from "./listeners/damage-buttons";
import { DegreeOfSuccessHighlights } from "./listeners/degree-of-success";
import { DamageChatCard } from "@system/damage/chat-card";
import { ChatMessageDataSF2e, ChatMessageFlagsSF2e, ChatMessageSourceSF2e } from "./data";

class ChatMessageSF2e extends ChatMessage<ActorSF2e> {
    /** The chat log doesn't wait for data preparation before rendering, so set some data in the constructor */
    constructor(
        data: DeepPartial<ChatMessageSourceSF2e> = {},
        context: DocumentConstructionContext<ChatMessageSF2e> = {}
    ) {
        data.flags ??= { core: {}, sf2e: {} };
        data.flags.core ??= {};
        data.flags.sf2e ??= {};
        super(data, context);
    }

    /** Is this a damage (or a manually-inputed non-D20) roll? */
    get isDamageRoll(): boolean {
        if (this.isRoll && this.roll.terms.some((term) => term instanceof FateDie || term instanceof Coin)) {
            return false;
        }
        const isDamageRoll = !!this.data.flags.sf2e.damageRoll;
        const fromRollTable = !!this.data.flags.core.RollTable;
        const isRoll = isDamageRoll || this.isRoll;
        const isD20 = (isRoll && this.roll && this.roll.dice[0]?.faces === 20) || false;
        return isRoll && !(isD20 || fromRollTable);
    }

    /** Get the actor associated with this chat message */
    get actor(): ActorSF2e | null {
        const fromItem = ((): ActorSF2e | null => {
            const origin = this.data.flags.sf2e?.origin ?? null;
            const match = /^(Actor|Scene)\.(\w+)/.exec(origin?.uuid ?? "") ?? [];
            switch (match[1]) {
                case "Actor": {
                    return game.actors.get(match[2]) ?? null;
                }
                case "Scene": {
                    const scene = game.scenes.get(match[2]);
                    const tokenId = /(?<=Token\.)(\w+)/.exec(origin!.uuid)?.[1] ?? "";
                    const token = scene?.tokens.get(tokenId);
                    return token?.actor ?? null;
                }
                default: {
                    return null;
                }
            }
        })();

        return fromItem ?? ChatMessageSF2e.getSpeakerActor(this.data.speaker);
    }

    /** Get the owned item associated with this chat message */
    get item(): Embedded<ItemSF2e> | null {
        const domItem = this.getItemFromDOM();
        if (domItem) return domItem;

        const origin = this.data.flags.sf2e?.origin ?? null;
        const match = /Item\.(\w+)/.exec(origin?.uuid ?? "") ?? [];
        return this.actor?.items.get(match?.[1] ?? "") ?? null;
    }

    /** Get stringified item source from the DOM-rendering of this chat message */
    getItemFromDOM(): Embedded<ItemSF2e> | null {
        const $domMessage = $("ol#chat-log").children(`li[data-message-id="${this.id}"]`);
        const sourceString = $domMessage.find("div.sf2e.item-card").attr("data-embedded-item") ?? "null";
        try {
            const itemSource = JSON.parse(sourceString);
            const item = itemSource ? new ItemSF2e(itemSource, { parent: this.actor }) : null;
            return item as Embedded<ItemSF2e> | null;
        } catch (_error) {
            return null;
        }
    }

    /**
     * Avoid triggering Foundry 0.8.8 bug in which a speaker with no alias and a deleted actor can cause and unhandled
     * exception to be thrown
     */
    override get alias(): string {
        const speaker = this.data.speaker;
        return speaker.alias ?? game.actors.get(speaker.actor ?? "")?.name ?? this.user?.name ?? "";
    }

    /** Get the token of the speaker if possible */
    get token(): TokenSF2e | null {
        if (!canvas.ready) return null;
        const tokenId = this.data.speaker.token;
        return canvas.tokens.placeables.find((token) => token.id === tokenId) ?? null;
    }

    override async getHTML(): Promise<JQuery> {
        const $html = await super.getHTML();

        if (this.isDamageRoll) {
            await DamageButtons.append(this, $html);

            // Clean up styling of old damage messages
            $html.find(".flavor-text > div:has(.tags)").removeAttr("style").attr({ "data-sf2e-deprecated": true });
        }

        CriticalHitAndFumbleCards.appendButtons(this, $html);

        ChatCards.listen($html);
        InlineRollsLinks.listen($html);
        DegreeOfSuccessHighlights.listen(this, $html);

        $html.on("mouseenter", () => this.onHoverIn());
        $html.on("mouseleave", () => this.onHoverOut());
        $html.find(".message-sender").on("click", this.onClick.bind(this));

        return $html;
    }

    private onHoverIn(): void {
        const token = this.token;
        if (token?.isVisible && !token.isControlled) {
            token.emitHoverIn();
        }
    }

    private onHoverOut(): void {
        this.token?.emitHoverOut();
    }

    private onClick(event: JQuery.ClickEvent): void {
        event.preventDefault();
        const token = this.token;
        if (token?.isVisible) {
            token.isControlled ? token.release() : token.control({ releaseOthers: !event.shiftKey });
        }
    }

    protected override async _preCreate(
        data: PreDocumentId<this["data"]["_source"]>,
        options: DocumentModificationContext,
        user: foundry.documents.BaseUser
    ): Promise<void> {
        data.flags ??= { core: {}, sf2e: {} };
        this.data.flags = expandObject(this.data.flags ?? { core: {}, sf2e: {} }) as ChatMessageFlagsSF2e;
        if (this.isDamageRoll && game.settings.get("sf2e", "automation.experimentalDamageFormatting")) {
            await DamageChatCard.preformat(this, data);
            data.flags.sf2e.preformatted = "both";
            this.data.update(data);
        }
        return super._preCreate(data, options, user);
    }

    protected override _onCreate(
        data: foundry.data.ChatMessageSource,
        options: DocumentModificationContext,
        userId: string
    ) {
        super._onCreate(data, options, userId);

        // Handle critical hit and fumble card drawing
        if (this.isRoll && game.settings.get("sf2e", "drawCritFumble")) {
            CriticalHitAndFumbleCards.handleDraw(this);
        }
    }
}

interface ChatMessageSF2e extends ChatMessage<ActorSF2e> {
    readonly data: ChatMessageDataSF2e<this>;

    get roll(): Rolled<Roll<RollDataSF2e>>;

    getFlag(scope: "core", key: "RollTable"): unknown;
    getFlag(scope: "sf2e", key: "damageRoll"): object | undefined;
    getFlag(scope: "sf2e", key: "modifierName"): string | undefined;
    getFlag(scope: "sf2e", key: "modifiers"): ModifierSF2e[] | undefined;
    getFlag(scope: "sf2e", key: "context"): (CheckModifiersContext & { rollMode: RollMode }) | undefined;
}

declare namespace ChatMessageSF2e {
    function getSpeakerActor(speaker: foundry.data.ChatSpeakerSource | foundry.data.ChatSpeakerData): ActorSF2e | null;
}

export { ChatMessageSF2e };
