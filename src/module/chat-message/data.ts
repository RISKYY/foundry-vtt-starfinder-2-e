import { ItemType } from "@item/data";
import { RawModifier } from "@module/modifiers";
import { CheckModifiersContext } from "@system/rolls";
import { ChatMessageSF2e } from ".";

export interface ChatMessageDataSF2e<TChatMessage extends ChatMessageSF2e = ChatMessageSF2e>
    extends foundry.data.ChatMessageData<TChatMessage> {
    readonly _source: ChatMessageSourceSF2e;
    flags: ChatMessageFlagsSF2e;
}

export interface ChatMessageSourceSF2e extends foundry.data.ChatMessageSource {
    flags: ChatMessageFlagsSF2e;
}

export type ChatMessageFlagsSF2e = Record<string, Record<string, unknown>> & {
    sf2e: {
        damageRoll?: boolean;
        context?: (CheckModifiersContext & { rollMode: RollMode }) | undefined;
        origin?: { type: ItemType; uuid: string } | null;
        modifierName?: string;
        modifiers?: RawModifier[];
        preformatted?: "flavor" | "content" | "both";
        [key: string]: unknown;
    };
    core: {
        canPopout?: boolean;
        RollTable?: string;
        [key: string]: unknown;
    };
};
