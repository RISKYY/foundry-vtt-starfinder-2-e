const USER_SETTINGS_KEYS = ["uiTheme", "showEffectPanel", "showRollDialogs", "darkvisionFilter"] as const;

/** Player-specific settings, stored as flags on each world User */
export class PlayerConfigSF2e extends FormApplication {
    settings: UserSettingsSF2e;

    constructor() {
        super();
        this.settings = game.user.settings;
    }

    static readonly defaultSettings: UserSettingsSF2e = {
        uiTheme: "blue",
        showEffectPanel: true,
        showRollDialogs: true,
        darkvisionFilter: false,
    };

    static override get defaultOptions(): Required<FormApplicationOptions> {
        return mergeObject(super.defaultOptions, {
            id: "sf2e-player-config-panel",
            title: "SF2e Player Settings",
            template: "systems/sf2e/templates/user/player-config.html",
            classes: ["sheet"],
            width: 500,
            height: "auto",
            resizable: false,
        });
    }

    override getData(): PlayerConfigData {
        return { ...super.getData(), ...this.settings, developMode: BUILD_MODE === "development" };
    }

    static activateColorScheme(): void {
        console.debug("SF2e System | Activating Player Configured color scheme");
        const color = game.user.getFlag("sf2e", "settings.uiTheme") ?? PlayerConfigSF2e.defaultSettings.uiTheme;

        const cssLink = `<link id="sf2e-color-scheme" href="systems/sf2e/styles/user/color-scheme-${color}.css" rel="stylesheet" type="text/css">`;
        $("head").append(cssLink);
    }

    /**
     * Creates a div for the module and button for the Player Configuration
     * @param html the html element where the button will be created
     */
    static hookOnRenderSettings(): void {
        Hooks.on("renderSettings", (_app: SettingsConfig, html: JQuery) => {
            const configButton = $(
                `<button id="sf2e-player-config" data-action="sf2e-player-config">
                    <i class="fas fa-cogs"></i> ${PlayerConfigSF2e.defaultOptions.title}
                 </button>`
            );

            const setupButton = html.find("#settings-game");
            setupButton.prepend(configButton);

            configButton.on("click", () => {
                new PlayerConfigSF2e().render(true);
            });
        });
    }

    async _updateObject(_event: Event, formData: Record<string, unknown> & UserSettingsSF2e): Promise<void> {
        const settings = USER_SETTINGS_KEYS.reduce((currentSettings: Record<UserSettingsKey, unknown>, key) => {
            currentSettings[key] = formData[key] ?? this.settings[key];
            return currentSettings;
        }, this.settings);

        await game.user.setFlag("sf2e", `settings`, settings);
        $("link#sf2e-color-scheme").attr({ href: `systems/sf2e/styles/user/color-scheme-${formData["uiTheme"]}.css` });
    }
}

interface PlayerConfigData extends FormApplicationData, UserSettingsSF2e {
    developMode: boolean;
}

type UserSettingsKey = typeof USER_SETTINGS_KEYS[number];
export interface UserSettingsSF2e {
    uiTheme: "blue" | "red" | "original" | "ui";
    showEffectPanel: boolean;
    showRollDialogs: boolean;
    darkvisionFilter: boolean;
}
