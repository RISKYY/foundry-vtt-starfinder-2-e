import { UserSF2e } from "./document";
import { UserSettingsSF2e } from "./player-config";

export interface UserDataSF2e<T extends UserSF2e> extends foundry.data.UserData<T> {
    _source: UserSourceSF2e;

    flags: {
        sf2e: {
            [key: string]: unknown;
            settings: UserSettingsSF2e;
        };
        [key: string]: Record<string, unknown>;
    };
}

interface UserSourceSF2e extends foundry.data.UserSource {
    flags: {
        sf2e: {
            [key: string]: unknown;
            settings: UserSettingsSF2e;
        };
        [key: string]: Record<string, unknown>;
    };
}
