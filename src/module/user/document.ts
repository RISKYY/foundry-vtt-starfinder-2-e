import { ActorSF2e } from "@actor/base";
import { UserDataSF2e } from "./data";
import { PlayerConfigSF2e, UserSettingsSF2e } from "./player-config";

export class UserSF2e extends User<ActorSF2e> {
    override prepareData(): void {
        super.prepareData();
        if (canvas.ready && canvas.tokens.controlled.length > 0) {
            game.sf2e.effectPanel.refresh();
        }
    }

    /** Set user settings defaults */
    override prepareBaseData(): void {
        super.prepareBaseData();
        this.data.flags = mergeObject(
            {
                sf2e: {
                    settings: deepClone(PlayerConfigSF2e.defaultSettings),
                },
            },
            this.data.flags
        );
    }

    get settings(): UserSettingsSF2e {
        return deepClone(this.data.flags.sf2e.settings);
    }

    protected override _onUpdate(
        changed: DeepPartial<this["data"]["_source"]>,
        options: DocumentModificationContext,
        userId: string
    ) {
        super._onUpdate(changed, options, userId);
        const filterSetting = changed.flags?.sf2e?.settings?.darkvisionFilter;
        if (filterSetting && game.user.id === this.id) canvas.darkvision.draw();
    }
}

export interface UserSF2e extends User<ActorSF2e> {
    readonly data: UserDataSF2e<this>;

    getFlag(
        scope: "sf2e",
        key: "settings"
    ): {
        uiTheme: "blue" | "red" | "original" | "ui";
        showEffectPanel: boolean;
        showRollDialogs: boolean;
    };
    getFlag(scope: "sf2e", key: "settings.uiTheme"): "blue" | "red" | "original" | "ui";
    getFlag(scope: "sf2e", key: "settings.showEffectPanel"): boolean;
    getFlag(scope: "sf2e", key: "settings.showRollDialogs"): boolean;
    getFlag(scope: "sf2e", key: `compendiumFolders.${string}.expanded`): boolean | undefined;
}
