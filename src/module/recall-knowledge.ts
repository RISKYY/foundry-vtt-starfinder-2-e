/**
 * Implementation of Creature Identification
 * https://2e.aonprd.com/Rules.aspx?ID=566
 * https://2e.aonprd.com/Skills.aspx?ID=5&General=true
 *
 * See https://www.youtube.com/watch?v=UtNS1vM7czM for interpretations
 */

import { SkillAbbreviation } from "@actor/creature/data";
import { NPCSystemData } from "@actor/npc/data";
import { Rarity } from "@module/data";
import {
    adjustDC,
    calculateDC,
    combineDCAdjustments,
    createDifficultyScale,
    DCAdjustment,
    DCOptions,
    NegativeDCAdjustment,
    rarityToDCAdjustment,
} from "./dc";
import { toNumber } from "./utils";

const identifySkills = new Map<string, SkillAbbreviation[]>();
identifySkills.set("aberration", ["mys"]);
identifySkills.set("animal", ["sci"]);
identifySkills.set("astral", ["sci"]);
identifySkills.set("beast", ["tec", "sci"]);
identifySkills.set("celestial", ["mys"]);
identifySkills.set("construct", ["tec", "eng"]);
identifySkills.set("dragon", ["tec"]);
identifySkills.set("elemental", ["tec", "sci"]);
identifySkills.set("ethereal", ["tec"]);
identifySkills.set("fey", ["tec"]);
identifySkills.set("fiend", ["mys"]);
identifySkills.set("fungus", ["sci"]);
identifySkills.set("humanoid", ["cul", "med"]);
identifySkills.set("monitor", ["mys"]);
identifySkills.set("ooze", ["mys"]);
identifySkills.set("plant", ["sci"]);
identifySkills.set("spirit", ["mys"]);
identifySkills.set("undead", ["mys"]);

export interface RecallKnowledgeDC {
    dc: number;
    progression: number[];
    start: DCAdjustment;
}

export interface IdentifyCreatureData {
    skill: RecallKnowledgeDC;
    specificLoreDC: RecallKnowledgeDC;
    unspecificLoreDC: RecallKnowledgeDC;
    skills: Set<SkillAbbreviation>;
}

function toKnowledgeDC(dc: number, rarity: Rarity, loreAdjustment: NegativeDCAdjustment = "normal"): RecallKnowledgeDC {
    const rarityAdjustment = rarityToDCAdjustment(rarity);
    const start = combineDCAdjustments(rarityAdjustment, loreAdjustment);
    const progression = createDifficultyScale(dc, start);
    return {
        dc: adjustDC(dc, start),
        progression,
        start,
    };
}

export function identifyCreature(
    creature: { data: NPCSystemData },
    { proficiencyWithoutLevel = false }: DCOptions = {}
): IdentifyCreatureData {
    const rarity = creature.data.traits.rarity.value ?? "common";
    const level = toNumber(creature.data.details.level?.value) ?? 0;
    const dc = calculateDC(level, { proficiencyWithoutLevel });

    const traits = creature.data.traits.traits.value;
    const skills = new Set(traits.flatMap((trait) => identifySkills.get(trait) ?? []));

    return {
        specificLoreDC: toKnowledgeDC(dc, rarity, "very easy"),
        unspecificLoreDC: toKnowledgeDC(dc, rarity, "easy"),
        skill: toKnowledgeDC(dc, rarity, "normal"),
        skills,
    };
}
