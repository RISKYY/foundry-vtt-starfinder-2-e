import { FamiliarSF2e } from "@actor/index";
import type { CreatureSF2e } from "@actor/index";

/* (Re)prepare data of familiars with masters */
export function prepareMinions(master?: CreatureSF2e) {
    const familiars = game.actors.filter(
        (actor) => actor instanceof FamiliarSF2e && (!master || actor.data.data.master?.id === master.id)
    );
    for (const familiar of familiars) {
        familiar.prepareData();
    }
}
