import { ActorSF2e } from "@actor";
import { ItemSF2e } from "@item";
import { MystifiedTraits } from "@item/data/values";
import { ActiveEffectSF2e } from "@module/active-effect";
import { FogExplorationSF2e } from "@module/fog-exploration";
import {
    AmbientLightSF2e,
    DarkvisionLayerSF2e,
    LightingLayerSF2e,
    MeasuredTemplateSF2e,
    SightLayerSF2e,
    TokenSF2e,
    TemplateLayerSF2e,
} from "@module/canvas";
import { ChatLogSF2e, CompendiumDirectorySF2e, EncounterTrackerSF2e } from "@module/apps/ui";
import { ChatMessageSF2e } from "@module/chat-message";
import { CombatSF2e } from "@module/combat";
import { CombatantSF2e } from "@module/combatant";
import { FolderSF2e } from "@module/folder";
import { registerHandlebarsHelpers } from "@module/handlebars";
import { MacroSF2e } from "@module/macro";
import {
    AmbientLightDocumentSF2e,
    MeasuredTemplateDocumentSF2e,
    SceneSF2e,
    TileDocumentSF2e,
    TokenConfigSF2e,
    TokenDocumentSF2e,
} from "@module/scene";
import { SceneConfigSF2e } from "@module/scene/sheet";
import { registerSettings } from "@module/settings";
import { loadSF2ETemplates } from "@module/templates";
import { PlayerConfigSF2e } from "@module/user/player-config";
import { SF2ECONFIG } from "../config";
import { UserSF2e } from "@module/user";
import { JournalSheetSF2e } from "@module/journal-entry/sheet";

export const Init = {
    listen: (): void => {
        Hooks.once("init", () => {
            console.log("SF2e System | Initializing Pathfinder 2nd Edition System");

            CONFIG.SF2E = SF2ECONFIG;
            CONFIG.debug.ruleElement ??= false;

            // Assign document and Canvas classes
            CONFIG.Item.documentClass = ItemSF2e;
            CONFIG.ActiveEffect.documentClass = ActiveEffectSF2e;
            CONFIG.Actor.documentClass = ActorSF2e;
            CONFIG.ChatMessage.documentClass = ChatMessageSF2e;
            CONFIG.Combat.documentClass = CombatSF2e;
            CONFIG.Combatant.documentClass = CombatantSF2e;
            CONFIG.FogExploration.documentClass = FogExplorationSF2e;
            CONFIG.Folder.documentClass = FolderSF2e;
            CONFIG.JournalEntry.sheetClass = JournalSheetSF2e;
            CONFIG.Macro.documentClass = MacroSF2e;

            CONFIG.Scene.documentClass = SceneSF2e;
            CONFIG.Scene.sheetClass = SceneConfigSF2e;

            CONFIG.User.documentClass = UserSF2e;

            CONFIG.AmbientLight.documentClass = AmbientLightDocumentSF2e;
            CONFIG.AmbientLight.layerClass = LightingLayerSF2e;
            CONFIG.AmbientLight.objectClass = AmbientLightSF2e;

            CONFIG.MeasuredTemplate.documentClass = MeasuredTemplateDocumentSF2e;
            CONFIG.MeasuredTemplate.objectClass = MeasuredTemplateSF2e;

            CONFIG.Tile.documentClass = TileDocumentSF2e;

            CONFIG.Token.documentClass = TokenDocumentSF2e;
            CONFIG.Token.objectClass = TokenSF2e;
            CONFIG.Token.sheetClass = TokenConfigSF2e;

            CONFIG.Canvas.layers.darkvision = DarkvisionLayerSF2e;
            CONFIG.Canvas.layers.lighting = LightingLayerSF2e;
            CONFIG.Canvas.layers.sight = SightLayerSF2e;
            CONFIG.Canvas.layers.templates = TemplateLayerSF2e;

            // Make darkness visibility a little more appropriate for basic map use
            CONFIG.Canvas.lightLevels.dim = 0.25;
            CONFIG.Canvas.darknessColor = PIXI.utils.rgb2hex([0.25, 0.25, 0.4]);
            CONFIG.Canvas.exploredColor = PIXI.utils.rgb2hex([0.6, 0.6, 0.6]);

            // Automatically advance world time by 6 seconds each round
            CONFIG.time.roundTime = 6;
            // Allowing a decimal on the Combat Tracker so the GM can set the order if players roll the same initiative.
            CONFIG.Combat.initiative.decimals = 1;

            // Assign the SF2e Sidebar subclasses
            CONFIG.ui.combat = EncounterTrackerSF2e;
            CONFIG.ui.chat = ChatLogSF2e;
            CONFIG.ui.compendium = CompendiumDirectorySF2e;

            // configure the bundled TinyMCE editor with PF2-specific options
            CONFIG.TinyMCE.extended_valid_elements = "pf2-action[action|glyph]";
            CONFIG.TinyMCE.content_css = CONFIG.TinyMCE.content_css.concat("systems/sf2e/styles/main.css");
            CONFIG.TinyMCE.style_formats = (CONFIG.TinyMCE.style_formats ?? []).concat({
                title: "SF2E",
                items: [
                    {
                        title: "Icons A D T F R",
                        inline: "span",
                        classes: ["pf2-icon"],
                        wrapper: true,
                    },
                    {
                        title: "Inline Header",
                        block: "h4",
                        classes: "inline-header",
                    },
                    {
                        title: "Info Block",
                        block: "section",
                        classes: "info",
                        wrapper: true,
                        exact: true,
                        merge_siblings: false,
                    },
                    {
                        title: "Stat Block",
                        block: "section",
                        classes: "statblock",
                        wrapper: true,
                        exact: true,
                        merge_siblings: false,
                    },
                    {
                        title: "Trait",
                        block: "section",
                        classes: "traits",
                        wrapper: true,
                    },
                    {
                        title: "Written Note",
                        block: "p",
                        classes: "message",
                    },
                ],
            });

            PlayerConfigSF2e.hookOnRenderSettings();
            MystifiedTraits.compile();

            registerSettings();
            loadSF2ETemplates();
            registerHandlebarsHelpers();
        });
    },
};
