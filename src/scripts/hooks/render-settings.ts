import { MigrationSummary } from "@module/apps/migration-summary";
import { fontAwesomeIcon } from "@module/utils";

export const RenderSettings = {
    listen: (): void => {
        Hooks.on("renderSettings", async (_app, $html) => {
            // Guide and Changelog
            const $systemRow = $html.find("#game-details > li.system");
            const $guideLog = $systemRow.clone().empty().removeClass("system").addClass("system-info");
            const links = {
                guide: {
                    url: "https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/wikis/Getting-Started-Guide/Getting-Started",
                    label: game.i18n.localize("SF2E.SETTINGS.Sidebar.Guide"),
                },
                changelog: {
                    url: "https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/blob/release-0.8.x/CHANGELOG.md",
                    label: game.i18n.localize("SF2E.SETTINGS.Sidebar.Changelog"),
                },
                discord: {
                    url: "https://discord.gg/SajryVzCyf",
                    label: game.i18n.localize("SF2E.SETTINGS.Sidebar.Discord"),
                },
            };
            $guideLog.append(
                `<a href="${links.guide.url}">${links.guide.label}</a>`,
                `<a href="${links.changelog.url}">${links.changelog.label}</a>`,
                `<a href="${links.discord.url}">${links.discord.label}</a>`
            );
            $systemRow.after($guideLog);

            // Paizo License and Migration Troubleshooting
            const $license = $("<div>").attr({ id: "sf2e-license" });
            const $licenseButton = $('<button type="button">')
                .append(fontAwesomeIcon("balance-scale"), game.i18n.localize("SF2E.LicenseViewer.Label"))
                .on("click", () => game.sf2e.licenseViewer.render(true));
            $license.append($licenseButton);

            if (game.user.hasRole("GAMEMASTER")) {
                const $troubleshooting = $("<div>").attr({ id: "sf2e-troubleshooting" });
                const $shootButton = $('<button type="button">')
                    .append(fontAwesomeIcon("wrench"), game.i18n.localize("SF2E.Migrations.Troubleshooting"))
                    .on("click", () => new MigrationSummary({ troubleshoot: true }).render(true));
                $troubleshooting.append($shootButton);

                $("#settings-documentation").after("<h2>Pathfinder 2e</h2>", $license, $troubleshooting);
            }
        });
    },
};
