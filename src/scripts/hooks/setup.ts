import { LocalizeSF2e } from "@system/localize";
import { registerSheets } from "../register-sheets";
import { calculateXP } from "@scripts/macros/xp";
import { launchTravelSheet } from "@scripts/macros/travel/travel-speed-sheet";
import { rollActionMacro, rollItemMacro } from "@scripts/macros/hotbar";
import { raiseAShield } from "@scripts/macros/raise-a-shield";
import { restForTheNight } from "@scripts/macros/rest-for-the-night";
import { steelYourResolve } from "@scripts/macros/steel-your-resolve";
import { encouragingWords } from "@scripts/macros/encouraging-words";
import { earnIncome } from "@scripts/macros/earn-income";
import { DiceSF2e } from "@scripts/dice";
import {
    AbilityModifier,
    CheckModifier,
    ModifierSF2e,
    MODIFIER_TYPE,
    ProficiencyModifier,
    StatisticModifier,
} from "@module/modifiers";
import { CheckSF2e } from "@system/rolls";
import { RuleElementSF2e, RuleElements } from "@module/rules/rules";
import { ConditionManager } from "@system/conditions";
import { StatusEffects } from "@scripts/actor/status-effects";
import { EffectsPanel } from "@system/effect-panel";
import { EffectTracker } from "@system/effect-tracker";
import { remigrate } from "@scripts/system/remigrate";
import { ActorImporter } from "@system/importer/actor-importer";
import { HomebrewElements } from "@module/settings/homebrew";

/**
 * This runs after game data has been requested and loaded from the servers, so entities exist
 */
export function listen() {
    Hooks.once("setup", () => {
        LocalizeSF2e.ready = true;

        // Register actor and item sheets
        registerSheets();

        // Exposed objects for macros and modules
        Object.defineProperty(globalThis.game, "sf2e", { value: {} });
        game.sf2e.actions = {
            earnIncome,
            raiseAShield,
            restForTheNight,
            steelYourResolve,
            encouragingWords,
        };
        game.sf2e.importer = {
            actor: ActorImporter,
        };
        game.sf2e.rollItemMacro = rollItemMacro;
        game.sf2e.rollActionMacro = rollActionMacro;
        game.sf2e.gm = {
            calculateXP,
            launchTravelSheet,
        };
        game.sf2e.system = {
            remigrate,
        };
        game.sf2e.Dice = DiceSF2e;
        game.sf2e.StatusEffects = StatusEffects;
        game.sf2e.ConditionManager = ConditionManager;
        game.sf2e.ModifierType = MODIFIER_TYPE;
        game.sf2e.Modifier = ModifierSF2e;
        game.sf2e.AbilityModifier = AbilityModifier;
        game.sf2e.ProficiencyModifier = ProficiencyModifier;
        game.sf2e.StatisticModifier = StatisticModifier;
        game.sf2e.CheckModifier = CheckModifier;
        game.sf2e.Check = CheckSF2e;
        game.sf2e.RuleElements = RuleElements;
        game.sf2e.RuleElement = RuleElementSF2e;

        // Start system sub-applications
        game.sf2e.effectPanel = new EffectsPanel();
        game.sf2e.effectTracker = new EffectTracker();

        // Assign the homebrew elements to their respective `CONFIG.SF2E` objects
        HomebrewElements.refreshTags();
    });
}
