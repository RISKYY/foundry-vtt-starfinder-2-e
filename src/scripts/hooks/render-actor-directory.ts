export function listen() {
    Hooks.on("renderActorDirectory", async () => {
        if (game.ready) {
            game.sf2e.compendiumBrowser.injectActorDirectory();
        }
    });
}
