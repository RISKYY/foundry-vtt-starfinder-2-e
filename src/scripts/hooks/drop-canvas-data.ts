import { ActorSheetSF2e } from "@actor/sheet/base";
import { CanvasSF2e } from "@module/canvas";
import { DropCanvasDataSF2e, DropCanvasItemDataSF2e } from "@module/canvas/drop-canvas-data";

export function listen(): void {
    Hooks.on("dropCanvasData", async (canvas: CanvasSF2e, data: DropCanvasDataSF2e) => {
        const target = canvas.tokens.placeables.find((token) => {
            const maximumX = token.x + token.hitArea?.right ?? 0;
            const maximumY = token.y + token.hitArea?.bottom ?? 0;
            return data.x >= token.x && data.y >= token.y && data.x <= maximumX && data.y <= maximumY;
        });

        const actor = target?.actor;
        if (actor && data.type === "Item") {
            if (["character", "npc", "loot", "vehicle"].includes(actor.type) && actor.sheet instanceof ActorSheetSF2e) {
                await actor.sheet.onDropItem(data as DropCanvasItemDataSF2e);
                return true;
            }
            return false;
        }
        return true;
    });
}
