import { CharacterSheetSF2e } from "@actor/character/sheet";
import { NPCLegacySheetSF2e } from "@actor/npc/legacy-sheet";
import { ActionSheetSF2e } from "@item/action/sheet";
import { HazardSheetSF2e } from "@actor/hazard/sheet";
import { LootSheetSF2e } from "@actor/loot/sheet";
import { FamiliarSheetSF2e } from "@actor/familiar/sheet";
import { VehicleSheetSF2e } from "@actor/vehicle/sheet";
import { NPCSheetSF2e } from "@actor/npc/sheet";
import { ItemSheetSF2e } from "@item/sheet/base";
import { KitSheetSF2e } from "@item/kit/sheet";
import { AncestrySheetSF2e } from "@item/ancestry/sheet";
import { BackgroundSheetSF2e } from "@item/sheet/background";
import { ClassSheetSF2e } from "@item/class/sheet";
import { SpellSheetSF2e } from "@item/spell/sheet";
import { LocalizeSF2e } from "@system/localize";
import { PhysicalItemSheetSF2e } from "@item/physical/sheet";
import { ActorSheetSF2eDataEntryNPC } from "@actor/npc/data-entry-sheet";
import { FeatSheetSF2e } from "@item/feat/sheet";
import { PHYSICAL_ITEM_TYPES } from "@item/data/values";
import { WeaponSheetSF2e } from "@item/weapon/sheet";
import { FormulaSheetSF2e } from "@item/formula/sheet";
import { EffectSheetSF2e } from "@item/effect/sheet";

export function registerSheets() {
    const translations = LocalizeSF2e.translations.SF2E;
    const sheetLabel = translations.SheetLabel;
    const sheetLabelOld = translations.SheetLabelOld;
    const sheetLabelDataEntry = translations.SheetLabelDataEntry;

    // ACTORS
    Actors.unregisterSheet("core", ActorSheet);

    const localizeType = (type: string) => {
        const entityType = type in CONFIG.SF2E.Actor.documentClasses ? "ACTOR" : "ITEM";
        const camelized = type[0].toUpperCase() + type.slice(1).toLowerCase();
        return game.i18n.localize(`${entityType}.Type${camelized}`);
    };

    Actors.registerSheet("sf2e", CharacterSheetSF2e, {
        types: ["character"],
        label: game.i18n.format(sheetLabel, { type: localizeType("character") }),
        makeDefault: true,
    });

    // Register NPC Sheet
    Actors.registerSheet("sf2e", NPCLegacySheetSF2e, {
        types: ["npc"],
        label: game.i18n.format(sheetLabelOld, { type: localizeType("npc") }),
        makeDefault: false,
    });

    // Regiser NEW NPC Sheet (don't make it default, it's on testing phase)
    Actors.registerSheet("sf2e", NPCSheetSF2e, {
        types: ["npc"],
        label: game.i18n.format(sheetLabel, { type: localizeType("npc") }),
        makeDefault: true,
    });

    if (BUILD_MODE === "development") {
        Actors.registerSheet("sf2e", ActorSheetSF2eDataEntryNPC, {
            types: ["npc"],
            label: game.i18n.format(sheetLabelDataEntry, { type: localizeType("npc") }),
            makeDefault: false,
        });
    }

    // Register Hazard Sheet
    Actors.registerSheet("sf2e", HazardSheetSF2e, {
        types: ["hazard"],
        label: game.i18n.format(sheetLabel, { type: localizeType("hazard") }),
        makeDefault: true,
    });

    // Register Loot Sheet
    Actors.registerSheet("sf2e", LootSheetSF2e, {
        types: ["loot"],
        label: game.i18n.format(sheetLabel, { type: localizeType("loot") }),
        makeDefault: true,
    });

    // Register Familiar Sheet
    Actors.registerSheet("sf2e", FamiliarSheetSF2e, {
        types: ["familiar"],
        label: game.i18n.format(sheetLabel, { type: localizeType("familiar") }),
        makeDefault: true,
    });

    // Register Vehicle Sheet
    Actors.registerSheet("sf2e", VehicleSheetSF2e, {
        types: ["vehicle"],
        label: game.i18n.format(sheetLabel, { type: localizeType("vehicle") }),
        makeDefault: true,
    });

    // ITEMS
    Items.unregisterSheet("core", ItemSheet);

    const itemTypes = ["condition", "lore", "martial", "melee", "spellcastingEntry"];
    for (const itemType of itemTypes) {
        Items.registerSheet("sf2e", ItemSheetSF2e, {
            types: [itemType],
            label: game.i18n.format(sheetLabel, { type: localizeType(itemType) }),
            makeDefault: true,
        });
    }

    for (const itemType of PHYSICAL_ITEM_TYPES) {
        if (itemType === "weapon") continue;
        Items.registerSheet("sf2e", PhysicalItemSheetSF2e, {
            types: [itemType],
            label: game.i18n.format(sheetLabel, { type: localizeType(itemType) }),
            makeDefault: true,
        });
    }
    Items.registerSheet("sf2e", WeaponSheetSF2e, {
        types: ["weapon"],
        label: game.i18n.format(sheetLabel, { type: localizeType("weapon") }),
        makeDefault: true,
    });

    const sheetEntries = [
        ["action", ActionSheetSF2e],
        ["ancestry", AncestrySheetSF2e],
        ["background", BackgroundSheetSF2e],
        ["class", ClassSheetSF2e],
        ["feat", FeatSheetSF2e],
        ["effect", EffectSheetSF2e],
        ["spell", SpellSheetSF2e],
        ["kit", KitSheetSF2e],
        ["formula", FormulaSheetSF2e],
    ] as const;
    for (const [type, Sheet] of sheetEntries) {
        Items.registerSheet("sf2e", Sheet, {
            types: [type],
            label: game.i18n.format(sheetLabel, { type: localizeType(type) }),
            makeDefault: true,
        });
    }
}
