import { ActorSF2e } from "@actor/base";
import { ItemSF2e } from "@item/base";
import { ActiveEffectSF2e } from "@module/active-effect";
import { ChatLogSF2e, CompendiumDirectorySF2e, EncounterTrackerSF2e } from "@module/apps/ui";
import { ChatMessageSF2e } from "@module/chat-message";
import { MacroSF2e } from "@module/macro";
import { RuleElementSF2e, RuleElements } from "@module/rules/rules";
import type { HomebrewSettingsKey, HomebrewTag } from "@module/settings/homebrew";
import { StatusEffects } from "@scripts/actor/status-effects";
import { SF2ECONFIG, StatusEffectIconType } from "@scripts/config";
import { DiceSF2e } from "@scripts/dice";
import { rollActionMacro, rollItemMacro } from "@scripts/macros/hotbar";
import { launchTravelSheet } from "@scripts/macros/travel/travel-speed-sheet";
import { calculateXP } from "@scripts/macros/xp";
import { EffectsPanel } from "@system/effect-panel";
import { EffectTracker } from "@system/effect-tracker";
import { CheckSF2e } from "@system/rolls";
import { WorldClock } from "@system/world-clock";
import { CombatSF2e } from "./module/combat";
import { ConditionManager } from "./module/system/conditions";
import {
    AbilityModifier,
    CheckModifier,
    ModifierSF2e,
    MODIFIER_TYPE,
    ProficiencyModifier,
    StatisticModifier,
} from "./module/modifiers";
import { UserSF2e } from "@module/user";
import {
    AmbientLightDocumentSF2e,
    MeasuredTemplateDocumentSF2e,
    SceneSF2e,
    TileDocumentSF2e,
    TokenDocumentSF2e,
} from "@module/scene";
import { CompendiumBrowser } from "@module/apps/compendium-browser";
import { LicenseViewer } from "@module/apps/license-viewer";
import { remigrate } from "@scripts/system/remigrate";
import { FolderSF2e } from "@module/folder";
import { CanvasSF2e, DarkvisionLayerSF2e } from "@module/canvas";
import { FogExplorationSF2e } from "@module/fog-exploration";
import { ActorImporter } from "@system/importer/actor-importer";

declare global {
    interface Game {
        sf2e: {
            actions: Record<string, Function>;
            compendiumBrowser: CompendiumBrowser;
            licenseViewer: LicenseViewer;
            worldClock: WorldClock;
            effectPanel: EffectsPanel;
            effectTracker: EffectTracker;
            rollActionMacro: typeof rollActionMacro;
            rollItemMacro: typeof rollItemMacro;
            gm: {
                calculateXP: typeof calculateXP;
                launchTravelSheet: typeof launchTravelSheet;
            };
            system: {
                remigrate: typeof remigrate;
            };
            importer: {
                actor: typeof ActorImporter;
            };
            Dice: typeof DiceSF2e;
            StatusEffects: typeof StatusEffects;
            ConditionManager: typeof ConditionManager;
            ModifierType: typeof MODIFIER_TYPE;
            Modifier: typeof ModifierSF2e;
            AbilityModifier: typeof AbilityModifier;
            ProficiencyModifier: typeof ProficiencyModifier;
            StatisticModifier: typeof StatisticModifier;
            CheckModifier: typeof CheckModifier;
            Check: typeof CheckSF2e;
            RuleElements: typeof RuleElements;
            RuleElement: typeof RuleElementSF2e;
        };
    }

    interface ConfigSF2e extends ConfiguredConfig {
        debug: ConfiguredConfig["debug"] & {
            ruleElement: boolean;
        };
        SF2E: typeof SF2ECONFIG;
        time: {
            roundTime: number;
        };
        Canvas: ConfiguredConfig["Canvas"] & {
            layers: ConfiguredConfig["Canvas"]["layers"] & {
                darkvision: typeof DarkvisionLayerSF2e;
            };
        };
    }

    const CONFIG: ConfigSF2e;
    const canvas: CanvasSF2e;
    namespace globalThis {
        // eslint-disable-next-line no-var
        var game: Game<ActorSF2e, ChatMessageSF2e, CombatSF2e, FolderSF2e, ItemSF2e, MacroSF2e, SceneSF2e, UserSF2e>;
    }

    interface ClientSettings {
        get(module: "sf2e", setting: "automation.rulesBasedVision"): boolean;
        get(module: "sf2e", setting: "automation.effectExpiration"): boolean;
        get(module: "sf2e", setting: "automation.lootableNPCs"): boolean;

        get(module: "sf2e", setting: "ancestryParagonVariant"): boolean;
        get(module: "sf2e", setting: "freeArchetypeVariant"): boolean;
        get(module: "sf2e", setting: "staminaVariant"): 0 | 1;

        get(module: "sf2e", setting: "metagame.partyVision"): boolean;
        get(module: "sf2e", setting: "metagame.showResults"): "none" | "gm " | "owner" | "all";
        get(module: "sf2e", setting: "metagame.showDC"): "none" | "gm " | "owner" | "all";

        get(module: "sf2e", setting: "worldClock.dateTheme"): "AR" | "IC" | "AD" | "CE";
        get(module: "sf2e", setting: "worldClock.syncDarkness"): boolean;
        get(module: "sf2e", setting: "worldClock.timeConvention"): 24 | 12;
        get(module: "sf2e", setting: "worldClock.worldCreatedOn"): string;

        get(module: "sf2e", setting: "homebrew.weaponCategories"): HomebrewTag<"weaponCategories">[];
        get(module: "sf2e", setting: HomebrewSettingsKey): HomebrewTag[];

        get(module: "sf2e", setting: "proficiencyVariant"): "ProficiencyWithLevel" | "ProficiencyWithoutLevel";

        get(module: "sf2e", setting: "defaultTokenSettings"): boolean;
        get(module: "sf2e", setting: "defaultTokenSettingsBar"): number;
        get(module: "sf2e", setting: "defaultTokenSettingsName"): string;
        get(module: "sf2e", setting: "enabledRulesUI"): boolean;
        get(module: "sf2e", setting: "ignoreCoinBulk"): boolean;
        get(module: "sf2e", setting: "pfsSheetTab"): boolean;
        get(module: "sf2e", setting: "statusEffectType"): StatusEffectIconType;
        get(module: "sf2e", setting: "worldSchemaVersion"): number;
        get(module: "sf2e", setting: "drawCritFumble"): boolean;
        get(module: "sf2e", setting: "critFumbleButtons"): boolean;
        get(module: "sf2e", setting: "journalEntryTheme"): "sf2eTheme" | "foundry";
        get(module: "sf2e", setting: "identifyMagicNotMatchingTraditionModifier"): 0 | 2 | 5 | 10;
    }

    interface WorldSettingsStorage {
        get(setting: "sf2e.worldSchemaVersion"): string | undefined;
        getItem(setting: "sf2e.worldSchemaVersion"): string | null;
    }

    const BUILD_MODE: "development" | "production";
}

type ConfiguredConfig = Config<
    AmbientLightDocumentSF2e,
    ActiveEffectSF2e,
    ActorSF2e,
    ChatLogSF2e,
    ChatMessageSF2e,
    CombatSF2e,
    EncounterTrackerSF2e,
    CompendiumDirectorySF2e,
    FogExplorationSF2e,
    FolderSF2e,
    ItemSF2e,
    MacroSF2e,
    MeasuredTemplateDocumentSF2e,
    TileDocumentSF2e,
    TokenDocumentSF2e,
    SceneSF2e,
    UserSF2e
>;
