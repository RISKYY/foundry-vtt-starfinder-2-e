{
    "_id": "NXdpFypPPmRwYBT1",
    "data": {
        "abilities": {
            "cha": {
                "mod": 2
            },
            "con": {
                "mod": 0
            },
            "dex": {
                "mod": 6
            },
            "int": {
                "mod": 2
            },
            "str": {
                "mod": -5
            },
            "wis": {
                "mod": 4
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 27
            },
            "allSaves": {
                "value": ""
            },
            "hp": {
                "details": "",
                "max": 50,
                "temp": 0,
                "value": 50
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 16
            },
            "shield": {
                "ac": 0,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [
                    {
                        "label": "Fly",
                        "type": "fly",
                        "value": "50"
                    }
                ],
                "value": "0"
            }
        },
        "details": {
            "alignment": {
                "value": "CE"
            },
            "creatureType": "Aberration",
            "flavorText": "<p>Malevolent balls of colored light, will-o'-wisps haunt lonely marshes and forests where they lure unsuspecting travelers into danger. Will-o'-wisps can vary the color and illumination they shed, and delight in mimicking bobbing lanterns or distant fires to draw lost or disoriented travelers off of safe trails. They can extinguish their illumination entirely to become invisible, and they enjoy doing so once their victims are wholly lost and have realized that the bobbing light in the distance isn't, in fact, leading them to safety. Even invisible, however, a will-o'-wisp rarely ventures far from its target, as it feasts upon the panic and dread felt by its victims.</p>\n<p>Beneath its glow, a will-o'-wisp's body is a spongy ball approximately 1 foot in diameter and weighing less than 5 pounds. Although most will-o'-wisps are merely translucent, featureless orbs, gaining definition only in the shifting illumination they create, a few have dark mottling that makes them resemble a skull when viewed closely. Will-o'-wisps have no need for mundane nourishment, and in fact lack the ability to consume matter of any kind; they find all the sustenance they need in the terror of nearby creatures. For this reason, they like to work alongside undead that produce terror in their victims. Will-o'-wisps are long-lived, if not effectively immortal, and they have good memories. A cowed or defeated will-o'-wisp can be a good source of lore and information, though acquiring such cooperation from such a sinister monster is no easy feat.</p>\n<p>Will-o'-wisps inhabit desolate swamps and forests and are generally active at twilight and after dark. They are therefore reluctant to lead victims into immediately fatal areas such as deadfalls, but instead prefer hazards where their victims suffer over a long time, such as pockets of stale or poisonous air, patches of quicksand, and dens of bigger monsters. According to will-o'-wisps, different types of fear have subtle differences in flavor. The lurking dread in the pit of the stomach that gnaws at those who slowly become aware of the fact that they're lost produces a much different taste than the sudden stark terror of imminent death in the face of a towering monster. Because of this, will-o'-wisps try to vary the ways in which they induce terror in their prey, to ensure they don't tire of certain flavors of fear.</p>\n<hr />\n<p>Feeding not on fear but on the ephemeral potentiality of magic lodged in the minds of their victims, these will-o'-wisps are a bane to casters of all types. They usually leave non-spellcasters alone and sometimes follow adventuring groups without spellcasters on the hope of being led to creatures they find more appetizing.</p>",
            "level": {
                "value": 6
            },
            "source": {
                "value": "Pathfinder #164: Hands of the Devil"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 10
            },
            "reflex": {
                "saveDetail": "",
                "value": 16
            },
            "will": {
                "saveDetail": "",
                "value": 14
            }
        },
        "traits": {
            "attitude": {
                "value": "hostile"
            },
            "ci": [],
            "di": {
                "custom": "",
                "value": [
                    "magic"
                ]
            },
            "dr": [],
            "dv": [],
            "languages": {
                "custom": "",
                "selected": [],
                "value": [
                    "aklo",
                    "common"
                ]
            },
            "rarity": {
                "value": "common"
            },
            "senses": {
                "value": "darkvision"
            },
            "size": {
                "value": "sm"
            },
            "traits": {
                "custom": "",
                "value": [
                    "aberration",
                    "air"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/sf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "2rAKighB6Ywhk5s3",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "custom": "",
                    "value": []
                },
                "bonus": {
                    "value": 17
                },
                "damageRolls": {
                    "jtguda2tjozvfz78bzs3": {
                        "damage": "2d8+4",
                        "damageType": "electricity"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "electricity",
                        "magical"
                    ]
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/melee.svg",
            "name": "Shock",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "51r9xhJpaBdZxkK3",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A monster with darkvision can see perfectly well in areas of darkness and dim light, though such vision is in black and white only. Some forms of magical darkness, such as a 4th-level @Compendium[sf2e.spells-srd.Darkness]{Darkness} spell, block normal darkvision. A monster with @Compendium[sf2e.bestiary-ability-glossary-srd.Greater Darkvision]{Greater Darkvision}, however, can see through even these forms of magical darkness.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "darkvision",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.sf2e.bestiary-ability-glossary-srd.qCCLZhnp2HhP3Ex6"
                }
            },
            "img": "systems/sf2e/icons/default-icons/action.svg",
            "name": "Darkvision",
            "sort": 200000,
            "type": "action"
        },
        {
            "_id": "wChn2PEw9GYziq2n",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>20 feet. @Compendium[sf2e.bestiary-ability-glossary-srd.Aura]{Aura}</p>\n<p>A will-o'-wisp is itself naturally invisible, but glows with a colored light, casting bright light in the aura and making it visible.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "aura",
                        "light"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/actions/Passive.webp",
            "name": "Glow",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "3Eksrmh390T4SrP7",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A will-o'-wisp is immune to all spells except @Compendium[sf2e.spells-srd.Faerie Fire]{Faerie Fire}, @Compendium[sf2e.spells-srd.Glitterdust]{Glitterdust}, @Compendium[sf2e.spells-srd.Magic Missile]{Magic Missile}, and @Compendium[sf2e.spells-srd.Maze]{Maze}.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/actions/Passive.webp",
            "name": "Magic Immunity",
            "sort": 400000,
            "type": "action"
        },
        {
            "_id": "F8xpT7h5a5ZviXU5",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 1
                },
                "description": {
                    "value": "<p><strong>Requirement</strong> A creature who can cast spells is within 15 feet of the spellvoid;</p>\n<p><strong>Effect</strong> The spellvoid feeds on the creature's magic. The spellvoid regains [[/r 2d8 #Hit Points]]{2d8 Hit Points}, and the target must attempt a <span data-pf2-check=\"will\" data-pf2-traits=\"concentrate\" data-pf2-label=\"Feed on Magic DC\" data-pf2-dc=\"24\" data-pf2-show-dc=\"gm\">Will</span> save.</p>\n<p><em data-visibility=\"gm\">The target is then temporarily immune for 1 hour.</em></p>\n<hr />\n<p><em data-visibility=\"gm\"><strong>Critical Success</strong> The target is unaffected.</em></p>\n<p><em data-visibility=\"gm\"><strong>Success</strong> The first time the target Casts a Spell before the start of the spellvoid's next turn, the spell is disrupted unless the target succeeds at a <span data-pf2-check=\"flat\" data-pf2-dc=\"15\" data-pf2-label=\"Feed on Magic DC\" data-pf2-show-dc=\"gm\">flat check</span>.</em></p>\n<p><em data-visibility=\"gm\"><strong>Failure</strong> As success, but the effect applies the first time the target Casts a Spell within the next minute.</em></p>\n<p><em data-visibility=\"gm\"><strong>Critical Failure</strong> As success, but the effect applies each time the target Casts a Spell within the next minute. In addition, the spellvoid doubles the Hit Points it regains.</em></p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "concentrate"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/actions/OneAction.webp",
            "name": "Feed on Magic",
            "sort": 500000,
            "type": "action"
        },
        {
            "_id": "USCoVo5LJNs12zB4",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 1
                },
                "description": {
                    "value": "<p>The will-o'-wisp extinguishes its glow, becoming invisible. It can end this effect with another use of this action. If it uses its shock attack while invisible, the arc of electricity lets any observer determine its location, making the will-o'-wisp only hidden to all observers until it moves.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "concentrate"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/mystery-man.svg",
            "name": "Go Dark",
            "sort": 600000,
            "type": "action"
        },
        {
            "_id": "X2yn2MMfpvq1n5pe",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 18
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/mystery-man.svg",
            "name": "Acrobatics",
            "sort": 700000,
            "type": "lore"
        },
        {
            "_id": "smpDyRgoCURI6Otr",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 12
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/mystery-man.svg",
            "name": "Deception",
            "sort": 800000,
            "type": "lore"
        },
        {
            "_id": "o5ZowKCSH7katdBS",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 12
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/mystery-man.svg",
            "name": "Intimidation",
            "sort": 900000,
            "type": "lore"
        },
        {
            "_id": "dgC9LAHvEPLznmzF",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 16
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/mystery-man.svg",
            "name": "Stealth",
            "sort": 1000000,
            "type": "lore"
        }
    ],
    "name": "Spellvoid",
    "token": {
        "disposition": -1,
        "height": 1,
        "img": "systems/sf2e/icons/default-icons/npc.svg",
        "name": "Spellvoid",
        "width": 1
    },
    "type": "npc"
}
