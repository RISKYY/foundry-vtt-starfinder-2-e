{
    "_id": "KROWh0CteDFFGsih",
    "data": {
        "abilities": {
            "cha": {
                "mod": 1
            },
            "con": {
                "mod": 3
            },
            "dex": {
                "mod": 4
            },
            "int": {
                "mod": -1
            },
            "str": {
                "mod": -2
            },
            "wis": {
                "mod": 1
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 16
            },
            "allSaves": {
                "value": ""
            },
            "hp": {
                "details": "negative healing",
                "max": 20,
                "temp": 0,
                "value": 20
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 10
            },
            "shield": {
                "ac": 0,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [],
                "value": "fly 25 feet"
            }
        },
        "details": {
            "alignment": {
                "value": "LE"
            },
            "creatureType": "Undead",
            "flavorText": "<p>The life of a Hellknight is bloody, brutal, and often short. Many who perish in service to a Hellknight order are glad to rest after having served their masters so faithfully, but others seek to continue their work even in death. When a Hellknight is decapitated, the ghostly undead known as a hellcrown is an occasional result. Consumed by the desire to bring about order by inflicting cruelty, hellcrowns haunt battlefields and abandoned castles, slaying all they encounter. A strange fusion of spirit and steel, a hellcrown has no corporeal form by itself, but instead inhabits the helmet it so proudly wore in life. Dangling from the helmet like a shroud is a shadow of the former Hellknight's spine, adding to the creature's terrifyingly gruesome appearance. Hellknights regard hellcrowns with a mixture of disgust and respect, considering the individuals who transform into these floating undead to have been resolute in purpose but weak in body.</p>\n<p>A hellcrown usually manifests hours or longer after its body's death, and only when its body is unattended; this might even take days if the Hellknight perished in a major battle. Typically, no recognizable fragment of the Hellknight's former personality survives the grisly transformation into a hellcrown, but in rare and particularly tragic cases a hellcrown might remember its life and hold grudges against those it views as the cause of its death. Regardless of whether they retain memories of their lives or have lost all former sense of self, hellcrowns linger around the site of their death, reminding all they encounter of the merciless principles of their order.</p>",
            "level": {
                "value": 1
            },
            "source": {
                "value": "Pathfinder #145: Hellknight Hill"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 4
            },
            "reflex": {
                "saveDetail": "",
                "value": 6
            },
            "will": {
                "saveDetail": "",
                "value": 10
            }
        },
        "traits": {
            "attitude": {
                "value": "hostile"
            },
            "ci": [],
            "di": {
                "custom": "",
                "value": [
                    "death-effects",
                    "disease",
                    "paralyzed",
                    "poison",
                    "unconscious"
                ]
            },
            "dr": [],
            "dv": [],
            "languages": {
                "custom": "",
                "selected": [],
                "value": [
                    "common"
                ]
            },
            "rarity": {
                "value": "uncommon"
            },
            "senses": {
                "value": "darkvision"
            },
            "size": {
                "value": "tiny"
            },
            "traits": {
                "custom": "",
                "value": [
                    "undead"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/sf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "yh0jnKztqB0nQlYV",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "value": [
                        "bleeding-nail"
                    ]
                },
                "bonus": {
                    "value": 9
                },
                "damageRolls": {
                    "ek8jz1ab0p7zllg25pfj": {
                        "damage": "1d4+2",
                        "damageType": "piercing"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "range-increment-20"
                    ]
                },
                "weaponType": {
                    "value": "ranged"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/melee.svg",
            "name": "Nail",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "ZWaPe5FQtP4LDiEM",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>If the hellcrown hits with a nail Strike, the target must attempt a <span data-pf2-check=\"fortitude\" data-pf2-dc=\"16\" data-pf2-traits=\"damaging-effect\" data-pf2-label=\"Bleeding Nail DC\" data-pf2-show-dc=\"gm\">Fortitude</span> save. On a failure, the nail is embedded in the creature, making it @Compendium[sf2e.conditionitems.Enfeebled]{Enfeebled 1} for 1 minute and giving it [[/r 1 #persistent bleed]] @Compendium[sf2e.conditionitems.Persistent Damage]{Persistent Bleed Damage} (@Compendium[sf2e.conditionitems.Enfeebled]{Enfeebled 2} and [[/r 1d4 #persistent bleed]] @Compendium[sf2e.conditionitems.Persistent Damage]{Persistent Bleed Damage} on a critical failure). Each additional embedded nail increases the @Compendium[sf2e.conditionitems.Enfeebled]{Enfeebled} value by 1 (to a maximum of enfeebled 4) and the bleed damage by 1. A creature can remove a nail with an Interact action to reduce the enfeeblement and amount of bleed damage. Pulling out the last nail removes both conditions.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/actions/Passive.webp",
            "name": "Bleeding Nail",
            "sort": 200000,
            "type": "action"
        },
        {
            "_id": "OqoxUu62vH7mcrPs",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 1
                },
                "description": {
                    "value": "<p>All creatures that can see the hellcrown and are suffering from its bleeding nail must attempt a <span data-pf2-check=\"will\" data-pf2-dc=\"16\" data-pf2-traits=\"fear,mental,visual\" data-pf2-label=\"Terrifying Stare DC\" data-pf2-show-dc=\"gm\">Will</span> saving throw. A creature that fails is @Compendium[sf2e.conditionitems.Frightened]{Frightened 1}, and on a critical failure becomes fleeing for as long as it's frightened. Any creature that attempts a save is then temporarily immune for 10 minutes.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "fear",
                        "mental",
                        "visual"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/actions/OneAction.webp",
            "name": "Terrifying Stare",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "1TBTjsMnNkNnPbpt",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 8
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/lore.svg",
            "name": "Intimidation",
            "sort": 400000,
            "type": "lore"
        },
        {
            "_id": "Js7neEYKwlo4WOvM",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 7
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/lore.svg",
            "name": "Stealth",
            "sort": 500000,
            "type": "lore"
        }
    ],
    "name": "Hellcrown",
    "token": {
        "disposition": -1,
        "height": 0.5,
        "img": "systems/sf2e/icons/default-icons/npc.svg",
        "name": "Hellcrown",
        "width": 0.5
    },
    "type": "npc"
}
