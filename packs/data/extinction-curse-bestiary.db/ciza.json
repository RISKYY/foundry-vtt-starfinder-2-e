{
    "_id": "s7447jKTaAd4r3Oa",
    "data": {
        "abilities": {
            "cha": {
                "mod": 7
            },
            "con": {
                "mod": 2
            },
            "dex": {
                "mod": 6
            },
            "int": {
                "mod": 0
            },
            "str": {
                "mod": -5
            },
            "wis": {
                "mod": 7
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 39
            },
            "allSaves": {
                "value": ""
            },
            "hp": {
                "details": "",
                "max": 250,
                "temp": 0,
                "value": 250
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 32
            },
            "shield": {
                "ac": 0,
                "brokenThreshold": "0",
                "hardness": 0,
                "max": "0",
                "value": 0
            },
            "speed": {
                "otherSpeeds": [],
                "value": "fly 60 feet"
            }
        },
        "details": {
            "alignment": {
                "value": "CE"
            },
            "creatureType": "Spirit",
            "flavorText": "<p>Banshees are the furious, tormented souls of elves bound to the Material Plane by a betrayal that defined the final hours of their lives. Some banshees arise from elves who were slain by trusted friends and allies, or whose loved ones betrayed them on their deathbeds. Others spawn from elves whose treacherous deeds shortly before their deaths left a stain upon their souls. Regardless of their origin, banshees despise the living. This hatred of life is all too often a horrific inversion of their personalities in life. Some speculate that the more kind-hearted the elf (and the more wrenching the betrayal), the crueler the banshee.</p>\n<p>Banshees rarely stray far from where they perished and typically haunt thick forests and canopied swamps where little light graces the ground. Many banshees can be found in the elven nation of Kyonin, specifically in Tanglebriar, the sinister domain of the demon Treerazer. Similarly, a large number of banshees can be found lurking about the edges of drow settlements in the Darklands, as plenty of cruelty and betrayal exists in drow culture.</p>\n<p>Banshees' mere touch inflicts pain and primal fear, and those exposed to their wails of grief rarely survive the experience.</p>",
            "level": {
                "value": 17
            },
            "source": {
                "value": "Pathfinder Bestiary"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 25
            },
            "reflex": {
                "saveDetail": "",
                "value": 29
            },
            "will": {
                "saveDetail": "",
                "value": 32
            }
        },
        "traits": {
            "attitude": {
                "value": "hostile"
            },
            "ci": [],
            "di": {
                "custom": "",
                "value": [
                    "disease",
                    "paralyzed",
                    "poison",
                    "precision",
                    "unconscious"
                ]
            },
            "dr": [
                {
                    "exceptions": "force or ghost touch or positive; double resistance vs. non-magical",
                    "label": "All",
                    "type": "all",
                    "value": "12"
                }
            ],
            "dv": [],
            "languages": {
                "custom": "",
                "selected": [],
                "value": [
                    "elven",
                    "undercommon"
                ]
            },
            "rarity": {
                "value": "unique"
            },
            "senses": {
                "value": "hears heartbeats (imprecise) 60 feet, darkvision"
            },
            "size": {
                "value": "med"
            },
            "traits": {
                "custom": "",
                "value": [
                    "incorporeal",
                    "spirit",
                    "undead"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/sf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "860MOhDdvHc8KanP",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "value": [
                        "terrifying-touch"
                    ]
                },
                "bonus": {
                    "value": 32
                },
                "damageRolls": {
                    "3p4soxr7gkvvnnt2orea": {
                        "damage": "4d10+14",
                        "damageType": "negative"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "finesse",
                        "magical"
                    ]
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/melee.svg",
            "name": "Hand",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "74wDKknM5LCOcK73",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>The banshee can hear heartbeats within 60 feet (imprecise).</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/actions/Passive.webp",
            "name": "Hears Heartbeats",
            "sort": 200000,
            "type": "action"
        },
        {
            "_id": "rQT7KUvA4yzEm4PO",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>If in direct sunlight, the banshee is slowed 1 and can't use actions that have the attack trait.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/actions/Passive.webp",
            "name": "Sunlight Powerlessness",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "NKs8hoGb4IyfFWCw",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "reaction"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>; <strong>Trigger</strong> A foe critically hits the banshee, or the banshee critically fails their save against a foe's damaging effect. <strong>Effect</strong> The banshee lashes back at their tormentor, dealing [[/r 4d10+14 #mental]]{4d10+14 mental damage} (<span data-pf2-check=\"will\" data-pf2-dc=\"38\" data-pf2-traits=\"evocation,occult,damaging-effect\" data-pf2-label=\"Vengeful Spite DC\" data-pf2-show-dc=\"gm\">basic Will</span> save) and applying the effects of terrifying touch based on the results of the same Will save.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "evocation",
                        "occult"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/action.svg",
            "name": "Vengeful Spite",
            "sort": 400000,
            "type": "action"
        },
        {
            "_id": "q8ASqdiBd9SflYVU",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>When a banshee Strides at least 10 feet, they're concealed until the start of their next turn.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/actions/Passive.webp",
            "name": "Spectral Ripple",
            "sort": 500000,
            "type": "action"
        },
        {
            "_id": "ePRrl3MsWtcXXzD7",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A creature damaged by the banshee's touch that isn't already @Compendium[sf2e.conditionitems.Frightened]{Frightened} must attempt a <span data-pf2-check=\"will\" data-pf2-dc=\"38\" data-pf2-traits=\"emotion,enchantment,fear,occult\" data-pf2-label=\"Terrifying Touch DC\" data-pf2-show-dc=\"gm\">Will</span> save (DC 43 if the attack was a critical hit). If the creature fails its save, it's @Compendium[sf2e.conditionitems.Frightened]{Frightened 2}; on a critical failure, the creature also cowers with fear and is @Compendium[sf2e.conditionitems.Stunned]{Stunned 4}. If the creature is protected against fear by a spell or magic item, the banshee's touch first attempts to counteract the protection effect, with the effect of a 9th-level @Compendium[sf2e.spells-srd.Dispel Magic]{Dispel Magic} spell.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "emotion",
                        "enchantment",
                        "fear",
                        "occult"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/action.svg",
            "name": "Terrifying Touch",
            "sort": 600000,
            "type": "action"
        },
        {
            "_id": "IFPnmQpUJLW9TJa9",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 2
                },
                "description": {
                    "value": "<p>The banshee unleashes a soul-chilling @Compendium[sf2e.spells-srd.Wail of the Banshee]{Wail of the Banshee} (DC 38). This Wail overcomes @Compendium[sf2e.spells-srd.Silence]{Silence} and similar effects of 5th level or lower. The banshee can instead use Wail as a three-action activity to overcome such effects of up to 8th level.</p>\n<p>The banshee's Wail resonates for 1 round, and any creature that comes within the area during that time must attempt a save against the effect. A creature can't be affected more than once by the same Wail. The banshee can't Wail again for [[/br 1d4 #rounds]]{1d4 rounds}.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "auditory",
                        "concentrate",
                        "death",
                        "necromancy",
                        "occult"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/action.svg",
            "name": "Wail",
            "sort": 700000,
            "type": "action"
        },
        {
            "_id": "eUYCxHp6KbpZHY3f",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 31
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/lore.svg",
            "name": "Acrobatics",
            "sort": 800000,
            "type": "lore"
        },
        {
            "_id": "6pcKSyXrmSuUbpfx",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 32
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/lore.svg",
            "name": "Intimidation",
            "sort": 900000,
            "type": "lore"
        },
        {
            "_id": "zCKQUEHy6Qbu8CUo",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 25
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/lore.svg",
            "name": "Occultism",
            "sort": 1000000,
            "type": "lore"
        },
        {
            "_id": "ZK91O9llcaSM7QTJ",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 28
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/lore.svg",
            "name": "Performance",
            "sort": 1100000,
            "type": "lore"
        }
    ],
    "name": "Ciza",
    "token": {
        "disposition": -1,
        "height": 1,
        "img": "systems/sf2e/icons/default-icons/npc.svg",
        "name": "Ciza",
        "width": 1
    },
    "type": "npc"
}
