{
    "_id": "sNeIVL8w7NPFtpK2",
    "data": {
        "abilities": {
            "cha": {
                "mod": -3
            },
            "con": {
                "mod": 5
            },
            "dex": {
                "mod": 4
            },
            "int": {
                "mod": -5
            },
            "str": {
                "mod": 3
            },
            "wis": {
                "mod": 1
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 20
            },
            "allSaves": {
                "value": null
            },
            "hp": {
                "details": "",
                "max": 65,
                "temp": 0,
                "value": 65
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 10
            },
            "shield": {
                "ac": 2,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [
                    {
                        "exceptions": "",
                        "label": "Climb",
                        "type": "climb",
                        "value": "30"
                    }
                ],
                "value": "30"
            }
        },
        "details": {
            "alignment": {
                "value": "N"
            },
            "creatureType": "",
            "flavorText": "<p>This strange type of fungus colony takes its name from its initial purpose: the wizard who first developed it sought a means to keep her tower clean from residue both mundane and magical. Wizard sponge does just that, dutifully cleaning its vicinity by devouring dust, organic debris, and even the decaying bodies of fallen creatures. Its designer intentionally crafted the fungus to not merely resist but be healed by fire, intended as an additional safety feature, and most varieties have a convenient side effect of being edible once slain. Unfortunately, the fungus also developed an unexpected ability to magically mutate over time, which eventually led to its escape and independent propagation.</p>\n<p>Wizard sponge colonies of various sizes and shapes now thrive in dungeons, caverns, sewers, abandoned castles, crypts, and even the basements of some occupied dwellings. They often carry along bits of partially consumed material with them, such as clothing, limbs, and adventuring gear-sometimes even valuable treasures! An individual colony typically takes on distinctive properties based on what it's consumed in its environment; a variety living in a necromancer's crypt might acquire the undead's affinity for negative energy, for example.</p>\n<hr />\n<p>The wizard sponge gains weakness 5 to cold iron, increases its Speed and climb Speed to 30 feet, and develops a strangely capricious growth pattern despite being mindless, such as growing in the pockets of a creature's clothing before devouring it.</p>",
            "level": {
                "value": 5
            },
            "source": {
                "value": "Pathfinder Bestiary 3"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 14
            },
            "reflex": {
                "saveDetail": "",
                "value": 13
            },
            "will": {
                "saveDetail": "",
                "value": 8
            }
        },
        "traits": {
            "attitude": {
                "value": "hostile"
            },
            "ci": [],
            "di": {
                "custom": "",
                "value": [
                    "critical-hits",
                    "fire",
                    "mental",
                    "precision",
                    "visual"
                ]
            },
            "dr": [
                {
                    "exceptions": "",
                    "label": "Piercing",
                    "type": "piercing",
                    "value": 5
                },
                {
                    "exceptions": "",
                    "label": "Slashing",
                    "type": "slashing",
                    "value": 5
                }
            ],
            "dv": [
                {
                    "exceptions": "",
                    "label": "Area Damage",
                    "type": "area-damage",
                    "value": "5"
                },
                {
                    "exceptions": "",
                    "label": "Cold Iron",
                    "type": "coldiron",
                    "value": "5"
                },
                {
                    "exceptions": "",
                    "label": "Splash Damage",
                    "type": "splash-damage",
                    "value": "5"
                }
            ],
            "languages": {
                "custom": "",
                "selected": [],
                "value": []
            },
            "rarity": {
                "value": "common"
            },
            "senses": {
                "value": "no vision, tremorsense 60 feet"
            },
            "size": {
                "value": "lg"
            },
            "traits": {
                "custom": "",
                "value": [
                    "fungus",
                    "mindless",
                    "swarm"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/sf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "PuvbyIbJk14Uho2a",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>Tremorsense allows a monster to feel the vibrations through a solid surface caused by movement. It is an imprecise sense with a limited range (listed in the ability). Tremorsense functions only if the monster is on the same surface as the subject, and only if the subject is moving along (or burrowing through) the surface.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "tremorsense",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.sf2e.bestiary-ability-glossary-srd.j2wsK6IsW5yMW1jW"
                }
            },
            "img": "systems/sf2e/icons/default-icons/mystery-man.svg",
            "name": "Tremorsense 60 feet",
            "sort": 100000,
            "type": "action"
        },
        {
            "_id": "OazlOifRnQFSPc2g",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>Anytime a wizard sponge would take fire damage, it instead regains [[/r 1d8]] Hit Points (regardless of the amount of damage the fire effect would have caused).</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/actions/Passive.webp",
            "name": "Fire Healing",
            "sort": 200000,
            "type": "action"
        },
        {
            "_id": "fbcWIR3VTHaRmvLw",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 1
                },
                "description": {
                    "value": "<p>The wizard sponge slithers over each creature in its space, dealing [[/r 2d10 #acid]]{2d10 acid damage} (<span data-pf2-check=\"reflex\" data-pf2-dc=\"20\" data-pf2-traits=\"damaging-effect\" data-pf2-label=\"Swarming Slither DC\" data-pf2-show-dc=\"gm\">basic Reflex</span> save). A creature that critically fails is @Compendium[sf2e.conditionitems.Sickened]{Sickened 1}.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/actions/OneAction.webp",
            "name": "Swarming Slither",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "Hi0AFeSVBBw7vuPF",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>Wizard sponges can climb on ceilings and other inverted surfaces.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/actions/Passive.webp",
            "name": "Suction",
            "sort": 400000,
            "type": "action"
        },
        {
            "_id": "4XdLVNceTaTeWXxs",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>Wizard sponge acid damages only organic material—not metal, stone, or other inorganic substances.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/actions/Passive.webp",
            "name": "Weak Acid",
            "sort": 500000,
            "type": "action"
        },
        {
            "_id": "dO8YIdebcg6JU7kQ",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 12
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/lore.svg",
            "name": "Athletics",
            "sort": 600000,
            "type": "lore"
        },
        {
            "_id": "HofMxPk6h7wuflyA",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 13
                },
                "proficient": {
                    "value": 0
                },
                "rules": [
                    {
                        "key": "FlatModifier",
                        "label": "Amid Decaying Plant Matter",
                        "predicate": {
                            "all": [
                                "decaying-plants"
                            ]
                        },
                        "selector": "stealth",
                        "value": 2
                    },
                    {
                        "key": "FlatModifier",
                        "label": "Amid Fungus",
                        "predicate": {
                            "all": [
                                "terrain:fungus"
                            ]
                        },
                        "selector": "stealth",
                        "value": 2
                    }
                ],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "variants": {
                    "0": {
                        "label": "+15 amid decaying plant matter",
                        "options": "decaying-plants"
                    },
                    "1": {
                        "label": "or fungus",
                        "options": "terrain:fungus"
                    }
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/sf2e/icons/default-icons/lore.svg",
            "name": "Stealth",
            "sort": 700000,
            "type": "lore"
        }
    ],
    "name": "Wizard Sponge (Fey Domain)",
    "token": {
        "disposition": -1,
        "height": 2,
        "img": "systems/sf2e/icons/default-icons/npc.svg",
        "name": "Wizard Sponge (Fey Domain)",
        "width": 2
    },
    "type": "npc"
}
