import { ActorSF2e } from "@actor/base";
import { SceneSF2e } from "@module/scene";
import { TokenDocumentSF2e } from "@module/scene/token-document";

export class FakeToken {
    _actor: ActorSF2e | null;
    parent: SceneSF2e | null;
    data: foundry.data.TokenData<TokenDocumentSF2e>;

    constructor(data: foundry.data.TokenSource, context: TokenDocumentConstructionContext<TokenDocumentSF2e> = {}) {
        this.data = duplicate(data) as foundry.data.TokenData<TokenDocumentSF2e>;
        this.parent = context.parent ?? null;
        this._actor = context.actor ?? null;
    }

    get actor() {
        return this._actor;
    }

    get scene() {
        return this.parent;
    }

    get id() {
        return this.data._id;
    }

    get name() {
        return this.data.name;
    }

    update(changes: EmbeddedDocumentUpdateData<TokenDocument>, context: DocumentModificationContext = {}) {
        changes["_id"] = this.id;
        this.scene?.updateEmbeddedDocuments("Token", [changes], context);
    }
}
